# require_relative 'page_helper.rb'
# require_relative 'helper.rb'.
# require 'xmlrpc/client'
# require "cucumber"
# require "httparty/request"
# require "httparty/response/headers"
# require 'spreadsheet'
require 'capybara/cucumber'
require 'capybara/dsl'
require 'capybara/rspec/matchers'
require 'selenium-webdriver'
require 'allure-cucumber'
require "httparty"
require "faker"
require 'pg'
require 'ostruct'
require 'timeout'


BROWSER = ENV['BROWSER']
USER = ENV['USERNAME']

# Capybara.register_driver(:headless_chrome) do |app|
#         capabilities = Selenium::WebDriver::Remote::Capabilities.chrome(
#           # Add 'no-sandbox' arg if you have an "unknown error: Chrome failed to start: exited abnormally"
#           # @see https://github.com/SeleniumHQ/selenium/issues/4961
#           chromeOptions: { args: %w[headless disable-gpu no-sandbox] } 
#         )
      
#         Capybara::Selenium::Driver.new(app, browser: :chrome, desired_capabilities: capabilities)
# end
      
# Capybara.default_driver = :headless_chrome

Capybara.register_driver :chrome_timeout do |app|
        client = Selenium::WebDriver::Remote::Http::Default.new
        client.open_timeout = 200
        browser_options = ::Selenium::WebDriver::Chrome::Options.new.tap do |opts|
          opts.args << '--window-size=1920,1080'
          opts.args << '--force-device-scale-factor=0.95'
          opts.args << '--disable-dev-shm-usage'
          opts.args << '--disable-extensions'
          opts.args << '--headless'
          opts.args << 'disable-infobars'
          opts.args << '--disable-gpu'
          opts.args << '--disable-site-isolation-trials'
          opts.args << '--no-sandbox'
          
        end
        Capybara::Selenium::Driver.new(app, browser: :chrome, options: browser_options)
      end

    
Capybara.default_driver = :chrome_timeout

Cucumber::Core::Test::Step.module_eval do
        def name
          return text if text == 'Before hook'
          return text if text == 'After hook'
          "#{source.last.keyword}#{text}"
        end
end

DEFAULT_TMS_PREFIX      = 'TMS:'
DEFAULT_ISSUE_PREFIX    = 'ISSUE:'
DEFAULT_SEVERITY_PREFIX = 'SEVERITY:'
     

Allure.configure do |c|
        c.results_directory = "/proj_burger_king/automacao_servicos/results"
        c.clean_results_directory = false
        c.logging_level = Logger::INFO
        # these are used for creating links to bugs or test cases where {} is replaced with keys of relevant items
        c.link_tms_pattern = "http://www.jira.com/browse/{}"
        c.link_issue_pattern = "http://www.jira.com/browse/{}"
end