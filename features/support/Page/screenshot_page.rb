def tirar_foto(nome_arquivo)
    data = Time.now.strftime("%Y%m%d%H%M%S")
    caminho_arquivo = "results/screenshots/test_passou/#{$data_execucao}"
    foto = "#{caminho_arquivo}/#{data}_#{nome_arquivo}.png"
    
    width  = Capybara.page.execute_script("return Math.max(document.body.scrollWidth, document.body.offsetWidth, document.documentElement.clientWidth, document.documentElement.scrollWidth, document.documentElement.offsetWidth);")
    height = Capybara.page.execute_script("return Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);")

    # # puts width
    # # puts height

    window = Capybara.current_session.driver.browser.manage.window
    window.resize_to(width, height)

    # def take_full_page_screenshot
    #     # Get the actual page dimensions using javascript
    #     #
    #     width  = Capybara.page.execute_script("return Math.max(document.body.scrollWidth, document.body.offsetWidth, document.documentElement.clientWidth, document.documentElement.scrollWidth, document.documentElement.offsetWidth);")
    #     height = Capybara.page..execute_script("return Math.max(document.body.scrollHeight, document.body.offsetHeight, document.documentElement.clientHeight, document.documentElement.scrollHeight, document.documentElement.offsetHeight);")
        
    #     window = Capybara.current_session.driver.browser.manage.window
    #     window.resize_to(width+100, height+100)
        
    #     img = page.driver.browser.screenshot_as(:png)
        
    #     File.open('full-page.png', 'w+') do |f|
    #       f.write img
    #     end
    #     end
    # window.resize_to(1136,1038)
    page.save_screenshot(foto)
    embed(foto, 'image/png', 'Visualizar evidência do step abaixo')
    # window.resize_to(1136,1038)
    return(foto)
 end