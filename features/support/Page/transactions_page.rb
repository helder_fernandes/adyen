def criaTransacao(encriptacao,code)
    data = Time.now.strftime("%Y%m%d%H%M%S")
    orderid = Faker::Number.number(digits: 10).to_s
    customerId = data.to_s + Faker::Number.number(digits: 6).to_s
    amount = Faker::Number.decimal(l_digits: 2, r_digits: 2)
    # STDOUT.puts amount

    body = {
            "orderId": "#{orderid}", # faker (AAAAMMDD00000001)
            "paymentId": "2000006",
            "amount": "#{amount}", # faker (500000 ou 5000000)
            "currency": "BRL",
            "customerId": "#{customerId}", # faker (AAAAMMDD00000001)
            "email": "#{$email}",
            "processingType": "reserved",
            "storeCard": false,
            "card": {
                "code": code,
                "cardHolderName":"Daniel N Rodrigues",
                "expirationMonth": encriptacao[1],
                "expirationYear": encriptacao[2],
                "number": encriptacao[0],
                "securityCode": encriptacao[3],
                "type":"credit_card"
            },
            "additionalData": {
                "ip": "172.200.20.8",
                "storeCode": "99994",
                "createAt": "2019-04-26T22:33:06",
                "customer": {
                    "id": "bd1c7a33-23d2-4a0f-9116-488019753ba2",
                    "email": "#{$email}",
                    "phoneNumber": "(11) 98977-4828",
                    "name": "QA teste",
                    "birthDate": "1981-06-20T00:00:00",
                    "genre": "M",
                    "cpf": "177.418.468-09",
                    "createAt": "2019-09-01T10:00:00",
                    "addressCountry": "Brasil",
                    "addressZipcode": "06414-000",
                    "addressLocation": "Rua Marte",
                    "addressNeighborhood": "Jardim Tupanci",
                    "addressNumber": "426",
                    "addressCity": "Barueri",
                    "addressState": "SP"
                },
                "address": {
                    "country": "BR",
                    "zipCode": "",
                    "location": "AL RIO NEGRO ",
                    "neighborhood": "ALPHAVILLE INDUSTRIAL",
                    "number": "500",
                    "complement": "",
                    "city": "Barueri",
                    "state": "SP"
                },
                "parts": [
                    {
                        "partCode": "7100026",
                        "partType": "Combo",
                        "title": "BK PICANHA PRIME",
                        "defaultQuantity": 0,
                        "unitPrice": 30.9,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    }
                ]
            }
        }
    @body = JSON.generate(body)
    # puts @body
    # STDOUT.puts @body2 = ":" + @body+ ":"
    # STDOUT.puts @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @orderid = JSON.parse(@body)['orderId']
    @hmac = $api_token
    modulo = 'transaction'
    @transaction = Cartao.new(@body, @hmac, modulo)
    return @body, @orderid
end

def criaTransacao_fraude(encriptacao,code)
    data = Time.now.strftime("%Y%m%d%H%M%S")
    orderid = Faker::Number.number(digits: 10).to_s
    customerId = data.to_s + Faker::Number.number(digits: 6).to_s
    amount = Faker::Number.decimal(l_digits: 2, r_digits: 2)
    # STDOUT.puts amount

    body = {
            "orderId": "#{orderid}", # faker (AAAAMMDD00000001)
            "paymentId": "2000006",
            "amount": "#{amount}", # faker (500000 ou 5000000)
            "currency": "BRL",
            "customerId": "#{customerId}", # faker (AAAAMMDD00000001)
            "email": "#{$email}",
            "processingType": "reserved",
            "storeCard": false,
            "card": {
                "code": code,
                "cardHolderName":"TEST_EDP_ADYEN_RESPONSE:20",
                "expirationMonth": encriptacao[1],
                "expirationYear": encriptacao[2],
                "number": encriptacao[0],
                "securityCode": encriptacao[3],
                "type":"credit_card"
            },
            "additionalData": {
                "ip": "172.200.20.8",
                "storeCode": "99994",
                "createAt": "2019-04-26T22:33:06",
                "customer": {
                    "id": "bd1c7a33-23d2-4a0f-9116-488019753ba2",
                    "email": "#{$email}",
                    "phoneNumber": "(11) 98977-4828",
                    "name": "QA teste",
                    "birthDate": "1981-06-20T00:00:00",
                    "genre": "M",
                    "cpf": "177.418.468-09",
                    "createAt": "2019-09-01T10:00:00",
                    "addressCountry": "Brasil",
                    "addressZipcode": "06414-000",
                    "addressLocation": "Rua Marte",
                    "addressNeighborhood": "Jardim Tupanci",
                    "addressNumber": "426",
                    "addressCity": "Barueri",
                    "addressState": "SP"
                },
                "address": {
                    "country": "BR",
                    "zipCode": "",
                    "location": "AL RIO NEGRO ",
                    "neighborhood": "ALPHAVILLE INDUSTRIAL",
                    "number": "500",
                    "complement": "",
                    "city": "Barueri",
                    "state": "SP"
                },
                "parts": [
                    {
                        "partCode": "7100026",
                        "partType": "Combo",
                        "title": "BK PICANHA PRIME",
                        "defaultQuantity": 0,
                        "unitPrice": 30.9,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    }
                ]
            }
        }
    @body = JSON.generate(body)
    # puts @body
    # STDOUT.puts @body2 = ":" + @body+ ":"
    # STDOUT.puts @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @orderid = JSON.parse(@body)['orderId']
    @hmac = $api_token
    modulo = 'transaction'
    @transaction = Cartao.new(@body, @hmac, modulo)
    return @body, @orderid
end

def criaTransacao_sem_address(encriptacao,code)
    data = Time.now.strftime("%Y%m%d%H%M%S")
    orderid = Faker::Number.number(digits: 10).to_s
    customerId = data.to_s + Faker::Number.number(digits: 6).to_s
    amount = Faker::Number.decimal(l_digits: 2, r_digits: 2)
    # STDOUT.puts amount

    body = {
            "orderId": "#{orderid}", # faker (AAAAMMDD00000001)
            "paymentId": "2000006",
            "amount": "#{amount}", # faker (500000 ou 5000000)
            "currency": "BRL",
            "customerId": "#{customerId}", # faker (AAAAMMDD00000001)
            "email": "#{$email}",
            "processingType": "reserved",
            "storeCard": false,
            "card": {
                "code": code,
                "cardHolderName":"Daniel N Rodrigues",
                "expirationMonth": encriptacao[1],
                "expirationYear": encriptacao[2],
                "number": encriptacao[0],
                "securityCode": encriptacao[3],
                "type":"credit_card"
            },
            "additionalData": {
                "ip": "172.200.20.8",
                "storeCode": "99994",
                "createAt": "2019-04-26T22:33:06",
                "customer": {
                    "id": "bd1c7a33-23d2-4a0f-9116-488019753ba2",
                    "email": "#{$email}",
                    "phoneNumber": "(11) 98977-4828",
                    "name": "QA teste",
                    "birthDate": "1981-06-20T00:00:00",
                    "genre": "M",
                    "cpf": "177.418.468-09",
                    "createAt": "2019-09-01T10:00:00",
                    "addressCountry": "Brasil",
                    "addressZipcode": "06414-000",
                    "addressLocation": "Rua Marte",
                    "addressNeighborhood": "Jardim Tupanci",
                    "addressNumber": "426",
                    "addressCity": "Barueri",
                    "addressState": "SP"
                },
                "parts": [
                    {
                        "partCode": "7100026",
                        "partType": "Combo",
                        "title": "BK PICANHA PRIME",
                        "defaultQuantity": 0,
                        "unitPrice": 30.9,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    }
                ]
            }
        }
    @body = JSON.generate(body)
    # puts @body
    # STDOUT.puts @body2 = ":" + @body+ ":"
    # STDOUT.puts @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @orderid = JSON.parse(@body)['orderId']
    @hmac = $api_token
    modulo = 'transaction'
    @transaction = Cartao.new(@body, @hmac, modulo)
    return @body, @orderid
end

def criaTransacao_Sem_StoreCard(encriptacao)
    data = Time.now.strftime("%Y%m%d%H%M%S")
    orderid = Faker::Number.number(digits: 10).to_s
    customerId = data.to_s + Faker::Number.number(digits: 6).to_s
    amount = Faker::Number.decimal(l_digits: 2, r_digits: 2)
    # STDOUT.puts amount

    body = {
            "orderId": "#{orderid}", # faker (AAAAMMDD00000001)
            "paymentId": "2000006",
            "amount": "#{amount}", # faker (500000 ou 5000000)
            "currency": "BRL",
            "customerId": "#{customerId}", # faker (AAAAMMDD00000001)
            "email": "#{$email}",
            "processingType": "reserved",
            "card": {
                "code":"mc",
                "cardHolderName":"Daniel N Rodrigues",
                "expirationMonth": encriptacao[1],
                "expirationYear": encriptacao[2],
                "number": encriptacao[0],
                "securityCode": encriptacao[3],
                "type":"credit_card"
            },
            "additionalData": {
                "ip": "172.200.20.8",
                "storeCode": "99994",
                "createAt": "2019-04-26T22:33:06",
                "customer": {
                    "id": "bd1c7a33-23d2-4a0f-9116-488019753ba2",
                    "email": "#{$email}",
                    "phoneNumber": "(11) 98977-4828",
                    "name": "Jedi teste hmledp",
                    "birthDate": "1981-06-20T00:00:00",
                    "genre": "M",
                    "cpf": "177.418.468-09",
                    "createAt": "2019-04-26T22:33:06",
                    "addressCountry": "Brasil",
                    "addressZipcode": "06414-000",
                    "addressLocation": "Rua Marte",
                    "addressNeighborhood": "Jardim Tupanci",
                    "addressNumber": "426",
                    "addressCity": "Barueri",
                    "addressState": "SP"
                },
                "address": {
                    "country": "BR",
                    "zipCode": "",
                    "location": "AL RIO NEGRO ",
                    "neighborhood": "ALPHAVILLE INDUSTRIAL",
                    "number": "500",
                    "complement": "",
                    "city": "Barueri",
                    "state": "SP"
                },
                "parts": [
                    {
                        "partCode": "7100026",
                        "partType": "Combo",
                        "title": "BK PICANHA PRIME",
                        "defaultQuantity": 0,
                        "unitPrice": 30.9,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    }
                ]
            }
        }
    @body = JSON.generate(body)
    # puts @body
    # STDOUT.puts @body2 = ":" + @body+ ":"
    # STDOUT.puts @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @orderid = JSON.parse(@body)['orderId']
    @hmac = '5E05B564-3CF3-4ADB-9A19-8138419D6CB9'
    modulo = 'transaction'
    @transaction = Cartao.new(@body, @hmac, modulo)
    return @body, @orderid
end

def criaTransacao_numero_errado()
    data = Time.now.strftime("%Y%m%d%H%M%S")
    orderid = Faker::Number.number(digits: 10).to_s
    customerId = data.to_s + Faker::Number.number(digits: 6).to_s
    amount = Faker::Number.decimal(l_digits: 2, r_digits: 2)
    # STDOUT.puts amount

    body = {
            "orderId": "#{orderid}", # faker (AAAAMMDD00000001)
            "paymentId": "2000006",
            "amount": "#{amount}", # faker (500000 ou 5000000)
            "currency": "BRL",
            "customerId": "#{customerId}", # faker (AAAAMMDD00000001)
            "email": "#{$email}",
            "processingType": "reserved",
            "storeCard": false,
            "card": {
                "code":"mc",
                "cardHolderName":"teste compra visaSG",
                # "cardHolderName":"Daniel N Rodrigues",
                "expirationMonth": "adyenjs_0_1_18$NYltOWDsMpXo1HZkvZJaVZwTT2mD5cqq8ks+yr8tNKGnCLDs2l5TVOkyclCL4u+YXDDlB9plTxWHXlpTv42zojUaDv5MLrSRht4rm0hbsQ1+HLIn8KznPcK10UTzRjm0bCo7t4jkyOAjMxXYipMR3mQnKFG9sro0JpfeIKenWERKUpt8vA3vVJ9aZy3SImdEmR3vgetNttwby7yk4jBMm+cDT1QyZys87Yvz4Cx3Nydl2PrgHxQpr+7C6ZYjgf3+FsrZI//61gGFCk+QMS1nPOG9QRCfzKtFuwNyFGmEsEKJw5A8bHUFf9RV0ockyF4LuYVso8GCQ2SVkCDUyNDrZA==$A77mFk424VQcRciHezU5vFNH2snKJ4T4y4Y8ek2yQ/mxkM3swTy4EY/M3UgRVUXoNHfc/BwvMvm6NhBDutPL3LU8EzsMCOzvkP8N7c1zxP9mOD9EIJ43NFEk9Oz7ixglt9s05HWnHf4x/Dt+aqqTtgZR5L19AClF6+vczNTBm7yojwOZJzrwaFignwbUoBkFMMAqncnkIqtbR0l4CPHSojcR0v6jeXINp/kwA/RXZO/ycSt5knvduRnZ0w4xUkB3Ip0EnQks+tv7s3ZClhhvT01PbVhOVZQISze1QKEUsOc97oAiEQwkEhw0s7/epwFn9a0=",
                "expirationYear": "adyenjs_0_1_18$qrMVOSark2Q8JRzoDJuwue/8Ard4pN1mz+s+D3oWgzlJHScmFb8ouhJCjNb9Xa+W1t6URkOOVXbGRMTfJQHurc/U2bb9NREXvUzZuheWP+GfgLHl7sDIni+0kxtapp6Z/693M8Bsxvo/df2D1PXEIqheikDi+w7PVYTM/2EY1E48ckcPSmn1uHzn7BVIbtLXq95nY0X/ZJQDWMBBlacI0xRrUMRz7n6V+tB+CMo6BqiJgQFqKwC0qbleOW77dAG8YnDO6gyUP4mDpdFC7nhMLJsvuKvJgQj3ii7kuw9uHqdnDW48/+6VO4ppJWS1pdc8udx2wzwz+D6pIEtenodZSA==$vjyhVK2dgis4SEe9ZW/rMXn/GowsZtW01i9f84Rjmr3Aysc0Y3ntIfcCH7ab4UEKEsIBjOb1/CWNLwJj7wudUT55yXRtIUz0CxzCcvJcfqgNyW0gKEmOSQ2N2VQjIMqdG9+9bLI9/1DkOgyHIGGKR0lgbSgIjpJadHIWDwOmPVBcAqFVWlOr9bTlzg1+7vLKYQyQYJb5Dq2RVFeC3xWX6iuGFrWzMr/gu8BUmBQzmBCfY8wx555WmLjU69SMhegEBYhsZjW/tUgRKrRlKSW+2qtYsn8yxmpi3MkUHIQdVpBQj6LV2Lumk8Kj8xc+eWp5ahcN",
                "number": "adyenjs_0_1_18$NXGo7P5zNxZ/3BrVZ+kUNjJ1eooZe99d6/eXnAx0IvOPgBoxxrQpCtmYESASbPBQrDfszxPKDwznnmDJMka8vKfLFVdnp+KFH92uStisQVce3q2fW7N4IzrFiFKz8gYCn3dydFdP3RxdGEd3DSqo18p+5pLKJ5IAXxVKwgOrteiea2YVcHU7yNyUj1Qg6ajnBn02519II29NGbg5mNhb1S6ABiUpnZkPgIP+AnpKF1jkD4DlPgMpDu9JVUGJP+4UPzxlN3PdmPZyFlTEbDjaI8y8QigDT6yoyRydLPgQWgSEiAinF6WRv4mrAtjLusrKsp9D8N4izZWJWYt6ggic1Q==$uNWRmuBwmSMdkmn2w0EpdA4JzSd1U5OaFFPEMRhovfVuHsSQ/Evk38mEus+CLkzecdad8u96NnnxzDn+cUMHIRhTDRYlVL1J1KGQrRMrYMDranI3xlRFmCysuTDN+ce9ZIulg9zOTDB1NGcQ/FmPj3BbymY7NRQXZDUncrWGw3kLs6UEH5BYvxoN4DHAsbZ7hSnWFc4VJevc/CWZBVeOPAAv8e9QT52kkL9WclQrtswByqx2b/a7vMeTJjGxjlLw54LOUfFdLv3Per6qFrSjtlfYdwZKYikSmcFxfSk8bfBQ1gNxdMc63OxOkylSFKOSe48A/ANCSmJ+YXi+XF/lOLL3jH9N933TG4Vo+S+66PyrFZm0T7upCDZ9z9g5v+VtknSg9ynaZ73H9QW5fcnQo7Ytj8Eb86ZxCNje",
                "securityCode": "adyenjs_0_1_18$4NEGZ09P7b0BSNeWKSku2J8YTJT/RB+8yRmIOq1an3bBOEP0f6gnQt8n8du+x6GT9qG29WWCGwmfAlsRV1xLKTQTajZLyvSgLMYX0sFex/sd3Skl60vYK9MISdcFPw5kuou61FmOy+V2RYfOsQc90npnRK82BLg2Jl1dDn97sqTzR84tjQmTIVPIP9G2Q4PLYIHEsaP/KJbG1SZTEvO4X3uthvknMgFPH39IH5CSjqeXbsadH4+RFcs3gZeLoTohBy2RvQ6O1d88NRPP8yeBNql/3V+nEk0KKIoL1FI7z8uWBm2c3XMgTV3U9t+kFx0A2hafYelo98Gjp0go1a6/3w==$B6N7LSCOi2u8q3ZV8DsI4xPONlYFE9JHJMBP6u7YgvkOdQsVfXMBZf6C6cRoEt+ZA2ZgCpuRroFfqg0aC9PaMEidy36SE2mvY3n7dlsOFi16G7U1ZaUqYPRKCyV2nKjuimvCOFLuCS1BQL/fZBLTrLncB/eDlQ3iJQjUMG3iPMHmvKxT/R8XjLwkXRh1VOPgcxblpKRlJFkFzTYyCP1kMgeMHRNoMQg0RVTvS5W+cZfH9DKSH4zKPlG61tyEAQzqy8ThfK0qmd2g1lSTjlto5n+6syKWBjyDnY5IETc6LQc7KfQ6iiq/Td/zUg==",
                "type":"credit_card"
            },
            "additionalData": {
                "ip": "172.200.20.8",
                "storeCode": "99994",
                "createAt": "2019-04-26T22:33:06",
                "customer": {
                    "id": "bd1c7a33-23d2-4a0f-9116-488019753ba2",
                    "email": "#{$email}",
                    "phoneNumber": "(11) 98977-4828",
                    "name": "Jedi teste hmledp",
                    "birthDate": "1981-06-20T00:00:00",
                    "genre": "M",
                    "cpf": "177.418.468-09",
                    "createAt": "2019-04-26T22:33:06",
                    "addressCountry": "Brasil",
                    "addressZipcode": "06414-000",
                    "addressLocation": "Rua Marte",
                    "addressNeighborhood": "Jardim Tupanci",
                    "addressNumber": "426",
                    "addressCity": "Barueri",
                    "addressState": "SP"
                },
                "address": {
                    "country": "BR",
                    "zipCode": "",
                    "location": "AL RIO NEGRO ",
                    "neighborhood": "ALPHAVILLE INDUSTRIAL",
                    "number": "500",
                    "complement": "",
                    "city": "Barueri",
                    "state": "SP"
                },
                "parts": [
                    {
                        "partCode": "7100026",
                        "partType": "Combo",
                        "title": "BK PICANHA PRIME",
                        "defaultQuantity": 0,
                        "unitPrice": 30.9,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    }
                ]
            }
        }
    @body = JSON.generate(body)
    # puts @body
    # STDOUT.puts @body2 = ":" + @body+ ":"
    # STDOUT.puts @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @orderid = JSON.parse(@body)['orderId']
    @hmac = '5E05B564-3CF3-4ADB-9A19-8138419D6CB9'
    modulo = 'transaction'
    @transaction = Cartao.new(@body, @hmac, modulo)
    return @body, @orderid
end

def criaTransacao_Orderid_Manual(encriptacao, order)
    data = Time.now.strftime("%Y%m%d%H%M%S")
    # orderid = Faker::Number.number(digits: 10).to_s
    amount = Faker::Number.decimal(l_digits: 2, r_digits: 2)
    customerId = data.to_s + Faker::Number.number(digits: 6).to_s
    # STDOUT.puts amount

    body = {
            "orderId": "#{order}", # faker (AAAAMMDD00000001)
            "paymentId": "2000006",
            "amount": "#{amount}", # faker (500000 ou 5000000)
            "currency": "BRL",
            "customerId": "#{customerId}", # faker (AAAAMMDD00000001)
            "email": "#{$email}",
            "processingType": "reserved",
            "storeCard": false,
            "card": {
                "code":"mc",
                "cardHolderName":"Daniel N Rodrigues",
                "expirationMonth": encriptacao[1],
                "expirationYear": encriptacao[2],
                "number": encriptacao[0],
                "securityCode": encriptacao[3],
                "type":"credit_card"
            },
            "additionalData": {
                "ip": "172.200.20.8",
                "storeCode": "99994",
                "createAt": "2019-04-26T22:33:06",
                "customer": {
                    "id": "bd1c7a33-23d2-4a0f-9116-488019753ba2",
                    "email": "#{$email}",
                    "phoneNumber": "(11) 98977-4828",
                    "name": "Jedi teste hmledp",
                    "birthDate": "1981-06-20T00:00:00",
                    "genre": "M",
                    "cpf": "177.418.468-09",
                    "createAt": "2019-04-26T22:33:06",
                    "addressCountry": "Brasil",
                    "addressZipcode": "06414-000",
                    "addressLocation": "Rua Marte",
                    "addressNeighborhood": "Jardim Tupanci",
                    "addressNumber": "426",
                    "addressCity": "Barueri",
                    "addressState": "SP"
                },
                "address": {
                    "country": "BR",
                    "zipCode": "",
                    "location": "AL RIO NEGRO ",
                    "neighborhood": "ALPHAVILLE INDUSTRIAL",
                    "number": "500",
                    "complement": "",
                    "city": "Barueri",
                    "state": "SP"
                },
                "parts": [
                    {
                        "partCode": "7100026",
                        "partType": "Combo",
                        "title": "BK PICANHA PRIME",
                        "defaultQuantity": 0,
                        "unitPrice": 30.9,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    }
                ]
            }
        }
    @body = JSON.generate(body)
    # STDOUT.puts @body2 = ":" + @body+ ":"
    # STDOUT.puts @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @orderid = JSON.parse(@body)["orderId"]
    @hmac = '5E05B564-3CF3-4ADB-9A19-8138419D6CB9'
    modulo = 'transaction'
    @transaction = Cartao.new(@body, @hmac, modulo)
    return @body, @orderid
end

def envia_transacao_full(table)
    visit $pagina_encriptacao
    dados = table.hashes[0]
    encriptacao = @encrypter.preencher_cartao(dados)
    
    id = ""
    status = ""
    code = "mc"
    body,orderid = criaTransacao(encriptacao,code)
    obj1 = JSON.parse(body)
    pretty_str1 = JSON.pretty_unparse(obj1)

    response = @transaction.postTransaction()
    obj = JSON.parse(response.body) 
    pretty_str = JSON.pretty_unparse(obj)
    # puts pretty_str             
    obj.each_pair {|k,v| 
        id = v if k == "id"
        status = v if k == "status"
    } 
    # sleep 2
    return id,status,response, pretty_str1

end

def criaTransacaoStoreCard(encriptacao, storeCard)
    data = Time.now.strftime("%Y%m%d%H%M%S")
    orderid = Faker::Number.number(digits: 10).to_s
    customerId = data.to_s + Faker::Number.number(digits: 6).to_s
    amount = Faker::Number.decimal(l_digits: 2, r_digits: 2)
    # STDOUT.puts amount

    body = {
            "orderId": "#{orderid}", # faker (AAAAMMDD00000001)
            "paymentId": "2000006",
            "amount": "#{amount}", # faker (500000 ou 5000000)
            "currency": "BRL",
            "customerId": "#{customerId}", # faker (AAAAMMDD00000001)
            "email": "#{$email}",
            "processingType": "reserved",
            "storeCard": storeCard,
            "card": {
                "code":"mc",
                "cardHolderName":"Daniel N Rodrigues",
                "expirationMonth": encriptacao[1],
                "expirationYear": encriptacao[2],
                "number": encriptacao[0],
                "securityCode": encriptacao[3],
                "type":"credit_card"
            },
            "additionalData": {
                "ip": "172.200.20.8",
                "storeCode": "99994",
                "createAt": "2019-04-26T22:33:06",
                "customer": {
                    "id": "bd1c7a33-23d2-4a0f-9116-488019753ba2",
                    "email": "#{$email}",
                    "phoneNumber": "(11) 98977-4828",
                    "name": "Jedi teste hmledp",
                    "birthDate": "1981-06-20T00:00:00",
                    "genre": "M",
                    "cpf": "177.418.468-09",
                    "createAt": "2019-04-26T22:33:06",
                    "addressCountry": "Brasil",
                    "addressZipcode": "06414-000",
                    "addressLocation": "Rua Marte",
                    "addressNeighborhood": "Jardim Tupanci",
                    "addressNumber": "426",
                    "addressCity": "Barueri",
                    "addressState": "SP"
                },
                "address": {
                    "country": "BR",
                    "zipCode": "",
                    "location": "AL RIO NEGRO ",
                    "neighborhood": "ALPHAVILLE INDUSTRIAL",
                    "number": "500",
                    "complement": "",
                    "city": "Barueri",
                    "state": "SP"
                },
                "parts": [
                    {
                        "partCode": "7100026",
                        "partType": "Combo",
                        "title": "BK PICANHA PRIME",
                        "defaultQuantity": 0,
                        "unitPrice": 30.9,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    }
                ]
            }
        }
    @body = JSON.generate(body)
    # puts @body
    # STDOUT.puts @body2 = ":" + @body+ ":"
    # STDOUT.puts @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @orderid = JSON.parse(@body)['orderId']
    @hmac = '5E05B564-3CF3-4ADB-9A19-8138419D6CB9'
    modulo = 'transaction'
    @transaction = Cartao.new(@body, @hmac, modulo)
    return @body, @orderid
end

def criaTransacaoProcessingType(encriptacao, type)
    data = Time.now.strftime("%Y%m%d%H%M%S")
    orderid = Faker::Number.number(digits: 10).to_s
    customerId = data.to_s + Faker::Number.number(digits: 6).to_s
    amount = Faker::Number.decimal(l_digits: 2, r_digits: 2)
    # STDOUT.puts amount

    body = {
            "orderId": "#{orderid}", # faker (AAAAMMDD00000001)
            "paymentId": "2000006",
            "amount": "#{amount}", # faker (500000 ou 5000000)
            "currency": "BRL",
            "customerId": "#{customerId}", # faker (AAAAMMDD00000001)
            "email": "#{$email}",
            "processingType": "#{type}",
            "storeCard": false,
            "card": {
                "code": "mc",
                "cardHolderName":"Daniel N Rodrigues",
                "expirationMonth": encriptacao[1],
                "expirationYear": encriptacao[2],
                "number": encriptacao[0],
                "securityCode": encriptacao[3],
                "type":"credit_card"
            },
            "additionalData": {
                "ip": "172.200.20.8",
                "storeCode": "99994",
                "createAt": "2019-04-26T22:33:06",
                "customer": {
                    "id": "bd1c7a33-23d2-4a0f-9116-488019753ba2",
                    "email": "#{$email}",
                    "phoneNumber": "(11) 98977-4828",
                    "name": "Jedi teste hmledp",
                    "birthDate": "1981-06-20T00:00:00",
                    "genre": "M",
                    "cpf": "177.418.468-09",
                    "createAt": "2019-04-26T22:33:06",
                    "addressCountry": "Brasil",
                    "addressZipcode": "06414-000",
                    "addressLocation": "Rua Marte",
                    "addressNeighborhood": "Jardim Tupanci",
                    "addressNumber": "426",
                    "addressCity": "Barueri",
                    "addressState": "SP"
                },
                "address": {
                    "country": "BR",
                    "zipCode": "",
                    "location": "AL RIO NEGRO ",
                    "neighborhood": "ALPHAVILLE INDUSTRIAL",
                    "number": "500",
                    "complement": "",
                    "city": "Barueri",
                    "state": "SP"
                },
                "parts": [
                    {
                        "partCode": "7100026",
                        "partType": "Combo",
                        "title": "BK PICANHA PRIME",
                        "defaultQuantity": 0,
                        "unitPrice": 30.9,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    }
                ]
            }
        }
    @body = JSON.generate(body)
    # puts @body
    # STDOUT.puts @body2 = ":" + @body+ ":"
    # STDOUT.puts @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @orderid = JSON.parse(@body)['orderId']
    @hmac = '5E05B564-3CF3-4ADB-9A19-8138419D6CB9'
    modulo = 'transaction'
    @transaction = Cartao.new(@body, @hmac, modulo)
    return @body, @orderid
end

def criaTransacaoComValor(encriptacao, valor)
    data = Time.now.strftime("%Y%m%d%H%M%S")
    orderid = Faker::Number.number(digits: 10).to_s
    customerId = data.to_s + Faker::Number.number(digits: 6).to_s
    # STDOUT.puts amount

    body = {
            "orderId": "#{orderid}", # faker (AAAAMMDD00000001)
            "paymentId": "2000006",
            "amount": "#{valor}", # faker (500000 ou 5000000)
            "currency": "BRL",
            "customerId": "#{customerId}", # faker (AAAAMMDD00000001)
            "email": "#{$email}",
            "processingType": "reserved",
            "storeCard": false,
            "card": {
                "code":"mc",
                "cardHolderName":"Daniel N Rodrigues",
                "expirationMonth": encriptacao[1],
                "expirationYear": encriptacao[2],
                "number": encriptacao[0],
                "securityCode": encriptacao[3],
                "type":"credit_card"
            },
            "additionalData": {
                "ip": "172.200.20.8",
                "storeCode": "99994",
                "createAt": "2019-04-26T22:33:06",
                "customer": {
                    "id": "bd1c7a33-23d2-4a0f-9116-488019753ba2",
                    "email": "#{$email}",
                    "phoneNumber": "(11) 98977-4828",
                    "name": "Jedi teste hmledp",
                    "birthDate": "1981-06-20T00:00:00",
                    "genre": "M",
                    "cpf": "177.418.468-09",
                    "createAt": "2019-04-26T22:33:06",
                    "addressCountry": "Brasil",
                    "addressZipcode": "06414-000",
                    "addressLocation": "Rua Marte",
                    "addressNeighborhood": "Jardim Tupanci",
                    "addressNumber": "426",
                    "addressCity": "Barueri",
                    "addressState": "SP"
                },
                "address": {
                    "country": "BR",
                    "zipCode": "",
                    "location": "AL RIO NEGRO ",
                    "neighborhood": "ALPHAVILLE INDUSTRIAL",
                    "number": "500",
                    "complement": "",
                    "city": "Barueri",
                    "state": "SP"
                },
                "parts": [
                    {
                        "partCode": "7100026",
                        "partType": "Combo",
                        "title": "BK PICANHA PRIME",
                        "defaultQuantity": 0,
                        "unitPrice": 30.9,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    }
                ]
            }
        }
    @body = JSON.generate(body)
    # STDOUT.puts @body2 = ":" + @body+ ":"
    # STDOUT.puts @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @orderid = JSON.parse(@body)["orderId"]
    @hmac = '5E05B564-3CF3-4ADB-9A19-8138419D6CB9'
    modulo = 'transaction'
    @transaction = Cartao.new(@body, @hmac, modulo)
    return @body, @orderid
end

def criaTransacao_informando_Zipcode(encriptacao, zipcode)
    data = Time.now.strftime("%Y%m%d%H%M%S")
    orderid = Faker::Number.number(digits: 10).to_s
    customerId = data.to_s + Faker::Number.number(digits: 6).to_s
    amount = Faker::Number.decimal(l_digits: 2, r_digits: 2)
    # STDOUT.puts amount

    body = {
            "orderId": "#{orderid}", # faker (AAAAMMDD00000001)
            "paymentId": "2000006",
            "amount": "#{amount}", # faker (500000 ou 5000000)
            "currency": "BRL",
            "customerId": "#{customerId}", # faker (AAAAMMDD00000001)
            "email": "#{$email}",
            "processingType": "reserved",
            "storeCard": false,
            "card": {
                "code":"mc",
                "cardHolderName":"Daniel N Rodrigues",
                "expirationMonth": encriptacao[1],
                "expirationYear": encriptacao[2],
                "number": encriptacao[0],
                "securityCode": encriptacao[3],
                "type":"credit_card"
            },
            "additionalData": {
                "ip": "172.200.20.8",
                "storeCode": "99994",
                "createAt": "2019-04-26T22:33:06",
                "customer": {
                    "id": "bd1c7a33-23d2-4a0f-9116-488019753ba2",
                    "email": "#{$email}",
                    "phoneNumber": "(11) 98977-4828",
                    "name": "Jedi teste hmledp",
                    "birthDate": "1981-06-20T00:00:00",
                    "genre": "M",
                    "cpf": "177.418.468-09",
                    "createAt": "2019-04-26T22:33:06",
                    "addressCountry": "Brasil",
                    "addressZipcode": zipcode,
                    "addressLocation": "Rua Marte",
                    "addressNeighborhood": "Jardim Tupanci",
                    "addressNumber": "426",
                    "addressCity": "Barueri",
                    "addressState": "SP"
                },
                "address": {
                    "country": "BR",
                    "zipCode": "",
                    "location": "AL RIO NEGRO ",
                    "neighborhood": "ALPHAVILLE INDUSTRIAL",
                    "number": "500",
                    "complement": "",
                    "city": "Barueri",
                    "state": "SP"
                },
                "parts": [
                    {
                        "partCode": "7100026",
                        "partType": "Combo",
                        "title": "BK PICANHA PRIME",
                        "defaultQuantity": 0,
                        "unitPrice": 30.9,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    }
                ]
            }
        }
    @body = JSON.generate(body)
    # puts @body
    # STDOUT.puts @body2 = ":" + @body+ ":"
    # STDOUT.puts @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @orderid = JSON.parse(@body)['orderId']
    @hmac = '5E05B564-3CF3-4ADB-9A19-8138419D6CB9'
    modulo = 'transaction'
    @transaction = Cartao.new(@body, @hmac, modulo)
    return @body, @orderid
end

def criaTransacao_sem_Zipcode(encriptacao)
    data = Time.now.strftime("%Y%m%d%H%M%S")
    orderid = Faker::Number.number(digits: 10).to_s
    customerId = data.to_s + Faker::Number.number(digits: 6).to_s
    amount = Faker::Number.decimal(l_digits: 2, r_digits: 2)
    # STDOUT.puts amount

    body = {
            "orderId": "#{orderid}", # faker (AAAAMMDD00000001)
            "paymentId": "2000006",
            "amount": "#{amount}", # faker (500000 ou 5000000)
            "currency": "BRL",
            "customerId": "#{customerId}", # faker (AAAAMMDD00000001)
            "email": "#{$email}",
            "processingType": "reserved",
            "storeCard": false,
            "card": {
                "code":"mc",
                "cardHolderName":"Daniel N Rodrigues",
                "expirationMonth": encriptacao[1],
                "expirationYear": encriptacao[2],
                "number": encriptacao[0],
                "securityCode": encriptacao[3],
                "type":"credit_card"
            },
            "additionalData": {
                "ip": "172.200.20.8",
                "storeCode": "99994",
                "createAt": "2019-04-26T22:33:06",
                "customer": {
                    "id": "bd1c7a33-23d2-4a0f-9116-488019753ba2",
                    "email": "#{$email}",
                    "phoneNumber": "(11) 98977-4828",
                    "name": "Jedi teste hmledp",
                    "birthDate": "1981-06-20T00:00:00",
                    "genre": "M",
                    "cpf": "177.418.468-09",
                    "createAt": "2019-04-26T22:33:06",
                    "addressCountry": "Brasil",
                    "addressLocation": "Rua Marte",
                    "addressNeighborhood": "Jardim Tupanci",
                    "addressNumber": "426",
                    "addressCity": "Barueri",
                    "addressState": "SP"
                },
                "address": {
                    "country": "BR",
                    "zipCode": "",
                    "location": "AL RIO NEGRO ",
                    "neighborhood": "ALPHAVILLE INDUSTRIAL",
                    "number": "500",
                    "complement": "",
                    "city": "Barueri",
                    "state": "SP"
                },
                "parts": [
                    {
                        "partCode": "7100026",
                        "partType": "Combo",
                        "title": "BK PICANHA PRIME",
                        "defaultQuantity": 0,
                        "unitPrice": 30.9,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    }
                ]
            }
        }
    @body = JSON.generate(body)
    # puts @body
    # STDOUT.puts @body2 = ":" + @body+ ":"
    # STDOUT.puts @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @orderid = JSON.parse(@body)['orderId']
    @hmac = '5E05B564-3CF3-4ADB-9A19-8138419D6CB9'
    modulo = 'transaction'
    @transaction = Cartao.new(@body, @hmac, modulo)
    return @body, @orderid
end

def criaTransacao_invalid_token(encriptacao)
    data = Time.now.strftime("%Y%m%d%H%M%S")
    orderid = Faker::Number.number(digits: 10).to_s
    customerId = data.to_s + Faker::Number.number(digits: 6).to_s
    amount = Faker::Number.decimal(l_digits: 2, r_digits: 2)
    # STDOUT.puts amount

    body = {
            "orderId": "#{orderid}", # faker (AAAAMMDD00000001)
            "paymentId": "2000006",
            "amount": "#{amount}", # faker (500000 ou 5000000)
            "currency": "BRL",
            "customerId": "#{customerId}", # faker (AAAAMMDD00000001)
            "email": "test@adyen.com",
            "processingType": "reserved",
            "storeCard": false,
            "card": {
                "code":"mc",
                "cardHolderName":"Daniel N Rodrigues",
                "expirationMonth": encriptacao[1],
                "expirationYear": encriptacao[2],
                "number": encriptacao[0],
                "securityCode": encriptacao[3],
                "type":"credit_card"
            },
            "additionalData": {
                "ip": "172.200.20.8",
                "storeCode": "99994",
                "createAt": "2019-04-26T22:33:06",
                "customer": {
                    "id": "bd1c7a33-23d2-4a0f-9116-488019753ba2",
                    "email": "jedi@hmledp.com",
                    "phoneNumber": "(11) 98977-4828",
                    "name": "Jedi teste hmledp",
                    "birthDate": "1981-06-20T00:00:00",
                    "genre": "M",
                    "cpf": "177.418.468-09",
                    "createAt": "2019-04-26T22:33:06",
                    "addressCountry": "Brasil",
                    "addressZipcode": "06414-000",
                    "addressLocation": "Rua Marte",
                    "addressNeighborhood": "Jardim Tupanci",
                    "addressNumber": "426",
                    "addressCity": "Barueri",
                    "addressState": "SP"
                },
                "address": {
                    "country": "BR",
                    "zipCode": "",
                    "location": "AL RIO NEGRO ",
                    "neighborhood": "ALPHAVILLE INDUSTRIAL",
                    "number": "500",
                    "complement": "",
                    "city": "Barueri",
                    "state": "SP"
                },
                "parts": [
                    {
                        "partCode": "7100026",
                        "partType": "Combo",
                        "title": "BK PICANHA PRIME",
                        "defaultQuantity": 0,
                        "unitPrice": 30.9,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    },
                    {
                        "partCode": "20002",
                        "partType": "Product",
                        "title": "CASQUINHA MISTA",
                        "defaultQuantity": 0,
                        "unitPrice": 1.5,
                        "parts": [],
                        "quantity": 1,
                        "observation": "",
                        "totalprice": 0
                    }
                ]
            }
        }
    @body = JSON.generate(body)
    # puts @body
    # STDOUT.puts @body2 = ":" + @body+ ":"
    # STDOUT.puts @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @orderid = JSON.parse(@body)['orderId']
    # @hmac = '5E05B564-3CF3-4ADB-9A19-8138419D6CB9'
    @hmac = '5E05B564-3CF3-4ADB-9A19-8138419D6CB3'
    modulo = 'transaction'
    @transaction = Cartao.new(@body, @hmac, modulo)
    return @body, @orderid
end