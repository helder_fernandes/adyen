def consultaBanco(id,status)

  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd, :port=>$port, :dbname=>$database, :user=>$username, :password=>$password)
  query = "
          select t.external_order_id,
          t.broker_id,
          s.name            status,
          m2.name           payment_method,
          m2.id			 payment_method_id,
          t.card_bin,
          t.card_saved_id,
          t.created_date,
          t.customer_id,
          t.customer_email,
          t.amount,
          --t.currency,
          t.processing_type
          --t.card_holder_name,
          --t.store_card,
          --t.additional_data transaction_json,
          --th.detail         --history_json
          from transactions t
          inner join transaction_status_history th on th.transaction_id = t.id :: text
          inner join status s on s.id = th.status_id
          inner join payment_methods m2 on t.payment_method_id = m2.id
          where t.id = '#{id}'
          order by th.created_date desc
        "
  # puts "Query do Banco: "
  # puts query
  # execução da query:
  @res = conn.exec(query)

  puts "Retorno do Banco: "
  puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  
  # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
  puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|"
  puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  @res.each do |row|
    
    # puts "|    %s     | %s |   %s   | %s | %s | %s |  %s  |    %s     |       %s        |" % [row['external_order_id'],row['status'],row['payment_method'],row['created_date'],row['customer_id'],row['customer_email'],row['amount'],row['processing_type'], row['card_saved_id']]
    # puts "|#{row['external_order_id'].center(21)}|#{row['status'].center(15)}|#{row['payment_method'].center(18)}|#{row['created_date'].center(33)}|#{row['customer_id'].center(24)}|#{row['customer_email'].center(36)}|#{row['amount'].center(8)}|#{row['processing_type'].center(19)}|#{row['card_saved_id'].center(17)}|"

    puts "|#{row['external_order_id'].center(21)}|#{row['status'].center(15)}|#{row['payment_method'].center(18)}|#{row['created_date'].center(33)}|#{row['customer_id'].center(24)}|#{row['customer_email'].center(36)}|#{row['amount'].center(8)}|#{row['processing_type'].center(19)}|"
    
    dados = {
            :external_order_id => row['external_order_id'],
            :broker_id => row['broker_id'],
            :status => row['status'],
            :payment_method => row['payment_method'],
            :payment_method_id => row['payment_method_id'],
            :card_bin => row['card_bin'],
            :card_saved_id => row['card_saved_id'],
            :created_date => row['created_date'],
            :customer_id => row['customer_id'],
            :customer_email => row['customer_email'],
            :amount => row['amount'],
            :currency => row['currency'],
            :processing_type => row['processing_type'],
            :card_holder_name => row['card_holder_name'],
            :store_card => row['store_card'],
            :transaction_json => row['transaction_json'],
            :history_json => row['history_json']
          }

    dados_arr.push(dados)

    # if row['status'] == "REFUNDED"
    #     @statusRefunded = row['status']
    # end
    
  end

  puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  
 

  puts "Validação dos dados da transação no banco: "
  puts "----------------------------------------------------------"
  puts "|#{$broker_id.center(21)}|#{$payment_method_id.center(21)}|#{$card_bin.center(12)}|"
  puts "----------------------------------------------------------"
  
  @res.each do |row|
    
    # "|     broker_id    | payment_method_id | card_bin |"

    # puts "| %s |         %s         |  %s  |" % [row['broker_id'],row['payment_method_id'],row['card_bin']]
    puts "|#{row['broker_id'].center(21)}|#{row['payment_method_id'].center(21)}|#{row['card_bin'].center(12)}|"

  end
  puts "----------------------------------------------------------"

  dados2 ={
    :status_autorizacao => status,
    :id => id
  }
  dados_arr.push(dados2)

  dados3 ={
    :query => query
  }
  dados_arr.push(dados3)
  
  return dados_arr, @statusRefunded

end 

def consultaBanco_status_refunded(id,status)

  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd, :port=>'5432', :dbname=>$database, :user=>$username, :password=>$password)
  query = "
          select t.external_order_id,
          t.broker_id,
          s.name            status,
          m2.name           payment_method,
          m2.id			 payment_method_id,
          t.card_bin,
          t.card_saved_id,
          t.created_date,
          t.customer_id,
          t.customer_email,
          t.amount,
          --t.currency,
          t.processing_type
          --t.card_holder_name,
          --t.store_card,
          --t.additional_data transaction_json,
          --th.detail         --history_json
          from transactions t
          inner join transaction_status_history th on th.transaction_id = t.id :: text
          inner join status s on s.id = th.status_id
          inner join payment_methods m2 on t.payment_method_id = m2.id
          where t.id = '#{id}'
          order by th.created_date desc
        "
  # puts "Query do Banco: "
  # puts query
  # execução da query:

  timeout = 120
  existe = false
  status_objetivo = "REFUNDED"
  begin
    Timeout.timeout(timeout) do   
      while not existe 
        @res = conn.exec(query)
        @res.each do |row|
          # puts row['status']
          if row['status'] == status_objetivo
            @statusRefunded = row['status']
            existe = true
            break
          end
        end
        sleep 5
      end
    end
  rescue Timeout::Error
      @erro_timeout = true
  end

  if @erro_timeout
    puts "-------------------------------------------------------------------------------------------"
    puts "Após uma espera de #{timeout} segundos o status '#{status_objetivo}' não foi alcançado..."
    puts "-------------------------------------------------------------------------------------------"
  else
    puts "Retorno do Banco: "
    # puts "Campos consultados:"
    puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
    
    puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|"
    puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    @res.each do |row|
      
      # puts "|#{row['external_order_id'].center(21)}|#{row['status'].center(15)}|#{row['payment_method'].center(18)}|#{row['created_date'].center(33)}|#{row['customer_id'].center(24)}|#{row['customer_email'].center(36)}|#{row['amount'].center(8)}|#{row['processing_type'].center(19)}|#{row['card_saved_id'].center(17)}|"
      puts "|#{row['external_order_id'].center(21)}|#{row['status'].center(15)}|#{row['payment_method'].center(18)}|#{row['created_date'].center(33)}|#{row['customer_id'].center(24)}|#{row['customer_email'].center(36)}|#{row['amount'].center(8)}|#{row['processing_type'].center(19)}|"
      
      dados = {
              :external_order_id => row['external_order_id'],
              :broker_id => row['broker_id'],
              :status => row['status'],
              :payment_method => row['payment_method'],
              :payment_method_id => row['payment_method_id'],
              :card_bin => row['card_bin'],
              :card_saved_id => row['card_saved_id'],
              :created_date => row['created_date'],
              :customer_id => row['customer_id'],
              :customer_email => row['customer_email'],
              :amount => row['amount'],
              :currency => row['currency'],
              :processing_type => row['processing_type'],
              :card_holder_name => row['card_holder_name'],
              :store_card => row['store_card'],
              :transaction_json => row['transaction_json'],
              :history_json => row['history_json']
            }
  
      dados_arr.push(dados)
  
      # if row['status'] == "REFUNDED"
      #     @statusRefunded = row['status']
      # end
      
    end
  
    puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    
    puts "Validação dos dados da transação no banco: "
    puts "----------------------------------------------------------"
    puts "|#{$broker_id.center(21)}|#{$payment_method_id.center(21)}|#{$card_bin.center(12)}|"
    puts "----------------------------------------------------------"
    
    @res.each do |row|
      
      # "|     broker_id    | payment_method_id | card_bin |"
  
      puts "|#{row['broker_id'].center(21)}|#{row['payment_method_id'].center(21)}|#{row['card_bin'].center(12)}|"
  
    end
    puts "---------------------------------------------------"
  
  end

  @res = conn.exec(query)

  dados2 ={
    :status_autorizacao => status,
    :id => id
  }
  dados_arr.push(dados2)

  dados3 ={
    :query => query
  }
  dados_arr.push(dados3)
  
  return dados_arr, @statusRefunded, @erro_timeout

end 

def consultaBancoStatusCaptured(id,status)
  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd, :port=>'5432', :dbname=>$database, :user=>$username, :password=>$password)
  # or for a non IP address :host => 'my.host.name.com' instead of hostaddr
  # count = 0
  query = "
          select t.external_order_id,
          --t.broker_id,
          s.name            status,
          m2.name           payment_method,
          --t.card_bin,
          t.card_saved_id,
          t.created_date,
          t.customer_id,
          t.customer_email,
          t.amount,
          --t.currency,
          t.processing_type
          --t.card_holder_name,
          --t.store_card,
          --t.additional_data transaction_json,
          --th.detail         --history_json
          from transactions t
          inner join transaction_status_history th on th.transaction_id = t.id :: text
          inner join status s on s.id = th.status_id
          inner join payment_methods m2 on t.payment_method_id = m2.id
          where t.id = '#{id}'
          order by th.created_date desc
          "

          
  timeout = 120
  existe = false
  status_objetivo = "CAPTURED"
  begin
    Timeout.timeout(timeout) do   
        while not existe 
          @res = conn.exec(query)
          @res.each do |row|
            if row['status'] == status_objetivo
              existe = true
              break
            end
            # STDOUT.puts row['status']
          end
            sleep 5
        end
    end
  rescue Timeout::Error
      @erro_timeout = true
  end

  if @erro_timeout
    puts "-------------------------------------------------------------------------------------------"
    puts "Após uma espera de #{timeout} segundos o status '#{status_objetivo}' não foi alcançado..."
    puts "-------------------------------------------------------------------------------------------"

  else
    puts "Retorno do Banco: "
    puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
    puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|"
    puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  @res.each do |row|
    
    # puts "|#{row['external_order_id'].center(21)}|#{row['status'].center(15)}|#{row['payment_method'].center(18)}|#{row['created_date'].center(33)}|#{row['customer_id'].center(24)}|#{row['customer_email'].center(36)}|#{row['amount'].center(8)}|#{row['processing_type'].center(19)}|#{row['card_saved_id'].center(17)}|"
    puts "|#{row['external_order_id'].center(21)}|#{row['status'].center(15)}|#{row['payment_method'].center(18)}|#{row['created_date'].center(33)}|#{row['customer_id'].center(24)}|#{row['customer_email'].center(36)}|#{row['amount'].center(8)}|#{row['processing_type'].center(19)}|"

      
        # puts "| %s |    %s    | %s | %s | %s | %s | %s | %s | %s |" % [ row['external_order_id'],row['status'],row['payment_method'],row['created_date'],row['customer_id'],row['customer_email'],row['amount'],row['processing_type'], row['card_saved_id']]
        # row['external_order_id'],

        dados = {
         :external_order_id => row['external_order_id'],
         :broker_id => row['broker_id'],
         :status => row['status'],
         :payment_method => row['payment_method'],
         :card_bin => row['card_bin'],
         :card_saved_id => row['card_saved_id'],
         :created_date => row['created_date'],
         :customer_id => row['customer_id'],
         :customer_email => row['customer_email'],
         :amount => row['amount'],
         :currency => row['currency'],
         :processing_type => row['processing_type'],
         :card_holder_name => row['card_holder_name'],
         :store_card => row['store_card'],
         :transaction_json => row['transaction_json'],
         :history_json => row['history_json']
        }
        
        dados_arr.push(dados)
        if row['status'] == "REFUNDED"
            @statusRefunded = row['status']
        end
    end
    puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"  
  end

      dados2 ={
        :status_autorizacao => status,
        :id => id
      }
      dados_arr.push(dados2)

      dados3 ={
        :query => query
      }
      dados_arr.push(dados3)
      
      return dados_arr, @statusRefunded
end

def updateBanco(id, valor)
  conn=PG.connect(:host=>$host_bd, :port=>'5432', :dbname=>$database, :user=>$username, :password=>$password)

  # run the query
  query_update = "
      update transactions
      set amount = #{valor}
      where id = '#{id}';
      "

  conn.exec(query_update)

  query_consulta = "
      select amount from transactions
      where id = '#{id}';
      "

  res2 = conn.exec(query_consulta)
  
  res2.each do |x, y|
    @novoValor = x['amount']
    # puts @idRetorno = y['']
  end
  
  # conn2.close
  
  return @novoValor

end

def consultaBancoExternalId(orderId,status)


    order_arr = Array.new
    conn=PG.connect(:host=>$host_bd, :port=>'5432', :dbname=>$database, :user=>$username, :password=>$password)
    
    query = "
    select *
    from transactions 
    where external_order_id = '#{orderId}';
    "
    
    # run the query
    @res = conn.exec(query)

    
    puts "Retorno do Banco: "
    puts "------------------------------------------------------------------------------------------------------------------------------------------------------------"
    @res.each do |row|   
  
        puts "| %s | %s | %s | %s | %s | %s | %s | %s | %s |" % [row['id'], row['external_order_id'],row['external_order_id'],row['external_payment_id'],row['created_date'],row['customer_email'],row['processing_type'],row['customer_id'],row['payment_method_id'],row['broker_id']]
        # row['external_order_id'],

        data = {
         :id => row['id'],
         :external_order_id => row['external_order_id'],
         :external_payment_id => row['external_payment_id'],
         :created_date => row['created_date'],
         :customer_email => row['customer_email'],
         :processing_type => row['processing_type'],
         :customer_id => row['customer_id'],
         :payment_method_id => row['payment_method_id'],
         :broker_id => row['broker_id'],
        
        }

        order_arr.push(data)
        @statusId = row['id']
      end
      puts "------------------------------------------------------------------------------------------------------------------------------------------------------------"    
      
      query2 = "select * from transaction_status_history where transaction_id = '#{@statusId}'"

      res2 = conn.exec(query2)
      res2.each do |row|
      puts "| %s | %s | %s | %s |" % [row['id'], row['transaction_id'], row['status_id'], row['created_date'], row['detail']]
      if row['status_id'] == "2"
        @statusRefunded = row['status_id']
      end
      end
      
      return order_arr
end

def consultaBanco_status(transaction_id)
    @statusId = Array.new
    conn=PG.connect(:host=>$host_bd, :port=>'5432', :dbname=>$database, :user=>$username, :password=>$password)
    # or for a non IP address :host => 'my.host.name.com' instead of hostaddr

    query = "
      select * from 
      transaction_status_history th
      where th.transaction_id =  (select cast (t.id as text) from transactions t where t.external_order_id = '#{transaction_id}')
      order by created_date asc;
    "
    # puts "------------"
    # puts query
    # puts "------------"
    
    # run the query
    @res = conn.exec(query)

    puts "Retorno do Banco: "
    puts "------------------------------------------------------------------------------------------------------------------------------------------------------------"
    @res.each do |row|   

      # puts "| %s |   %s  |   %s   |     %s     |  %s  |" % [row['id'], row['transaction_id'],row['status_id'],row['created_date'],row['detail']]
      puts "| %s |   %s  |   %s   | %s |" % [row['id'], row['transaction_id'],row['status_id'],row['created_date']]
      # row['external_order_id'],
      
      
      if row['status_id'] == '11'
        @statusId.push(row['status_id'])
      end
      @details = row['detail']
      @transactionId = row['transaction_id']

      # # permanece desligado até segunda ordem:
      # puts "----------------"
      # puts "Body do Details:"
      # obj = JSON.pretty_generate(JSON.parse(@details))
      # puts obj

      # puts "----------------"
    end    
    puts "------------------------------------------------------------------------------------------------------------------------------------------------------------"
    
    return @statusId, @transactionId
end

def consultaBancoStatusChargeback(id,status)

    dados_arr = Array.new
    conn=PG.connect(:host=>$host_bd, :port=>'5432', :dbname=>$database, :user=>$username, :password=>$password)
    
    query = "
    select t.external_order_id,
    --t.broker_id,
    s.name            status,
    m2.name           payment_method,
    --t.card_bin,
    t.card_saved_id,
    t.created_date,
    t.customer_id,
    t.customer_email,
    t.amount,
    --t.currency,
    t.processing_type
    --t.card_holder_name,
    --t.store_card,
    --t.additional_data transaction_json,
    --th.detail         --history_json
    from transactions t
    inner join transaction_status_history th on th.transaction_id = t.id :: text
    inner join status s on s.id = th.status_id
    inner join payment_methods m2 on t.payment_method_id = m2.id
    where t.id = '#{id}'
    order by th.created_date desc
    "

    timeout = 120
    existe = false
    status_objetivo = "CHARGEBACK"
    begin
      Timeout.timeout(timeout) do   
          while not existe 
            @res = conn.exec(query)
            @res.each do |row|
              if row['status'] == status_objetivo
                existe = true
                break
              end
              # STDOUT.puts row['status']
            end
              sleep 5
          end
      end
    rescue Timeout::Error
        @erro_timeout = true
    end

    if @erro_timeout
      puts "-------------------------------------------------------------------------------------------"
      puts "Após uma espera de #{timeout} segundos o status '#{status_objetivo}' não foi alcançado..."
      puts "-------------------------------------------------------------------------------------------"
  
    else

      puts "Retorno do Banco: "
      puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
      # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
      puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|"
      puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

      @res.each do |row|
        
        # puts "| %s | %s | %s | %s | %s | %s | %s | %s | %s |" % [ row['external_order_id'],row['status'],row['payment_method'],row['created_date'],row['customer_id'],row['customer_email'],row['amount'],row['processing_type'], row['card_saved_id']]
        # puts "|#{row['external_order_id'].center(21)}|#{row['status'].center(15)}|#{row['payment_method'].center(18)}|#{row['created_date'].center(33)}|#{row['customer_id'].center(24)}|#{row['customer_email'].center(36)}|#{row['amount'].center(8)}|#{row['processing_type'].center(19)}|#{row['card_saved_id'].center(17)}|"
        puts "|#{row['external_order_id'].center(21)}|#{row['status'].center(15)}|#{row['payment_method'].center(18)}|#{row['created_date'].center(33)}|#{row['customer_id'].center(24)}|#{row['customer_email'].center(36)}|#{row['amount'].center(8)}|#{row['processing_type'].center(19)}|"
          # row['external_order_id'],

          dados = {
          :external_order_id => row['external_order_id'],
          :broker_id => row['broker_id'],
          :status => row['status'],
          :payment_method => row['payment_method'],
          :card_bin => row['card_bin'],
          :card_saved_id => row['card_saved_id'],
          :created_date => row['created_date'],
          :customer_id => row['customer_id'],
          :customer_email => row['customer_email'],
          :amount => row['amount'],
          :currency => row['currency'],
          :processing_type => row['processing_type'],
          :card_holder_name => row['card_holder_name'],
          :store_card => row['store_card'],
          :transaction_json => row['transaction_json'],
          :history_json => row['history_json']
          }
          
          dados_arr.push(dados)
          if row['status'] == "CHARGEBACK"
              @statusChargeback = row['status']
          end
        end
        puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
      end

      dados2 ={
        :status_autorizacao => status,
        :id => id
      }
      dados_arr.push(dados2)

      dados3 ={
        :query => query
      }
      dados_arr.push(dados3)
      
      return dados_arr, @statusChargeback
end

def consultaBancoStatusDenied(id,status)

  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd, :port=>'5432', :dbname=>$database, :user=>$username, :password=>$password)
  # or for a non IP address :host => 'my.host.name.com' instead of hostaddr
  # count = 0
  query = "
  select t.external_order_id,
  --t.broker_id,
  s.name            status,
  m2.name           payment_method,
  --t.card_bin,
  t.card_saved_id,
  t.created_date,
  t.customer_id,
  t.customer_email,
  t.amount,
  --t.currency,
  t.processing_type
  --t.card_holder_name,
  --t.store_card,
  --t.additional_data transaction_json,
  --th.detail         --history_json
  from transactions t
  inner join transaction_status_history th on th.transaction_id = t.id :: text
  inner join status s on s.id = th.status_id
  inner join payment_methods m2 on t.payment_method_id = m2.id
  where t.id = '#{id}'
  order by th.created_date desc
  "
  
  timeout = 240
  existe = false
  status_objetivo = "DENIED"
  begin
    Timeout.timeout(timeout) do   
        while not existe 
          @res = conn.exec(query)
          @res.each do |row|
            if row['status'] == status_objetivo
              existe = true
              break
            end
            # STDOUT.puts row['status']
          end
            sleep 5
        end
    end
  rescue Timeout::Error
      @erro_timeout = true
  end

  if @erro_timeout
    puts "-------------------------------------------------------------------------------------------"
    puts "Após uma espera de #{timeout} segundos o status '#{status_objetivo}' não foi alcançado..."
    puts "-------------------------------------------------------------------------------------------"

  else
    puts "Retorno do Banco: "
    puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
    puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|"
    puts "---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

    @res.each do |row|
      
        # puts "| %s | %s | %s | %s | %s | %s | %s | %s | %s |" % [ row['external_order_id'],row['status'],row['payment_method'],row['created_date'],row['customer_id'],row['customer_email'],row['amount'],row['processing_type'], row['card_saved_id']]
        puts "|#{row['external_order_id'].center(21)}|#{row['status'].center(15)}|#{row['payment_method'].center(18)}|#{row['created_date'].center(33)}|#{row['customer_id'].center(24)}|#{row['customer_email'].center(36)}|#{row['amount'].center(8)}|#{row['processing_type'].center(19)}|"
        # row['external_order_id'],

        dados = {
          :external_order_id => row['external_order_id'],
          :broker_id => row['broker_id'],
          :status => row['status'],
          :payment_method => row['payment_method'],
          :card_bin => row['card_bin'],
          :card_saved_id => row['card_saved_id'],
          :created_date => row['created_date'],
          :customer_id => row['customer_id'],
          :customer_email => row['customer_email'],
          :amount => row['amount'],
          :currency => row['currency'],
          :processing_type => row['processing_type'],
          :card_holder_name => row['card_holder_name'],
          :store_card => row['store_card'],
          :transaction_json => row['transaction_json'],
          :history_json => row['history_json']
        }
        
        dados_arr.push(dados)
        if row['status'] == "DENIED"
            @statusDenied = row['status']
        end
      end
    end

    dados2 ={
      :status_autorizacao => status,
      :id => id
    }
    dados_arr.push(dados2)

    dados3 ={
      :query => query
    }
    dados_arr.push(dados3)
    
    return dados_arr, @statusDenied, @erro_timeout
end

def consultaBanco_coupon_crm(id_coupon)

  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd_crm, :port=>'5432', :dbname=>$database_crm, :user=>$username_crm, :password=>$password_crm)
  query = "
            select * from coupon where id = '#{id_coupon}'"
  # puts "Query do Banco: "
  # puts query
  # execução da query:
  @res = conn.exec(query)

  puts "Retorno do Banco: "
  puts "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  
  # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
  puts "|#{$id.center(42)}|#{$external_id.center(42)}|#{$part_code.center(24)}|#{$created_at.center(36)}|#{$disabled_at.center(36)}|#{$app_id.center(42)}|"
  puts "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  @res.each do |row|
    
    
    puts "|#{row['id'].center(42)}|#{row['external_id'].center(42)}|#{row['part_code'].center(24)}|#{row['created_at'].center(36)}|             #{row['disabled_at']}                       |#{row['app_id'].center(42)}|"
    
    dados = {
            :id => row['id'],
            :rules => row['rules'],
            :external_id => row['external_id'],
            :coupon_info => row['coupon_info'],
            :part_code => row['part_code'],
            :created_at => row['created_at'],
            :disabled_at => row['disabled_at'],
            :app_id => row['app_id']
          }

    dados_arr.push(dados)

    puts "Retorno Colunas Rules e Coupon_info:"
    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    puts "|#{$rules.center(15)}|#{row['rules'].center(15)}|"
    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    puts "|#{$coupon_info.center(15)}|#{row['coupon_info'].center(15)}|"
    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  end

  return dados_arr

end 

def consultaBanco_coupon_crm_deleted(id_coupon)

  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd_crm, :port=>'5432', :dbname=>$database_crm, :user=>$username_crm, :password=>$password_crm)
  query = "
            select * from coupon where id = '#{id_coupon}'"
  # puts "Query do Banco: "
  # puts query
  # execução da query:
  @res = conn.exec(query)

  puts "Retorno do Banco: "
  puts "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  
  # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
  puts "|#{$id.center(42)}|#{$external_id.center(42)}|#{$part_code.center(24)}|#{$created_at.center(36)}|#{$disabled_at.center(36)}|#{$app_id.center(36)}|"
  puts "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  @res.each do |row|
    
    
    puts "|#{row['id'].center(42)}|#{row['external_id'].center(42)}|#{row['part_code'].center(24)}|#{row['created_at'].center(36)}|#{row['disabled_at'].center(36)}|#{row['app_id'].center(19)}|"
    
    dados = {
            :id => row['id'],
            :rules => row['rules'],
            :external_id => row['external_id'],
            :coupon_info => row['coupon_info'],
            :part_code => row['part_code'],
            :created_at => row['created_at'],
            :disabled_at => row['disabled_at'],
            :app_id => row['app_id']
          }

    dados_arr.push(dados)

    puts "Retorno Colunas Rules e Coupon_info:"
    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    puts "|#{$rules.center(15)}|#{row['rules'].center(15)}|"
    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    puts "|#{$coupon_info.center(15)}|#{row['coupon_info'].center(15)}|"
    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  end

  return dados_arr

end 

def consultaBanco_coupon_crm_deleted1(id_coupon)

  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd_crm, :port=>'5432', :dbname=>$database_crm, :user=>$username_crm, :password=>$password_crm)
  query = "
            select * from coupon where id = '#{id_coupon}'"
  # puts "Query do Banco: "
  # puts query
  # execução da query:
  @res = conn.exec(query)

  puts "Retorno do Banco: "
  puts "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  
  # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
  puts "|#{$id.center(42)}|#{$external_id.center(42)}|#{$part_code.center(24)}|#{$created_at.center(36)}|#{$app_id.center(42)}|"
  puts "-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  @res.each do |row|
    
    
    puts "|#{row['id'].center(42)}|#{row['external_id'].center(42)}|#{row['part_code'].center(24)}|#{row['created_at'].center(36)}|#{row['app_id'].center(42)}|"
    
    dados = {
            :id => row['id'],
            :rules => row['rules'],
            :external_id => row['external_id'],
            :coupon_info => row['coupon_info'],
            :part_code => row['part_code'],
            :created_at => row['created_at'],
            :disabled_at => row['disabled_at'],
            :app_id => row['app_id']
          }

    dados_arr.push(dados)

    puts "Retorno Colunas Rules e Coupon_info:"
    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    puts "|#{$rules.center(15)}|#{row['rules'].center(15)}|"
    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    puts "|#{$coupon_info.center(15)}|#{row['coupon_info'].center(15)}|"
    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  end

  return dados_arr

end

def consultaBanco_crm_external_id(external_id)

  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd_crm, :port=>'5432', :dbname=>$database_crm, :user=>$username_crm, :password=>$password_crm)
  query = "
      select * from coupon where external_id = '#{external_id}'"
  # puts "Query do Banco: "
  # puts query
  # execução da query:
  @res = conn.exec(query)

  puts "Retorno do Banco: "
  puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  
  # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
  puts "|#{$id.center(42)}|#{$external_id.center(42)}|#{$part_code.center(24)}|#{$created_at.center(36)}|#{$app_id.center(36)}|"
  puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  @res.each do |row|
    
    
    puts "|#{row['id'].center(42)}|#{row['external_id'].center(42)}|#{row['part_code'].center(24)}|#{row['created_at'].center(36)}|#{row['app_id'].center(36)}|"
    
    dados = {
            :id => row['id'],
            :rules => row['rules'],
            :external_id => row['external_id'],
            :coupon_info => row['coupon_info'],
            :part_code => row['part_code'],
            :created_at => row['created_at'],
            :disabled_at => row['disabled_at'],
            :app_id => row['app_id']
          }

    dados_arr.push(dados)

    puts "Retorno Colunas Rules e Coupon_info:"
    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    puts "|#{$rules.center(15)}|#{row['rules'].center(15)}|"
    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    puts "|#{$coupon_info.center(15)}|#{row['coupon_info'].center(15)}|"
    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  end

  return dados_arr

end 


def consultaBanco_crm_instance_id(instance_id)

  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd_crm, :port=>'5432', :dbname=>$database_crm, :user=>$username_crm, :password=>$password_crm)
  query = "
      select * from instance where id = '#{instance_id}'"
  # puts "Query do Banco: "
  # puts query
  # execução da query:
  @res = conn.exec(query)

  puts "Retorno do Banco: "
  puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  
  # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
  puts "|#{$id.center(42)}|#{$coupon_id.center(42)}|#{$customer_id.center(42)}|#{$created_at.center(36)}|#{$reference.center(36)}|#{$offline_data.center(36)}|#{$purged.center(36)}|#{$customer_info.center(36)}|#{$locked_for.center(36)}|#{$locked_until.center(18)}|#{$expires_at.center(18)}|"
  puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  @res.each do |row|
    
    
    puts "|#{row['id'].center(42)}|#{row['coupon_id'].center(42)}|#{row['customer_id'].center(42)}|#{row['created_at'].center(36)}|#{row['reference'].center(19)}|#{row['offline_data'].center(19)}|#{row['purged'].center(19)}|#{row['customer_info'].center(19)}|   #{row['locked_for']}   |      #{row['locked_until']}     |     #{row['expires_at']}     |"
    
    dados = {
            :id => row['id'],
            :coupon_id => row['coupon_id'],
            :customer_id => row['customer_id'],
            :created_at => row['created_at'],
            :reference => row['reference'],
            :offline_data => row['offline_data'],
            :purged => row['purged'],
            :customer_info => row['customer_info'],
            :locked_for => row['locked_for'],
            :locked_since => row['locked_until'],
            :expires_at => row['expires_at']
          }

    dados_arr.push(dados)

    # puts "Retorno Colunas Rules e Coupon_info:"
    # puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    # puts "|#{$rules.center(15)}|#{row['rules'].center(15)}|"
    # puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    # puts "|#{$coupon_info.center(15)}|#{row['coupon_info'].center(15)}|"
    # puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  end

  return dados_arr

end 

def consultaBanco_crm_instance_id_coupon_id(customer_id, coupon_id)

  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd_crm, :port=>'5432', :dbname=>$database_crm, :user=>$username_crm, :password=>$password_crm)
  query = "
          select * from instance where customer_id = '#{customer_id}' and coupon_id = '#{coupon_id}'"
  # puts "Query do Banco: "
  # puts query
  # execução da query:
  @res = conn.exec(query)

  puts "Retorno do Banco: "
  puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  
  # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
  puts "|#{$id.center(42)}|#{$coupon_id.center(42)}|#{$customer_id.center(24)}|#{$created_at.center(36)}|#{$reference.center(36)}|#{$offline_data.center(36)}|#{$purged.center(36)}|#{$customer_info.center(36)}|"
  puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  @res.each do |row|
    
    
    puts "|#{row['id'].center(42)}|#{row['coupon_id'].center(42)}|#{row['customer_id'].center(24)}|#{row['created_at'].center(36)}|#{row['reference'].center(19)}|#{row['offline_data'].center(19)}|#{row['purged'].center(19)}|#{row['customer_info'].center(19)}|"
    
    dados = {
            :id => row['id'],
            :coupon_id => row['coupon_id'],
            :customer_id => row['customer_id'],
            :created_at => row['created_at'],
            :reference => row['reference'],
            :offline_data => row['offline_data'],
            :purged => row['purged'],
            :customer_info => row['customer_info'],
            :locked_for => row['locked_for'],
            :locked_since => row['locked_since'],
            :expires_at => row['expires_at']
          }

    dados_arr.push(dados)

    # puts "Retorno Colunas Rules e Coupon_info:"
    # puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    # puts "|#{$rules.center(15)}|#{row['rules'].center(15)}|"
    # puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    # puts "|#{$coupon_info.center(15)}|#{row['coupon_info'].center(15)}|"
    # puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  end

  return dados_arr

end

def consultaBanco_crm_instance_consumed(instance_id)

  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd_crm, :port=>'5432', :dbname=>$database_crm, :user=>$username_crm, :password=>$password_crm)
  query = "
      select * from consumed where instance_id = '#{instance_id}'"
  # puts "Query do Banco: "
  # puts query
  # execução da query:
  @res = conn.exec(query)

  puts "Retorno do Banco: "
  puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  
  # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
  puts "|#{$id.center(42)}|#{$instance_id.center(42)}|#{$consumed_at.center(24)}|#{$sync_id.center(36)}|#{$pos_id.center(36)}|"
  puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  @res.each do |row|
    
    
    puts "|#{row['id'].center(42)}|#{row['instance_id'].center(42)}|#{row['consumed_at'].center(24)}|                        #{row['sync_id']}            |#{row['pos_id'].center(36)}|"
    
    dados = {
            :id => row['id'],
            :instance_id => row['instance_id'],
            :consumed_at => row['consumed_at'],
            :sync_id => row['sync_id'],
            :pos_id => row['pos_id']
          }

    dados_arr.push(dados)

    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  end

  return dados_arr

end 

def consultaBanco_crm_app_api_key()

  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd_crm, :port=>'5432', :dbname=>$database_crm, :user=>$username_crm, :password=>$password_crm)
  query = "
      select * from app_api_key where api_key = 'c819af3d-0eb7-4954-8def-06b1d0a09f2e'"
  # puts "Query do Banco: "
  # puts query
  # execução da query:
  @res = conn.exec(query)

  puts "Retorno do Banco: "
  puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  
  # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
  puts "|#{$app_id.center(42)}|#{$api_key.center(42)}|#{$created_at.center(26)}|#{$disabled_at.center(36)}|"
  puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  @res.each do |row|
    

    puts "|#{row['app_id'].center(42)}|#{row['api_key'].center(42)}|#{DateTime.parse(row['created_at']).strftime("%Y-%m-%d %H:%M:%S").center(26)}|#{row['disabled_at'].center(36)}|"
    
    dados = {
            :app_id => row['app_id'],
            :api_key => row['api_key'],
            :created_at => row['created_at'],
            :disabled_at => row['disabled_at']
          }

    dados_arr.push(dados)

    puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  end

  return dados_arr

end


def consultaBanco_crm_reference_id(reference)

  dados_arr = Array.new
  conn=PG.connect(:host=>$host_bd_crm, :port=>'5432', :dbname=>$database_crm, :user=>$username_crm, :password=>$password_crm)
  query = "
      select * from instance where reference = '#{reference}'"
  # puts "Query do Banco: "
  # puts query
  # execução da query:
  @res = conn.exec(query)

  puts "Retorno do Banco: "
  puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
  
  # puts "|#{$ext_order_id.center(21)}|#{$status.center(15)}|#{$payment_method.center(18)}|#{$created_date.center(33)}|#{$customer_id.center(24)}|#{$customer_email.center(36)}|#{$amount.center(8)}|#{$process_type.center(19)}|#{$card_save_id.center(17)}|"
  puts "|#{$id.center(42)}|#{$coupon_id.center(42)}|#{$customer_id.center(42)}|#{$created_at.center(36)}|#{$reference.center(36)}|#{$offline_data.center(36)}|#{$purged.center(36)}|#{$customer_info.center(36)}|#{$locked_for.center(36)}|#{$locked_until.center(18)}|#{$expires_at.center(18)}|"
  puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  @res.each do |row|
    
    
    puts "|#{row['id'].center(42)}|#{row['coupon_id'].center(42)}|#{row['customer_id'].center(42)}|#{row['created_at'].center(36)}|#{row['reference'].center(19)}|#{row['offline_data'].center(19)}|#{row['purged'].center(19)}|#{row['customer_info'].center(19)}|   #{row['locked_for']}   |      #{row['locked_until']}     |     #{row['expires_at']}     |"
    
    dados = {
            :id => row['id'],
            :coupon_id => row['coupon_id'],
            :customer_id => row['customer_id'],
            :created_at => row['created_at'],
            :reference => row['reference'],
            :offline_data => row['offline_data'],
            :purged => row['purged'],
            :customer_info => row['customer_info'],
            :locked_for => row['locked_for'],
            :locked_since => row['locked_until'],
            :expires_at => row['expires_at']
          }

    dados_arr.push(dados)

    # puts "Retorno Colunas Rules e Coupon_info:"
    # puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    # puts "|#{$rules.center(15)}|#{row['rules'].center(15)}|"
    # puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"
    # puts "|#{$coupon_info.center(15)}|#{row['coupon_info'].center(15)}|"
    # puts "-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------"

  end

  return dados_arr

end