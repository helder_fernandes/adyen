class Metodo_Pagamento
    include HTTParty  
        base_uri 'https://pf6hyb99x4.execute-api.us-east-1.amazonaws.com/stage'

    def initialize()
    
        @getData = {
            # :headers => $auth,
            :body => {}
        }    
      
    end

    def get_metodo_pagamento()
        #método get que traz os cartões cadastrados na base.
        self.class.get("/paymentMethods", @getData)
    end
end