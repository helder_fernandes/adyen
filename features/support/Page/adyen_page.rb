Dir[File.join(File.dirname(__FILE__), './features/support/Page/*_page.rb')].each { |file| require file} 
class AdyenPage
    include Capybara::DSL

    def elements_adyen_page
        {
            labelTest: "//*[@id='subcontent']/div/div[1]/form/div[2]/div[1]/div[1]/a[2]",
            account:   "//input[@name='j_account']",
            username:  "j_username",
            password:  "j_password",
            btnLogin:  "//*[@id='subcontent']/div/div[1]/form/div[2]/div[1]/div[8]/input",
            transactions: '/html/body/div[1]/div[1]/div[2]/nav/div[1]/div[1]',
            payments:  '/html/body/div[1]/div[1]/div[2]/nav/div[1]/div[2]/div/div[1]/div/div[3]/a/span',
            busca: '.csr3-input-text.transparent.large',
            busca2: 'csr3-input-text.transparent.u-width-full.csr3-valid-field',
            linkOrder: '//*[@id="paymentTable"]/tr/td[1]/a',
            iconUser: '/html/body/div[1]/div[1]/div[6]',
            signoff: '/html/body/div[1]/div[1]/div[6]/div[2]/div[5]/a',
            # stored_payment: '//*[@id="ca-payments-cardstyle"]/div[3]/div[2]/div/div[15]/a'
            stored_payment: '//*[@id="ca-payments-cardstyle"]/div[3]/div[2]/div/div[7]/a',
            editTest: '//*[@id="subcontent"]/div/table[1]/tbody/tr[1]/td[7]/form/input[1]',
            custom: '//*[@id="configure-third-party"]/div[5]/div/table/tbody/tr[12]/th/a',
            eventCode: '//*[@id="notification_custom"]/div[1]/select',
            pspReference: '//*[@id="notification_custom"]/div[2]/input',
            reference: '//*[@id="notification_custom"]/div[3]/input',
            orderId: '//*[@id="notification_custom"]/div[4]/input',
            eventDate: '//*[@id="notification_custom"]/div[5]/input',
            statusSucess: '//*[@id="notification_custom"]/div[6]/input[1]',
            paymentMethod: '//*[@id="notification_custom"]/div[8]/input',
            reason: '//*[@id="notification_custom"]/div[9]/input',
            testConfig: '//*[@id="configure-third-party"]/div[5]/div/input',
            switchbox: '//*[@id="configure-third-party"]/div[5]/div/table/tbody/tr[12]/td/label/span/em',
            amount: '//*[@id="notification_custom"]/div[7]/input'

        }
    end

    def login(login)
        visit $pagina_adyen_login

        # $adyen_login.each do |x|
        #     @login = x[:account]
        #     @username = x[:login]
        #     @password = x[:password]
        # end

        find(:xpath, elements_adyen_page[:labelTest]).click
        $adyen_login.each do |x|
            find(:xpath, elements_adyen_page[:account]).set x[:account]
            find(:id, elements_adyen_page[:username]).set x[:login]
            find(:id, elements_adyen_page[:password]).set x[:password]
        end
        find(:xpath, elements_adyen_page[:btnLogin]).click
        
    end

    def openPayments()
  
        visit $pagina_adyen_payments
        # find(:xpath, elements_adyen_page[:transactions]).click
        # find(:xpath, elements_adyen_page[:payments]).click
        
    end

    def busca_cartao()
        @pspReference = find(:xpath, "//*[@id='paymentTable']/tr[1]/td[1]")
        reference = @pspReference.text

        visit "https://ca-test.adyen.com/ca/ca/accounts/showTx.shtml?txType=Payment&pspReference=#{@pspReference.text}&accountKey=MerchantAccount.BKBrasilAPP" 
        

        # orderAdyen = find(:xpath, "//span[@title='#{orderid}']")
        # puts "O Order id presente na Adyen é: #{orderAdyen.text}"         
        return reference
    end

    def consulta_ordens_duplicadas(order)
        # sleep(18)
        
        found = false
    
        find(elements_adyen_page[:busca]).set order
        find(elements_adyen_page[:busca]).send_keys(:enter)
        
        found = has_xpath?("//span[@title='#{order}']", count: 2)

        if not found
            while not found
                visit $pagina_adyen_payments
                find(elements_adyen_page[:busca]).set order
                find(elements_adyen_page[:busca]).send_keys(:enter)

                found = has_xpath?("//span[@title='#{order}']", count: 2)
                if found == true
                    page.assert_text("#{order}", count: 2)
                    break
                end
            end
            # orderAdyen = find(:xpath, "//span[@title='#{order}']")
        else
            page.assert_text("#{order}", count: 2)
        end

    end

    def consulta_ordens_SettledInInstallments(order)
        # sleep(18)
        
        found = false
    
        find(elements_adyen_page[:busca]).set order
        find(elements_adyen_page[:busca]).send_keys(:enter)
        
        # found = has_xpath?("//span[@title='#{order}']", count: 2)
        found = has_xpath?("//td[@title='SettledInInstallments']", count: 1)

        if not found
            while not found
                visit $pagina_adyen_payments
                find(elements_adyen_page[:busca]).set order
                find(elements_adyen_page[:busca]).send_keys(:enter)

                found = has_xpath?("//td[@title='SettledInInstallments']", count: 1)
                if found == true
                    # page.assert_text("#{order}", count: 2)
                    page.assert_text("SettledInInstallments", count: 1)
                    break
                end
            end
            # orderAdyen = find(:xpath, "//span[@title='#{order}']")
        else
            page.assert_text("SettledInInstallments", count: 1)
        end

    end

    def consultOrderPayment(order)
        # sleep(18)
        
        found = false
        orderAdyen = ''

        find(elements_adyen_page[:busca]).set order
        find(elements_adyen_page[:busca]).send_keys(:enter)
        
        found = has_xpath?("//span[@title='#{order}']")

        if not found
            while not found
                visit $pagina_adyen_payments
                find(elements_adyen_page[:busca]).set order
                find(elements_adyen_page[:busca]).send_keys(:enter)

                found = has_xpath?("//span[@title='#{order}']")
                if found == true
                    orderAdyen = find(:xpath, "//span[@title='#{order}']")
                    break
                end
            end
        else
            orderAdyen = find(:xpath, "//span[@title='#{order}']")
        end
        return orderAdyen
    end

    def openStoredPaymentDetails()
        find(:xpath, elements_adyen_page[:stored_payment]).click
        
    end

    def openOrder
        find(:xpath, elements_adyen_page[:linkOrder]).click
    end

    def logout
        find(:xpath, elements_adyen_page[:iconUser]).click
        find(:xpath, elements_adyen_page[:signoff]).click
    end  

    def openChageBackPage(login_adyen)
        login(login_adyen)
        sleep(5)
        visit 'https://ca-test.adyen.com/ca/ca/config/showthirdparty.shtml'
        sleep(3)
        find(:xpath, elements_adyen_page[:editTest]).click
    end

    def doingChargeBack(charge_back_dados)
    # def doingChargeBack(orderId, pspReference, originalReference, eventDate, amount, paymentMethod, reason)
        find(:xpath, elements_adyen_page[:custom]).click
        find(:xpath, elements_adyen_page[:switchbox]).click
        find('option[value="CHARGEBACK"]').select_option
        find(:xpath, elements_adyen_page[:pspReference]).set charge_back_dados.pspReference
        find(:xpath, elements_adyen_page[:reference]).set charge_back_dados.originalReference
        find(:xpath, elements_adyen_page[:orderId]).set charge_back_dados.orderId
        find(:xpath, elements_adyen_page[:eventDate]).set charge_back_dados.eventDate
        find(:xpath, elements_adyen_page[:amount]).set charge_back_dados.amount
        find(:xpath, elements_adyen_page[:statusSucess]).click
        find(:xpath, elements_adyen_page[:paymentMethod]).set charge_back_dados.paymentMethod
        find(:xpath, elements_adyen_page[:reason]).set charge_back_dados.reason
        find(:xpath, elements_adyen_page[:testConfig]).click
        sleep(5)
    end
end