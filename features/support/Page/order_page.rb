class Order
    include HTTParty  
        base_uri 'http://stage.appbk.burgerking.com.br/v4/order/api'

    def initialize(body, modulo, hmac)
        case modulo
        when "TransactionOrder"
            $auth = {'Content-Type': 'application/json', 'x-api-token': hmac}
        end

        @sendDocument = {
            :headers => $auth,
            :body => body}

    end

    def put_order_transaction()
        # método post do documento para receber a idkey
        self.class.put("/order", @sendDocument)
    end
end

class OrderCrm
    include HTTParty  
        base_uri 'https://app.pos.hmledp.com.br/v4/order/api'

    def initialize(body, modulo, hmac)
        case modulo
        when "TransactionOrder"
            $auth = {'Content-Type': 'application/json', 'x-api-token': hmac}
        end

        @sendDocument = {
            :headers => $auth,
            :body => body}

    end

    def put_order_transaction_crm()
        # método post do documento para receber a idkey
        self.class.put("/order", @sendDocument)
    end
end

def cria_transacao_order(encriptacao, code)

    body = {
        "cpfNote": "177.418.468-09",
        "customerEmail": "jedi@stage.adyen.com",
        "customerId": "33cc2eba-bac4-4284-b62e-acc7c63a0c1b",
        "customerIP": "172.200.20.8",
        "customerName": "Jedi Stage Adyen",
        "customerPhone": "(11) 98977-4828",
        "parts": [{
                "addedUnitPrice": 0.0,
                "defaultQuantity": 0,
                "parts": [],
                "title": "CASQUINHA MISTA",
                "observation": "",
                "partCode": "20002",
                "partType": "Product",
                "quantity": 1,
                "unitPrice": 1.5
            }
        ],
        "payment": {
            "brand": "Master",
            "code": "#{code}",
            "cpf": "177.418.468-09",
            "cvv": encriptacao[3],
            "expirationMonth": encriptacao[1],
            "expirationYear": encriptacao[2],
            "name": "teste",
            "number": encriptacao[0],
            "storePaymentMethod": false,
            "type": "credit_card"
        },
        "pickupType": "eat_in",
        "shipping": {
            "addressCity": "Barueri",
            "addressComplement": "10 Andar",
            "addressCountry": "BR",
            "addressLocation": "Al Rio Negro",
            "addressNeighborhood": "Alphaville",
            "addressNumber": "500",
            "addressState": "SP",
            "addressZipcode": "064150000",
            "method": "eat_in"
        },
        "storeCode": "99994"
    }

    @body = JSON.generate(body)
    modulo = 'TransactionOrder'
    @hmac = "5E05B564-3CF3-4ADB-9A19-8138419D6CB9"
    @putOrder = Order.new(@body, modulo, @hmac)
    @order = Cartao.new(@body, @hmac, modulo)
    puts @hmac
    return @body
end

def cria_transacao_order_crm(encriptacao, code, couponId, instanceId, reference)

    body = {
        "cpfNote": "177.418.468-09",
        "customerEmail": "helder.070516@gmail.com",
        "customerId": "38dc3c59-34b3-407c-a720-ba3e6203a05a",
        "customerIP": "172.200.20.8",
        "customerName": "Helder Fernandes",
        "customerPhone": "(11) 98977-4828",
        "parts": [{
                "addedUnitPrice": 0.0,
                "defaultQuantity": 0,
                "parts": [],
                "title": "CASQUINHA MISTA",
                "observation": "",
                "partCode": "20002",
                "partType": "Product",
                "quantity": 1,
                "unitPrice": 1.5,
                "couponId": "#{couponId}",
                "instanceId": "#{instanceId}",
                "reference": "#{reference}"
            }
        ],
        "payment": {
            "brand": "Master",
            "code": "#{code}",
            "cpf": "177.418.468-09",
            "cvv": encriptacao[3],
            "expirationMonth": encriptacao[1],
            "expirationYear": encriptacao[2],
            "name": "teste",
            "number": encriptacao[0],
            "storePaymentMethod": false,
            "type": "credit_card"
        },
        "pickupType": "eat_in",
        "shipping": {
            "addressCity": "Barueri",
            "addressComplement": "10 Andar",
            "addressCountry": "BR",
            "addressLocation": "Al Rio Negro",
            "addressNeighborhood": "Alphaville",
            "addressNumber": "500",
            "addressState": "SP",
            "addressZipcode": "064150000",
            "method": "eat_in"
        },
        "storeCode": "99994"
    }

    @body = JSON.generate(body)
    modulo = 'TransactionOrder'
    @hmac = "5E05B564-3CF3-4ADB-9A19-8138419D6CB9"
    @putOrderCrm = OrderCrm.new(@body, modulo, @hmac)
    @order = Cartao.new(@body, @hmac, modulo)
    return @body
end

def cria_transacao_com_fraude(encriptacao, code)

    body = {
        "cpfNote": "177.418.468-09",
        "customerEmail": "jedi@stage.adyen.com",
        "customerId": "33cc2eba-bac4-4284-b62e-acc7c63a0c1b",
        "customerIP": "172.200.20.8",
        "customerName": "Jedi Stage Adyen",
        "customerPhone": "(11) 98977-4828",
        "parts": [{
                "addedUnitPrice": 0.0,
                "defaultQuantity": 0,
                "parts": [],
                "title": "CASQUINHA MISTA",
                "observation": "",
                "partCode": "20002",
                "partType": "Product",
                "quantity": 1,
                "unitPrice": 1.5
            }
        ],
        "payment": {
            "brand": "Master",
            "code": "#{code}",
            "cpf": "177.418.468-09",
            "cvv": encriptacao[3],
            "expirationMonth": encriptacao[1],
            "expirationYear": encriptacao[2],
            "name": "TEST_EDP_ADYEN_RESPONSE:20",
            "number": encriptacao[0],
            "storePaymentMethod": false,
            "type": "credit_card"
        },
        "pickupType": "eat_in",
        "shipping": {
            "addressCity": "Barueri",
            "addressComplement": "10 Andar",
            "addressCountry": "BR",
            "addressLocation": "Al Rio Negro",
            "addressNeighborhood": "Alphaville",
            "addressNumber": "500",
            "addressState": "SP",
            "addressZipcode": "064150000",
            "method": "eat_in"
        },
        "storeCode": "99994"
    }

    @body = JSON.generate(body)
    modulo = 'TransactionOrder'
    @hmac = "5E05B564-3CF3-4ADB-9A19-8138419D6CB9"
    @putOrder = Order.new(@body, modulo, @hmac)
    @order = Cartao.new(@body, @hmac, modulo)
    return @body
end
