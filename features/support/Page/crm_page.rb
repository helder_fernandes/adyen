
require 'securerandom'
class DeletCupon
    include HTTParty  
        base_uri 'https://56c5dtxw99.execute-api.us-east-2.amazonaws.com/hml/'
    
    def initialize()
        
        $auth = {'Content-Type': 'application/json', 'x-api-token': '28ab0a9c-c6be-46d6-a6bb-c8fba4263474'}

        $auth1 = {'x-api-token': '28ab0a9c-c6be-46d6-a6bb-c8fba4263475'}

        $auth2 = {'x-api-token': 'c819af3d-0eb7-4954-8def-06b1d0a09f2e'}
        
        
        @getdata = {
            :headers => $auth,
            :body => {}}

        @getdata1 = {
            :headers => $auth1,
            :body => {}}
            

        @getdata2 = {
            :headers => $auth2,
            :body => {}}
    end

    def deleta_coupons(uuid)
        self.class.delete("/coupons/#{uuid}?", @getdata)
    end

    def deleta_coupons_without_token(uuid)
        self.class.delete("/coupons/#{uuid}?", @getdata1)
    end

    def deleta_instances(id_instance)
        self.class.delete("/instances/#{id_instance}/lock/", @getdata)
    end

    def deleta_instances_wrong_apikey(id_instance)
        self.class.delete("/instances/#{id_instance}/lock/", @getdata1)
    end

    def deleta_instances_disabled_apikey(id_instance)
        self.class.delete("/instances/#{id_instance}/lock/", @getdata2)
    end
end

class PostIntances
    include HTTParty  
        base_uri 'https://56c5dtxw99.execute-api.us-east-2.amazonaws.com/hml/'
    
    def initialize()
    
            $auth = {'x-api-token': '28ab0a9c-c6be-46d6-a6bb-c8fba4263474'}
    
            $auth1 = {}

            $auth2 = {'x-api-token': '28ab0a9c-c6be-46d6-a6bb-c8fba4263475'}

            $auth3 = {'x-api-token': 'c819af3d-0eb7-4954-8def-06b1d0a09f2e'}

            $auth4 = {'x-api-token': '28ab0a9c-c6be-46d6-a6bb-c8fba4263474'}

        @sendDocument = {
            :headers => $auth,
            :body => {}}

        @sendDocument1 = {
            :headers => $auth1,
            :body => {}}

        @sendDocument2 = {
            :headers => $auth2,
            :body => {}}

        @sendDocument3 = {
            :headers => $auth3,
            :body => {}} 
          
    end

    def post_instances_code_lock(code, posId)
        self.class.post("/instances/#{code}/lock/#{posId}", @sendDocument)
    end  

    def post_instances_code_lock_without_api_token(code, posId)
        self.class.post("/instances/#{code}/lock/#{posId}", @sendDocument1)
    end

    def post_instances_code_lock_wrong_api_token(code, posId)
        self.class.post("/instances/#{code}/lock/#{posId}", @sendDocument2)
    end

    def post_instances_code_lock_api_token_disabled(code, posId)
        self.class.post("/instances/#{code}/lock/#{posId}", @sendDocument3)
    end

end

class Crm
    include HTTParty  
        base_uri 'https://56c5dtxw99.execute-api.us-east-2.amazonaws.com/hml/'
    
    def initialize(hmacCrm, modulo, body)
        case modulo
        when "get"
            $auth = {'Content-Type': 'application/json', 'X-Hmac-Signature': hmacCrm, 'Date': $dateFull}
        when "put"
            $auth = {'Content-Type': 'application/json', 'x-api-token': hmacCrm}
        when "post"
            $auth = {'Content-Type': 'application/json', 'X-Hmac-Signature': hmacCrm, 'Date': $dateFull}
        when "post_missing_api_key"
            $auth1 = {'x-api-token': ''}
        when "del"
            $auth = {'Content-Type': 'application/json', 'X-Hmac-Signature': hmacCrm, 'Date': $data_execucao}
        when "post_sync"
            $auth4 = {'Content-Type': 'application/json', 'x-api-token': '28ab0a9c-c6be-46d6-a6bb-c8fba4263474'}
        when "post_instances"
            $auth2 = {'x-api-token': '28ab0a9c-c6be-46d6-a6bb-c8fba4263474'}
        end

        @getdata = {
            :headers => $auth,
            :body => {}}
            
        @sendDocument = {
            :headers => $auth,
            :body => body}

        @sendDocument1 = {
            :headers => $auth1,
            :body => {}}

        @sendDocument2 = {
            :headers => $auth2,
            :body => {}}

        @sendDocument3 = {
            :headers => $auth4,
            :body => {}}
    end

    def get_coupons(cuponID, customerIDCrm)
        # método post do documento para receber a idkey
        self.class.get("/coupons/#{cuponID}/available/#{customerIDCrm}", @getdata)
    end

    def get_instances(uuid)
        # método post do documento para receber a idkey
        self.class.get("/instances?customerId=#{uuid}&consumed=False&_limit=5&_offset=0", @getdata)
    end

    def get_instances_without_customerId()
        # método post do documento para receber a idkey
        self.class.get("/instances?consumed=False&_limit=5&_offset=0", @getdata)
    end

    def get_instances_without_consumed(uuid)
        # método post do documento para receber a idkey
        self.class.get("/instances?customerId=#{uuid}&consumed=&_limit=5&_offset=0", @getdata)
    end

    def get_instances_without_limit(uuid)
        # método post do documento para receber a idkey
        self.class.get("/instances?customerId=#{uuid}&consumed=false&_limit=&_offset=0", @getdata)
    end

    def get_instances_without_offset(uuid)
        # método post do documento para receber a idkey
        self.class.get("/instances?customerId=#{uuid}&consumed=false&_limit=5&=", @getdata)
    end

    def deleta_coupons(uuid)
        self.class.delete("/coupons/#{uuid}?", @getdata)
    end

    def put_coupons()
        self.class.put("/coupons/", @sendDocument)
    end

    def post_instances_cupon()
        self.class.post("/instances", @sendDocument)
    end

    def post_instances_sync(posId)
        self.class.post("/instances/sync/#{posId}", @sendDocument)
    end
    
end

class HmacOpensslCrm    
    def hmac_generate(dados)
        key = "c7XZGsWDqyBCuYcEyu0KV1hEiTooNWod"
        mac = OpenSSL::HMAC.hexdigest("SHA256", key, dados)
        return mac
    end  
end

def hmac_consulta_crm(query)
    body_consulta = "#{$dateFull}"+"::"+query+":"
    hmacCrm = HmacOpensslCrm.new.hmac_generate(body_consulta)
    modulo = "get"
    #inicializa um novo método:
    @crm = Crm.new(hmacCrm, modulo, body)
    
    return hmacCrm
end

def hmac_consulta_crm_instances(uuid)
    body_consulta = "#{$dateFull}"+":::_limit=5&_offset=0&consumed=False&customerId=#{uuid}"
    hmacCrm = HmacOpensslCrm.new.hmac_generate(body_consulta)
    modulo = "get"
    #inicializa um novo método:
    @crm = Crm.new(hmacCrm, modulo, body)
    puts body_consulta
    return hmacCrm
end

def hmac_consulta_crm_instances_without_customerId()
    body_consulta = "#{$dateFull}"+":::_limit=5&_offset=0&consumed=False"
    hmacCrm = HmacOpensslCrm.new.hmac_generate(body_consulta)
    modulo = "get"
    #inicializa um novo método:
    @crm = Crm.new(hmacCrm, modulo, body)
    puts body_consulta
    return hmacCrm
end

def hmac_consulta_crm_instances_without_consumed(uuid)
    body_consulta = "#{$dateFull}"+":::_limit=5&_offset=0&consumed=&customerId=#{uuid}"
    hmacCrm = HmacOpensslCrm.new.hmac_generate(body_consulta)
    modulo = "get"
    #inicializa um novo método:
    @crm = Crm.new(hmacCrm, modulo, body)
    puts body_consulta
    return hmacCrm
end

def hmac_consulta_crm_instances_without_limit(uuid)
    body_consulta = "#{$dateFull}"+":::_limit=&_offset=0&consumed=false&customerId=#{uuid}"
    hmacCrm = HmacOpensslCrm.new.hmac_generate(body_consulta)
    modulo = "get"
    #inicializa um novo método:
    @crm = Crm.new(hmacCrm, modulo, body)
    puts body_consulta
    return hmacCrm
end

def hmac_consulta_crm_instances_without_offset(uuid)
    body_consulta = "#{$dateFull}"+":::=&_limit=5&consumed=false&customerId=#{uuid}"
    hmacCrm = HmacOpensslCrm.new.hmac_generate(body_consulta)
    modulo = "get"
    #inicializa um novo método:
    @crm = Crm.new(hmacCrm, modulo, body)
    puts body_consulta
    return hmacCrm
end

def hmac_post_crm_instances(hmacRequest)
    body_consulta = "#{$dateFull}"+":#{hmacRequest}::"
    @hmacCrm = HmacOpensslCrm.new.hmac_generate(body_consulta)
    modulo = "get"
    #inicializa um novo método:
    @crm = Crm.new(@hmacCrm, modulo, body)
    puts body_consulta
    return @hmacCrm
end

def creat_coupon()

    body = {
        "externalId": "#{SecureRandom.uuid}",
        "rules": $RULES_OK,
        "couponInfo": $COUPON_INFO_OK,
        "partCode": "#{$ANY_PARTCODE}"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def creat_coupon_partcode_alfanumerico()

    body = {
        "externalId": "#{SecureRandom.uuid}",
        "rules": $RULES_OK,
        "couponInfo": $COUPON_INFO_OK,
        "partCode": "12346qwert"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def creat_coupon_expiration_data()

    body = {
        "externalId": "#{SecureRandom.uuid}",
        "rules": $RULES_NOK,
        "couponInfo": $COUPON_INFO_OK,
        "partCode": "#{$ANY_PARTCODE}"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def creat_coupon_wrong_api_key()

    body = {
        "externalId": "#{SecureRandom.uuid}",
        "rules": $RULES_OK,
        "couponInfo": $COUPON_INFO_OK,
        "partCode": "#{$ANY_PARTCODE}"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = $api_wrong_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def creat_coupon_disabled_api_key()

    body = {
        "externalId": "#{SecureRandom.uuid}",
        "rules": $RULES_OK,
        "couponInfo": $COUPON_INFO_OK,
        "partCode": "#{$ANY_PARTCODE}"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = "c819af3d-0eb7-4954-8def-06b1d0a09f2e"
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def creat_coupon_wrong_rules()

    body = {
        "externalId": "#{SecureRandom.uuid}",
        "rules": $RULES_NOK,
        "couponInfo": $COUPON_INFO_OK,
        "partCode": "#{$ANY_PARTCODE}"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def creat_coupon_logged_false()

    body = {
        "externalId": "#{SecureRandom.uuid}",
        "rules": $RULES_LOGGED_FALSE,
        "couponInfo": $COUPON_INFO_OK,
        "partCode": "#{$ANY_PARTCODE}"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def creat_coupon_consumption_limit()

    body = {
        "externalId": "#{SecureRandom.uuid}",
        "rules": $RULES_CONSUMPTION_LIMIT,
        "couponInfo": $COUPON_INFO_OK,
        "partCode": "#{$ANY_PARTCODE}"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def creat_coupon_without_rules()

    body = {
        "externalId": "#{SecureRandom.uuid}",
        "couponInfo": $COUPON_INFO_OK,
        "partCode": "#{$ANY_PARTCODE}"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def creat_coupon_without_couponInfo()

    body = {
        "externalId": "#{SecureRandom.uuid}",
        "rules": $RULES_OK,
        "partCode": "#{$ANY_PARTCODE}"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def creat_coupon_without_extertalId()

    body = {
        "rules": $RULES_OK,
        "couponInfo": $COUPON_INFO_OK,
        "partCode": "#{$ANY_PARTCODE}"
    }

    @body = JSON.generate(body)
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body

end

def creat_coupon_without_partCode()

    body = {
        "externalId": "#{SecureRandom.uuid}",
        "rules": $RULES_OK,
        "couponInfo": $COUPON_INFO_OK
    }

    @body = JSON.generate(body)
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body

end

def creat_coupon_bad_request()

    body = {
        "any": "any"
      }

    @body = JSON.generate(body)
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body

end

def creat_coupon_missing_api_key()

    body = {
        "externalId": "#{SecureRandom.uuid}",
        "rules": $RULES_OK,
        "couponInfo": $COUPON_INFO_OK,
        "partCode": "#{$ANY_PARTCODE}"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = ""
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def creat_coupon_internal_server_error()

    body = {
        "externalId": $UUID_INTERNAL_SERVER_ERROR,
        "rules": $RULES_OK,
        "couponInfo": $COUPON_INFO_OK,
        "partCode": "#{$ANY_PARTCODE}"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def creat_coupon_external_id_already()

    body = {
        "externalId": $ANY_UUID,
        "rules": $RULES_UPDATE,
        "couponInfo": $COUPON_UPDATE_OK,
        "partCode": "1051"
    }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "put"
    @hmac = $api_token_crm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body, @externalID

end

def post_instance(couponId)

    body = {
        "couponId": "#{couponId}",
        "customerId": $customerIdApp,
        "customerInfo": "{}"
      }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "post"
    @hmac = @hmacCrm
    @crm = Crm.new(@hmac, modulo, @body)

    return @body
    
end

def post_instance_forbidden()

    body = {
        "couponId": "9be92d78-e653-11e9-92ed-522233ba648c",
        "customerId": "38dc3c59-34b3-407c-a720-ba3e6203a05c",
        "customerInfo": "{}"
      }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "post"
    @hmac = @hmacCrm
    @crm = Crm.new(@hmac, modulo, @body)

    return @body
    
end

def post_instance_not_found()

    body = {
        "couponId": $UUID_NOT_FOUND,
        "customerId": $ANY_UUID,
        "customerInfo": "{}"
      }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "post"
    @hmac = @hmacCrm
    @crm = Crm.new(@hmac, modulo, @body)

    return @body
    
end

def post_instance_internal_server_error()

    body = {
        "couponId": $UUID_INTERNAL_SERVER_ERROR,
        "customerId": $ANY_UUID,
        "customerInfo": "{}"
      }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "post"
    @hmac = @hmacCrm
    @crm = Crm.new(@hmac, modulo, @body)

    return @body
    
end

def post_instance_bad_request()

    body = {
        "any": "any"
      }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "post"
    @hmac = @hmacCrm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body
    
end

def post_instance_without_couponId()

body = {
    "customerId": $customerIdApp,
    "customerInfo": "{}"
  }

    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "post"
    @hmac = @hmacCrm
    @crm = Crm.new(@hmac, modulo, @body)

    return @body
end

def post_instance_without_customerId()

    body = {
        "couponId": "9be92d78-e653-11e9-92ed-522233ba648c",
        "customerInfo": "{}"
      }
    
    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "post"
    @hmac = @hmacCrm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body
end

def post_instance_without_customerInfo()

    body = {
        "couponId": "9be92d78-e653-11e9-92ed-522233ba648c",
        "customerId": $customerIdApp
      }
    
    @body = JSON.generate(body)
    @externalID = JSON.parse(@body)['externalId']
    modulo = "post"
    @hmac = @hmacCrm
    @crm = Crm.new(@hmac, modulo, @body)
    
    return @body
end


def post_instance_sync_posId(code, paidDate)

    body = [
        {
          "code": "#{code}",
          "paidDate": "#{paidDate}"
        }
      ]

    @body = JSON.generate(body)
    modulo = "put"
    @hmac = $api_token_crm
    puts @hmac
    @crm = Crm.new(@hmac, modulo, @body)
    return @body  
end

def post_instance_sync_missing_key(code, paidDate)

    body = [
        {
          "code": "#{code}",
          "paidDate": "#{paidDate}"
        }
      ]

    @body = JSON.generate(body)
    modulo = "post"
    @hmac = ""
    @crm = Crm.new(@hmac, modulo, @body)

    return @body  
end

def post_instance_sync_wrong_key(code, paidDate)

    body = [
        {
          "code": "#{code}",
          "paidDate": "#{paidDate}"
        }
      ]

    @body = JSON.generate(body)
    modulo = "post"
    @hmac = "28ab0a9c-c6be-46d6-a6bb-c8fba4263475"
    @crm = Crm.new(@hmac, modulo, @body)

    return @body  
end