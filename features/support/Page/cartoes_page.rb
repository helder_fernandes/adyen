class Cartao
    include HTTParty  
        base_uri 'https://pf6hyb99x4.execute-api.us-east-1.amazonaws.com/stage'

    def initialize(body, hmac, modulo)
        case modulo
        when "card"
            $auth = {'Content-Type': 'application/json','X-Hmac-Signature': hmac}
        when "transaction"
            $auth = {'Content-Type': 'application/json', 'x-api-token': hmac}
        when "chargeback"
            $auth = {'Content-Type': 'application/json', 'Authorization': 'Basic YWR5ZW5fZWt3OVBJNXRmbDphZHllbl81VzhPbTVwcHZW'}
        when "confirmIvalid"
            $auth1 = {'Content-Type': 'application/json', 'x-api-token': '5E05B564-3CF3-4ADB-9A19-8138419D6CB3'}
        end
    
 # "opção" de envio para o método POST, body recebe o body montado
        @sendDocument = {
            :headers => $auth,
            :body => body}
        
 # "opção" de envio para o método GET, body segue vazio
        @getData = {
            :headers => $auth,
            :body => {}}    

        @getData1 = {
            :headers => $auth1,
            :body => {}}     
      
    end

    def putCard
        # método post do documento para receber a idkey
        self.class.put("/cards", @sendDocument)
    end

    def getCard(customerid,email)
        #método get que traz os cartões cadastrados na base.
        self.class.get("/cards?customerId=#{customerid}&email=#{email}", @getData)
    end

    def delete_Card(idCard)
        deletUrl = self.class.delete("/cards/#{$customerId}/#{idCard}", @getData)
    end

    def postTransaction
        self.class.post("/transactions", @sendDocument)
    end

    def confirma_Transacao(id)
        self.class.post("/transactions/#{id}/confirm", @getData)
    end

    def confirma_Transacao_invalid_token(id)
        self.class.post("/transactions/#{id}/confirm", @getData1)
    end
    
    def deleta_Transacao(id)
        self.class.delete("/transactions/#{id}", @getData)
    end

    def deleta_Transacao_invalid_token(id)
        self.class.delete("/transactions/#{id}", @getData1)
    end
    
    def chargeBack_Transacao()
        self.class.post("/notification", @sendDocument)
    end

end

class HmacOpenssl    
    def hmac_generate(dados)
        key = "Ma66bwcKhhLBQ7zE48zrWUHqstC372pJ"
        mac = OpenSSL::HMAC.hexdigest("SHA256", key, dados)
        return mac
    end  
end

def hmac_delete(path)
    @body2 = ":"+path+":"
    @hmac = HmacOpenssl.new.hmac_generate(@body2)

    modulo = 'card'
    
    @deleta_cartao = Cartao.new(@body, @hmac, modulo)
    return @hmac
end

def hmac_consulta(query)
    body_consulta = "::"+query
    hmac = HmacOpenssl.new.hmac_generate(body_consulta)
    modulo = 'card'
    
    #inicializa um novo método:
    @cartao = Cartao.new(body, hmac, modulo)
    
    return hmac
end

def deleta_cartao_com_id(idcard)
    dados = "#{$customerId}&#{idcard}"
    hmac = hmac_delete(dados)
    puts "Dados: #{dados}"
    puts "HMAC para deleção: #{hmac}"
    response = @deleta_cartao.delete_Card(idcard)
    
    return response
end


def deleta_cards()
    # Delete para limpar os dados:
    hmac = hmac_consulta("customerId=#{$customerId}&email=#{$email}")
    # puts hmac
    response = @cartao.getCard($customerId,$email)
    # puts response
    # puts response.class
    lista_cartoes = JSON.parse(response.body)
    lista_cartoes.each do |x| 
        saveID = x["savedId"]   
        # puts saveID
        hmac_delete("#{$customerId}&#{saveID}")
        @deleta_cartao.delete_Card(saveID)
        # puts @url
    end         
    response = @cartao.getCard($customerId,$email)
    # return @response
    # puts response
    # puts response.class
 
end
  
def cria_cartao(encriptacao,code)
    
    body = {"card":{
        "code": "#{code}",
        "cardHolderName":"Julio Almeida",
        "expirationMonth": encriptacao[1],
        "expirationYear": encriptacao[2],
        "number": encriptacao[0],
        "securityCode": encriptacao[3],
        "type":"credit_card"},
        "customerId":"#{$customerId}",
        "email":"#{$email}"
    }

    @body = JSON.generate(body)
    
    @body2 = @body+"::"
    @hmac = HmacOpenssl.new.hmac_generate(@body2)

    modulo = 'card'

    @cartao = Cartao.new(@body, @hmac, modulo)

    return @body

end

def cria_cartao_sem_ano(encriptacao)
    
    body = {"card":{
        "code":"mc",
        "cardHolderName":"Julio Almeida",
        "expirationMonth": encriptacao[1],
        "expirationYear": "",
        "number": encriptacao[0],
        "securityCode": encriptacao[3],
        "type":"credit_card"},
        "customerId":"#{$customerId}",
        "email":"#{$email}"
    }

    @body = JSON.generate(body)
    
    @body2 = @body+"::"
    @hmac = HmacOpenssl.new.hmac_generate(@body2)

    modulo = 'card'

    @cartao = Cartao.new(@body, @hmac, modulo)

    return @body

end

def cria_cartao_sem_numero(encriptacao)
    
    body = {"card":{
        "code":"mc",
        "cardHolderName":"Julio Almeida",
        "expirationMonth": encriptacao[1],
        "expirationYear": encriptacao[2],
        "number": "",
        "securityCode": encriptacao[3],
        "type":"credit_card"},
        "customerId":"#{$customerId}",
        "email":"#{$email}"
    }

    @body = JSON.generate(body)
    
    @body2 = @body+"::"
    @hmac = HmacOpenssl.new.hmac_generate(@body2)

    modulo = 'card'

    @cartao = Cartao.new(@body, @hmac, modulo)

    return @body

end

def cria_cartao_sem_cvv(encriptacao)
    
    body = {"card":{
        "code":"mc",
        "cardHolderName":"Julio Almeida",
        "expirationMonth": encriptacao[1],
        "expirationYear": encriptacao[2],
        "number": encriptacao[0],
        "securityCode": "",
        "type":"credit_card"},
        "customerId":"#{$customerId}",
        "email":"#{$email}"
    }

    @body = JSON.generate(body)
    
    @body2 = @body+"::"
    @hmac = HmacOpenssl.new.hmac_generate(@body2)

    modulo = 'card'

    @cartao = Cartao.new(@body, @hmac, modulo)

    return @body

end

def cria_cartao_sem_mes(encriptacao)
    
    body = {"card":{
        "code":"mc",
        "cardHolderName":"Julio Almeida",
        "expirationMonth": "",
        "expirationYear": encriptacao[2],
        "number": encriptacao[0],
        "securityCode": encriptacao[3],
        "type":"credit_card"},
        "customerId":"#{$customerId}",
        "email":"#{$email}"
    }

    @body = JSON.generate(body)
    
    @body2 = @body+"::"
    @hmac = HmacOpenssl.new.hmac_generate(@body2)

    modulo = 'card'

    @cartao = Cartao.new(@body, @hmac, modulo)

    return @body

end

def cria_cartao_hmac_incorreto(encriptacao)
    
    body = {"card":{
        "code":"mc",
        "cardHolderName":"Julio Almeida",
        "expirationMonth": encriptacao[1],
        "expirationYear": encriptacao[2],
        "number": encriptacao[0],
        "securityCode": encriptacao[3],
        "type":"credit_card"},
        "customerId":"#{$customerId}",
        "email":"#{$email}"
    }

    @body = JSON.generate(body)
    
    @body2 = @body+"::"
    @hmac = HmacOpenssl.new.hmac_generate(@body2)
    @hmac = "#{@hmac}0"
    modulo = 'card'

    @cartao = Cartao.new(@body, @hmac, modulo)

    return @body

end

def busca_cartao_api()
    hmac = hmac_consulta("customerId=#{$customerId}&email=#{$email}")
    puts "HMAC de consulta #{hmac}"
    response = @cartao.getCard($customerId,$email)

    return hmac, response
end

def cria_cartao_completo()
 # método para criação de um cartão mastercard apenas para validar a consulta

    @dados = {"cardNumber"=>"2222400010000008", "month"=>"10", "year"=>"20", "CVV"=>"737"}
    
    visit $pagina_encriptacao

    @encriptacao = @encrypter.preencher_cartao(@dados)


    body = {"card":{
        "code": "mc",
        "cardHolderName":"Julio Almeida",
        "expirationMonth": @encriptacao[1],
        "expirationYear": @encriptacao[2],
        "number": @encriptacao[0],
        "securityCode": @encriptacao[3],
        "type":"credit_card"},
        "customerId":"#{$customerId}",
        "email":"#{$email}"
    }


    @body = JSON.generate(body)
    
    # puts @body
    @body2 = @body+"::"
    @hmac = HmacOpenssl.new.hmac_generate(@body2)
    modulo = 'card'
    @cartao = Cartao.new(@body, @hmac, modulo)

    response = @cartao.putCard()
    return response
end

def cria_cartao_numero_errado(encriptacao,code)
    
    body = {"card":{
        "code": "#{code}",
        "cardHolderName":"Julio Almeida",
        "expirationMonth": encriptacao[1],
        "expirationYear": encriptacao[2],
        "number": "adyenjs_0_1_18$gwADlakXCv5V0SmUo86x5mgKbQy/YMNBGaAfa/uJZqu5i5X1lJoB5KGGaYGEKr6TQJodKunyG9hXFKwTk/6DgZ7KUqMnf4KWEVPTS91e0XH6K3Y/LLWx9OjEiLn559RPGQPfWGDaZW/+3FpBS8JUpcH1ctY/zvtZWpP08NkBxCrb21wWg01UrhqcpexExOPHScBZpqClhU3NlAu+6fHinuRWvKX8+IedL0mQoXdEbOsEI/RHGUb6OT5bjxqTbmL4y6JhbkibGmRFsSe2Wlc0arQddtDI2yCKd36HbsDs0n7DO+S8SePbDtMw1+dObXnnxGExffuwG8E1NQKsooD5hQ==$QQ17MPjpnZmYSVO2VWbEWrhdeZCtO+OmoI87Vzf5I39OFitTgXCxQoWyuGGakvMjDcbUHX/i4gRgKkxD+MJCK0WFv16mNyzXD1x6t/kW36pOhC6WW8Cv1pSyaXiQRDHbi9LATGZQ1kBdF8Yd0TXnQVJI9EYYeQh4Cu5eWAy8CkIQwrW/mpq2x26MBEkY9O9wLFBVcGj/3uCe80p7MdpdqnGiB5tzYO1yQx0E7frCOTo/kVguvIx+wAK34YLB1YAtdY1FtZoPLODBpBCbIDXGbgR3ek2QeqCa0TfZ3p6T8uhzVWH3Kqyvt8pRrGlwlBHx8DRv4Mv1TylF8vov70cf4vDCFzIakwCIzWc6cqa0kkRHA8kKnsTnh5ldBYeXLD5BqZe6rumTQ1nfiv2TolKvROhjR7JjrBrFYW5Y",
        "securityCode": encriptacao[3],
        "type":"credit_card"},
        "customerId":"#{$customerId}",
        "email":"#{$email}"
    }

    @body = JSON.generate(body)
    
    @body2 = @body+"::"
    @hmac = HmacOpenssl.new.hmac_generate(@body2)

    modulo = 'card'

    @cartao = Cartao.new(@body, @hmac, modulo)

    return @body

end

def cria_cartao_sem_customerId(encriptacao,code)
    
    body = {"card":{
        "code": "#{code}",
        "cardHolderName":"Julio Almeida",
        "expirationMonth": encriptacao[1],
        "expirationYear": encriptacao[2],
        "number": encriptacao[0],
        "securityCode": encriptacao[3],
        "type":"credit_card"},
        "email":"#{$email}"
    }

    @body = JSON.generate(body)
    
    @body2 = @body+"::"
    @hmac = HmacOpenssl.new.hmac_generate(@body2)

    modulo = 'card'

    @cartao = Cartao.new(@body, @hmac, modulo)

    return @body

end