class Encrypter
    include Capybara::DSL

    def elements_encript_page
        {

     cardnumber: '#encryptedCardNumber',
     expiryMonth: '#encryptedExpiryMonth',
     expiryYear: 'encryptedExpiryYear',
     securityCode: 'encryptedSecurityCode',
     iframecardNumber: '/html/body/div/div/label[1]/span/iframe',
     iframexpiryMonth: '/html/body/div/div/label[2]/span/iframe',
     iframexpiryYear: '/html/body/div/div/label[3]/span/iframe',
     iframesecurityCode: '/html/body/div/div/label[4]/span/iframe'

        }
    end

    def preencher_cartao(number)
        elements_encript_page()
        encriptacao = Array.new
        within_frame(:xpath, '/html/body/div/div/label[1]/span/iframe')do
            fill_in 'encryptedCardNumber', :with => number['cardNumber']
        end
        encriptacao.push(find("#card-encrypted-encryptedCardNumber", :visible => false).value)
        
        within_frame(:xpath, '/html/body/div/div/label[2]/span/iframe')do
            fill_in(id: 'encryptedExpiryMonth', with: number['month'])
        end
        
        encriptacao.push(find("#card-encrypted-encryptedExpiryMonth", :visible => false).value)

        within_frame(:xpath, '/html/body/div/div/label[3]/span/iframe')do
            fill_in(id: 'encryptedExpiryYear', with: number['year'])
        end
        
        encriptacao.push(find("#card-encrypted-encryptedExpiryYear", :visible => false).value)
        
        within_frame(:xpath, '/html/body/div/div/label[4]/span/iframe')do
            fill_in(id: 'encryptedSecurityCode', with: number['CVV'])
        end
        encriptacao.push(find("#card-encrypted-encryptedSecurityCode", :visible => false).value)

        return encriptacao
    end
end


def porra2()
    STDOUT.puts "PORRA22!!!!!"
end


# class Encrypter < SitePrism::Page

#     element :cardnumber, '#encryptedCardNumber'
#     element :expiryMonth, '#encryptedExpiryMonth'
#     element :expiryYear, 'encryptedExpiryYear'
#     element :securityCode, 'encryptedSecurityCode'
#     element :iframecardNumber, :xpath, '/html/body/div/div/label[1]/span/iframe'
#     element :iframexpiryMonth, :xpath, '/html/body/div/div/label[2]/span/iframe'
#     element :iframexpiryYear, :xpath, '/html/body/div/div/label[3]/span/iframe'
#     element :iframesecurityCode, :xpath, '/html/body/div/div/label[4]/span/iframe'

#     def inputCardInformations(number)
#         within_frame(:xpath, '/html/body/div/div/label[1]/span/iframe')do
#         fill_in(id: 'encryptedCardNumber', with: number['cardNumber'])
#         end
#         within_frame(:xpath, '/html/body/div/div/label[2]/span/iframe')do
#             fill_in(id: 'encryptedExpiryMonth', with: number['month'])
#         end
#         within_frame(:xpath, '/html/body/div/div/label[3]/span/iframe')do
#             fill_in(id: 'encryptedExpiryYear', with: number['year'])
#         end
#         within_frame(:xpath, '/html/body/div/div/label[4]/span/iframe')do
#             fill_in(id: 'encryptedSecurityCode', with: number['CVV'])
#         end
#     end
# end