Dir[File.join(File.dirname(__FILE__), './features/support/Page/*_page.rb')].each { |file| require file} 
# require '..features/support/Page/encriptador_page.rb'

Before do

# configurações de url:
    $pagina_encriptacao = 'http://adyen-card-encryption.s3-website.us-east-2.amazonaws.com/'
    $pagina_adyen_login = 'https://ca-test.adyen.com/ca/ca/login.shtml'
    $pagina_adyen_payments = 'https://ca-test.adyen.com/ca/ca/payments/showList.shtml'
    $api_consulta_cards = 'https://pf6hyb99x4.execute-api.us-east-1.amazonaws.com/stage'
    $api_exclusao_cards = 'https://pf6hyb99x4.execute-api.us-east-1.amazonaws.com/stage/cards/'

# inicialização dos métodos:
    @encrypter = Encrypter.new
    @adyen = AdyenPage.new
    @metodo_pagamento = Metodo_Pagamento.new
    @crm_delete = DeletCupon.new
    @post_instances = PostIntances.new
    

# cofigurações do Banco:
    $host_bd = 'stagecatalogapidb.infra.appbk.burgerking.com.br.'
    $username = 'postgres'
    $password = 's7U_UjU<'
    $database = 'postgres'
    $port = '5432'

# configurações banco CRM:
    $host_bd_crm = 'catalogdb.cpxecbpvmajx.us-east-2.rds.amazonaws.com'
    $username_crm = 'app_pos_coupon_service_app_login'
    $password_crm = 'xSxqnCyugJU0ipP7'
    $database_crm = 'appposcouponservice' 

# configurações de Email:
    $customerId = 'teste_adyen_aldemir2019'
    $email = 'test-adyen-aldemir@adyen.com'

# configurações CRM - Variáveis    
    $customerIdCrm = '77105a5a-17ef-4194-abae-bbaa9685b0cc'
    $customerIdCrmNotFound = '77105a5a-17ef-4194-abae-bbaa9685b0c9'
    $customerIdApp = '38dc3c59-34b3-407c-a720-ba3e6203a05a'
    $external_id_crm = '77105a5a-17ef-4194-abae-bbaa9685b0cc'
    $couponIdCrm = 'cdd4ddb6-e7aa-11e9-86ad-7662b3a61d86'
    $couponId = '11111111-1111-1111-1111-111111111111'
    $couponIdForbidden = '00000000-0000-0000-0000-000000000403'
    $couponIdNotFound = '00000000-0000-0000-0000-000000000404'
    $couponInternalServerError = '00000000-0000-0000-0000-000000000500'
    $UUID_INTERNAL_SERVER_ERROR = '00000000-0000-0000-0000-000000000500'
    $ANY_UUID = '77105a5a-17ef-4194-abae-bbaa9685b0cc'
    $UUID_OK = '11111111-1111-1111-1111-111111111111'
    $ANY_PARTCODE = '1050'
    $ANY_PARTCODE_UPDATE = '1051'
    $UUID_FORBIDDEN = '00000000-0000-0000-0000-000000000403'
    $UUID_NOT_FOUND = '00000000-0000-0000-0000-000000000404'
    $COUPON_INFO_OK = {"price":8.5,"couponCode":"APP41","image":"https://media.appbk.burgerking.com.br/v3/a7ee2b032620ff9d18506e547a9bdfd6b262068f743bdbec3eed74eb54e5cd50.png","description":"<p>Apresente esse cupom na compra de 1 Pipochicken com 10 unidades e pague R$7,50</p><p>Promoção válida por tempo indeterminado ou enquanto durarem os estoques. Promoção não cumulativa. Imagens meramente ilustrativas. Os preços dos produtos podem variar de acordo com região e/ou restaurantes. Promoção válida somente nos restaurantes Burger King® participantes. Consulte os restaurantes participantes no site: cupons.burgerking.com.br. Em caso de item adicional ou troca, o item será cobrado separadamente. TM &amp; © 2019 Burger King Corporation. Uso sob licença. Todos os direitos reservados.</p>","name":"PIPOCHICKEN 10 UNIDADES"}
    $COUPON_UPDATE_OK = {"price":9.5,"couponCode":"APP41","image":"https://media.appbk.burgerking.com.br/v3/a7ee2b032620ff9d18506e547a9bdfd6b262068f743bdbec3eed74eb54e5cd50.png","description":"<p>Apresente esse cupom na compra de 1 Pipochicken com 10 unidades e pague R$7,50</p><p>Promoção válida por tempo indeterminado ou enquanto durarem os estoques. Promoção não cumulativa. Imagens meramente ilustrativas. Os preços dos produtos podem variar de acordo com região e/ou restaurantes. Promoção válida somente nos restaurantes Burger King® participantes. Consulte os restaurantes participantes no site: cupons.burgerking.com.br. Em caso de item adicional ou troca, o item será cobrado separadamente. TM &amp; © 2019 Burger King Corporation. Uso sob licença. Todos os direitos reservados.</p>","name":"PIPOCHICKEN 11 UNIDADES"}
    $RULES_OK = {"usageLimit":5,"expirationDate":"2019-10-20 16:39:44.716205","loggedCoupon":true}
    $RULES_NOK = {"usageLimit":5,"expirationDate":"2019-10-02 16:39:44.716205","loggedCoupon":true}
    $RULES_LOGGED_FALSE = {"usageLimit":5,"expirationDate":"2019-10-20 16:39:44.716205","loggedCoupon":false}
    $RULES_UPDATE = {"usageLimit":3,"expirationDate":"2019-10-20 16:39:44.716205","loggedCoupon":true}
    $RULES_CONSUMPTION_LIMIT = {"usageLimit":5,"expirationDate":"2019-10-20 16:39:44.716205","loggedCoupon":"true","consumptionLimit":"1"}
    $UUID_INT = "123456789"
    $dateFull = Time.now.httpdate
    $time_execucao = Time.new.strftime("%Y-%m-%d %H:%M:%S")
    #{$customerId}
    #{$email}
    $cuponId = "couponId:"
    $customerId = "customerId:"
    $customerInfo = "customerInfo"
    
    


# configurações Adyen:
    $adyen_login = Array.new
    dados = {
        :account => "BKBrasil",
        :login => "admin",
        :password => "app@2019"
    }

    $adyen_login.push(dados)

# Config Token:
    $api_token = '5E05B564-3CF3-4ADB-9A19-8138419D6CB9'
    $api_token_crm = '28ab0a9c-c6be-46d6-a6bb-c8fba4263474'
    $api_wrong_token_crm = '28ab0a9c-c6be-46d6-a6bb-c8fba4263475'
    $authorization = 'Basic YWR5ZW5fZWt3OVBJNXRmbDphZHllbl81VzhPbTVwcHZW'
    $hmacSignature = 'XhXfBkxsnvlmbWXjWeRq4m5LnftNTVsf/74XV6TDzdc='
    $hmacHash = '7916045164fd13224f08f6e44b76d1a136404ed545bdf7470a79f9993b6d9053'

# Config Campos do Banco:
    $ext_order_id = "EXTERNAL_ORDER_ID"
    $status = "STATUS" 
    $payment_method = "PAYMENT_METHOD"
    $created_date = "CREATED_DATE"         
    $customer_id = "CUSTOMER_ID"    
    $customer_email = "CUSTOMER_EMAIL"        
    $amount = "AMOUNT"
    $process_type = "PROCESSING_TYPE"
    $card_save_id = "CARD_SAVED_ID"
    $broker_id = "BROKER_ID"
    $payment_method_id = "PAYMENT_METHOD_ID"
    $card_bin = "CARD_BIN"

# Config Campos do Banco CRM:
    $id = "ID*"
    $rules = "RULES"
    $external_id = "EXTERNAL_ID*"
    $coupon_info = "COUPON_INFO"
    $part_code = "PART_CODE*"
    $created_at = "CREATED_AT*"
    $disabled_at = "DISABLED_AT"
    $app_id = "APP_ID*"
    $coupon_id = "COUPON_ID"
    $reference = "REFERENCE"
    $offline_data = "OFFLINE_DATA"
    $purged = "PURGED"
    $customer_info = "CUSTOMER_INFO"
    $locked_for = "LOCKED_FOR"
    $locked_since = "LOCKED_SINCE"
    $expires_at = "EXPIRES_AT"
    $instance_id = "INSTANCE_ID"
    $consumed_at = "CONSUMED_AT"
    $sync_id = "SYNC_ID"
    $pos_id = "POS_ID"
    $locked_until = "LOCKED_UNTIL"
    $api_key = "API_KEY"
    


end



Before do |scenario, step|
# Feature name
    @feature_name = scenario.feature.name
    
# Scenario name
    @scenario_name = scenario.name.gsub(/\s+/, '_').tr('/','_')
    
# Tags (as an array)
    @scenario_tags = scenario.source_tag_names

    # @step_name = step.name
end

# After do
#     copia_arquivos($data_execucao)

# end

