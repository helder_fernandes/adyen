#language: pt
@consultar_metodos_pagamento
Funcionalidade: Consultar todos os métodos de pagamentos disponíveis pela Adyen.

@consultar_metodos
Cenário: Consultar todos os métodos de pagamentos - Status Code: 200
Dado que faço a consulta na API
Então Recebo o retorno com status code "200" e todos os métodos de pagamento disponíveis
