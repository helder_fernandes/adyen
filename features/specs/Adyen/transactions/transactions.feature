#language: pt
@transacao
Funcionalidade: Fluxo de Transações
    Validar a Criação e demais operações do fluxo de transações

@cria_transacao_mastercard @cria_transacao_cartoes @BKD-311 @poc_bk
Cenário: Criar transação com cartão Mastercard - Status Code 200
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com Mastercard
    Entao Recebo o código de sucesso "200" e o código da transação
    E verifico no banco os dados dessa transação
    E acesso a plataforma da Adyen e verifico o número do pedido criado.

@cria_transacao_visa @cria_transacao_cartoes @poc_bk
Cenário: Criar transação com cartão Visa - Status Code 200
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |4000020000000000| 10  | 20 | 737 |
    Quando faço um envio de uma transação com Visa
    Entao Recebo o código de sucesso "200" e o código da transação
    E verifico no banco os dados dessa transação
    E acesso a plataforma da Adyen e verifico o número do pedido criado.

@cria_transacao_amex @cria_transacao_cartoes
Cenário: Criar transação com cartão AMEX - Status Code: 200
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |370000000000002 | 10  | 20 | 7373|
    Quando faço um envio de uma transação com Amex
    Entao Recebo o código de sucesso "200" e o código da transação
    E verifico no banco os dados dessa transação
    E acesso a plataforma da Adyen e verifico o número do pedido criado.

@cria_transacao_hipercard @cria_transacao_cartoes
Cenário: Criar transação com cartão Hipercard - Status Code: 200
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |6062828888666688| 10  | 20 | 737 |
    Quando faço um envio de uma transação com Hipercard
    Entao Recebo o código de sucesso "200" e o código da transação
    E verifico no banco os dados dessa transação
    E acesso a plataforma da Adyen e verifico o número do pedido criado.

@cria_transacao_elo @cria_transacao_cartoes
Cenário: Criar transação com cartão Elo - Status Code: 200
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |5066991111111118| 10  | 20 | 737 |
    Quando faço um envio de uma transação com Elo
    Entao Recebo o código de sucesso "200" e o código da transação
    E verifico no banco os dados dessa transação
    E acesso a plataforma da Adyen e verifico o número do pedido criado.

@cria_transacao_faltando_parametro @cria_transacao_fluxos_alternativos
Cenário: Criar transação com parametro faltando - Status Code: 400
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação sem o parametro storeCard
    Entao Recebo o código "400" e a mensagem "Could not parse storeCard"

@Cria_transacao_com_cvv_incorreto @cria_transacao_fluxos_alternativos @BKD-304
Cenário: Criar transação com CVV incorreto - Status Code: 406
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 730 |
    Quando faço um envio de uma transação com um CVC incorreto.
    Entao Recebo o status code "406" e a mensagem "DECLINED CVC Incorrect"
    Então consulto na base de dados se a Adyen nos retornou o status "11" 2 vezes

@Cria_com_encriptacao_expirada @cria_transacao_fluxos_alternativos
Cenário: Criar transação com Encryptação Expirada  - Status Code: 400
    Quando faço um envio de uma transação com dados expirados.
    Entao Recebo o status code "400" e a mensagem informando que o número esta incorreto

@Cria_vencido @cria_transacao_fluxos_alternativos
Cenário: Criar transação com Data Validade Expirada  - Status Code: 406
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 18 | 737 |
    Quando faço um envio de uma transação com data de validade expirada.
    Entao Recebo o status code "406" e a mensagem "DECLINED Expiry Incorrect"

@criar_mesmo_orderid @cria_transacao_fluxos_alternativos
Cenario: Criar transações com o mesmo OrderID
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação nova
    E crie uma nova transação com mesmo orderID
    E eu vou na adyen e confirmo que ambas as transações serão apresentadas.
    E confirmo uma das transações e recebo o status code "201"
    E consulto no banco ambas todas as transações com o OrderID criado anteriormente.
    Então consulta na Adyen novamente e uma das transação apresentará o status SettledInInstallments.

@cria_transacao_charge_back
Cenário: Fazer o teste de charge Back transaction
Dado que tenho o cartão encriptado:
    |cardNumber      |month|year| CVV |
    |2222400010000008| 10  | 20 | 737 |
Quando faço um envio de uma transação
Entao Recebo o código de sucesso "200" e o código da transação
E verifico no banco os dados dessa transação
E acesso a plataforma da Adyen e realizo o chargeback.
Então retorno ao banco onde deverá ser apresentado o status "CHARGEBACK"

@cria_transacao_zipcode_sem_parametro @zipcodes
Cenário: Criar transação - validações do Zipcode - Sem Parâmetro
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação sem o parametro zipcode
    Entao Recebo o código de sucesso "200" e o código da transação
    E verifico no banco os dados dessa transação card_saved_id está com o saveId do cartão


@cria_transacao_zipcode_nulo @zipcodes
Cenário: Criar transação - validações do Zipcode - Nulo
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com zipcode nulo
    Entao Recebo o código de sucesso "200" e o código da transação
    E verifico no banco os dados dessa transação card_saved_id está com o saveId do cartão

@cria_transacao_zipcode_vazio @zipcodes
Cenário: Criar transação - validações do Zipcode - Vazio
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com zipcode vazio
    Entao Recebo o código de sucesso "200" e o código da transação
    E verifico no banco os dados dessa transação card_saved_id está com o saveId do cartão

@cria_transacao_zipcode_invalido @zipcodes
Cenário: Criar transação - validações do Zipcode - Inválido
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com zipcode inválido
    Entao Recebo o código de sucesso "200" e o código da transação
    E verifico no banco os dados dessa transação card_saved_id está com o saveId do cartão

@cria_transacao_storeCard_true @transacao_diversos
Cenário: Criar transação com o parametro StoreCard = true
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com storeCard true
    Entao Recebo o código de sucesso "200" e o código da transação
    E verifico no banco os dados dessa transação card_saved_id está com o saveId do cartão

@cria_transacao_storeCard_false @transacao_diversos
Cenário: Criar transação com o parametro StoreCard = false
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com storeCard false
    Entao Recebo o código de sucesso "200" e o código da transação
    E verifico no banco que a coluna card_saved_id está vazia

@cria_transacao_processingType_reserved @processingTypes @transacao_diversos
Cenário: Cria transação com parametro processingType = reserved
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com processingType "reserved"
    Entao Recebo o código de sucesso "200" e o código da transação
    E verifico no banco os dados dessa transação
    E acesso a plataforma da Adyen e verifico o número do pedido criado.

@cria_transacao_processingType_captured @processingTypes @transacao_diversos
Cenário: Cria transação com parametro processingType = captured
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com processingType "captured"
    Entao Recebo o código de sucesso "200" os dados da transação e a mensagem de captura
    E verifico no banco os dados dessa transação com status CAPTURING E CAPTURED
    E acesso a plataforma da Adyen e verifico o número do pedido criado.

@Cria_com_Cvv_incorreto_denied
Cenário: Criar transação com fluxo Denied - Status Code: 406
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 730 |
    Quando faço um envio de uma transação com um CVC incorreto.
    Entao Recebo o status code "406" e a mensagem "DECLINED CVC Incorrect"
    E verifico no banco os dados dessa transação com status DENIED
    E acesso a plataforma da Adyen e verifico o número do pedido criado.

# @cria_transacao_refunded
# Cenário: Cria transação com fluxo Refunded
#     Dado que tenho uma transação criada com o cartão:
#         |cardNumber      |month|year| CVV |
#         |2222400010000008| 10  | 20 | 737 |
#     E uma transação confirmada
#     Entao Recebo o código de sucesso "201" informando que a transação foi confirmada.
#     E deleto a transação
#     E verifico no banco os dados dessa transação com status REFUNDING e "REFUNDED"
#     E acesso a plataforma da Adyen e verifico o número do pedido criado.

@confirma_transacao @confirmacao_transacoes
Cenário: Confirmar transação - Status Code: 201
    Dado que tenho uma transação criada com o cartão:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando confirmo a transação
    Entao recebo o código de sucesso "201" informando que a transação foi criada.
    E verifico no banco os dados dessa transação ela com status CAPTURED
    E acesso a plataforma da Adyen e verifico o número do pedido criado.

@confirma_transacao_token_invalido
Cenário: Confirmar transação com token invalido - Status Code: 401
    Dado que tenho uma transação criada com o cartão:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando confirmo a transação com token invalido  
    Entao Recebo o status code "401" e a mensagem "Access is denied due to invalid token" como retorno.

@confirmar_transacao_inexistente @confirmacao_transacoes
Cenário: Confirmar transacao inexistente - Status Code: 404
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação inexistente
    Entao Recebo o código "404" e o retorno de que a transação não foi encontrada

@confirmar_transacao_confirmada_cancelled @BKD-300 @BKD-324
Cenário: Confirmar Transação que já foi deletada depois de confirmada - Status Code: 404
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com Mastercard
    E confirmo a transação
    Quando deleto a transação
    E tento realizar a confirmação da transação deletada e retorna o status code "404" informando que a transação não existe.

# @confirmar_transacao_cancelled @BKD-300 @BKD-324
# Cenário: Confirmar Transação que já foi deletada sem ter sido confirmada - Status Code: 404
#     Dado que tenho o cartão encriptado:
#         |cardNumber      |month|year| CVV |
#         |2222400010000008| 10  | 20 | 737 |
#     Quando faço um envio de uma transação com Mastercard
#     Quando deleto a transação
#     E tento realizar a confirmação da transação deletada e retorna o status code "404" informando que a transação não existe.

# @confirmar_transacao_refunded @ultimos_cenarios @BDK-328
# Cenário: Capturar uma transação - Status Refunded
#     Dado que tenho uma transação criada com o cartão:
#         |cardNumber      |month|year| CVV |
#         |2222400010000008| 10  | 20 | 737 |
#     E uma transação confirmada
#     Entao Recebo o código de sucesso "201" informando que a transação foi confirmada.
#     E deleto a transação
#     E verifico no banco os dados dessa transação com status REFUNDING e "REFUNDED"
#     Então confirmo a transação que já foi deletada.
#   # Então Acesso o banco e vejo que o status "DENIED" será apresentado.

@confirmar_transacao_card_excluded @ultimos_cenarios
Cenário: Confirmar transação após excluir cartão
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    E faço o envio de uma nova transação
    E deleto cartão que foi utilizado na transação.
    E confirmo a transação criada
    Entao recebo o código status code "200" informando que a transação foi criada.
    E verifico no banco os dados dessa transação esta com status CAPTURED
    E acesso a plataforma da Adyen e verifico o número do pedido criado. 

@confimar_transacao_valor_maior
Cenário: Confirmar Transação com valor maior que o reservado.
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com um valor definido
    Então altero o valor no banco para um valor diferente
    E confirmo a transação com o valor inicial

@deleta_transacao @deleta_transacoes
Cenário: deletar transação - Status Code: 204
    Dado que tenho uma transação criada com o cartão:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando deleto a transação
    Entao Recebo o código de sucesso "204"

@deleta_transacao_token_invalido @deleta_transacoes
Cenário: deletar transação com Api token inválido - Status Code: 401
    Dado que tenho uma transação criada com o cartão:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando deleto a transação com um token invalido
    Entao Recebo o status code "401" e a mensagem "Access is denied due to invalid token" informando que o token é invalido.


@deleta_transacao_inexistente @deleta_transacoes
Cenário: deletar transação inexistente - Status Code: 404
    Dado que tenho uma transação criada com o cartão:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando deleto a transação inexistente
    Entao Recebo o código de sucesso "404" e a mensagem informando que a Transação não foi encontrada.
 
@transacao_valor_negativo @BKD-312 
Cenário: Tentar reserva com valor negativo - Status Code: 400
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com um valor negativo
    # E verifico no banco os dados dessa transação
    Então recebo o status code "400" e a mensagem "Invalid request"

@transacao_valor_vazio
Cenário: Tentar uma reserva valor zerado - Status Code: 400
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com um valor zerado
    Então recebo o status code "400" e a mensagem "Invalid request: Total value=0.00" informando que o parametro amount está zerado.

@alfanumerico
Cenário: Tentar passar venda com valor alfanumérico - Status Code: 400
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com um valor alfanumérico
    Então recebo o status code "400" e a mensagem "Invalid request"

@transacao_valor_alto
Cenário: Tentar reserva com valor muito alto - Status Code: 400
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com um valor muito alto
    E recebo o status code "400" e a mensagem "Invalid amount" como resposta.
    # E verifico o pedido no banco.

@transacao_apikey_invalido @ultimos_cenarios
Cenário: Criar transação com API Key inválido - Status Code: 401
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com ApiToken inválido 
    Entao Recebo o status code "401" e a mensagem "Access is denied due to invalid token".

@cria_transacao_numero_incorreto @cria_transacao_cartoes @BKD-311
Cenário: Criar transação com numero de cartão incorreto - Status Code: 400
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |0000000000000000| 10  | 20 | 737 |
    Quando faço um envio de uma transação com numero incorreto
    Então recebo o status code "400" e mensagem informando que o número é inválido.

@cria_transacao_sem_additionalData_address
Cenário: Criar transação sem informar o additionalData Address - Status Code: 400
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com o additionalData Address removido do Body.
    Então recebo o status code "400" e mensagem informando que não foi possível encontrar o endereço.

@cria_transacao_com_fraud
Cenário: Criar transação com Fraude - Status Code: 500
    Dado que tenho o cartão encriptado:
        |cardNumber      |month|year| CVV |
        |2222400010000008| 10  | 20 | 737 |
    Quando faço um envio de uma transação com o cardHolderName = TEST_EDP_ADYEN_RESPONSE:20
    Entao Recebo o status "500" e a mensagem "Requested test acquirer response."
    E verifico no banco os dados dessa transação com status DENIED
    E acesso a plataforma da Adyen e verifico que o pedido será apresentado com status Refused.




# @confirmar_mesma_transação_duas_vezes
# Cenario: Confirmar a mesma transação duas vezes
# Dado que tenho o cartão encriptado:
#     |cardNumber      |month|year| CVV |
#     |2222400010000008| 10  | 20 | 737 |
# Quando faço um envio de uma transação
# E confirmo a transação através do TransactionID
# E consulto status action history no banco
# E Consulto notification no Banco
# E confirmo a transação novamente através do mesmo Order ID
# E Consulto a transação no banco
# Então dentro do banco verifico que no notification será apresentado status Failure.


    # @cria_transacao_hiper @cria_transacao_cartoes
# Cenário: Criar transação com cartão hiper
#     Dado que tenho o cartão encriptado:
#         |cardNumber      |month|year| CVV |
#         |6370950000000005| 10  | 20 | 737 |
#     Quando faço um envio de uma transação com Hiper
#     Entao Recebo o código de sucesso "200" e o código da transação
#     E verifico no banco os dados dessa transação
#     E acesso a plataforma da Adyen e verifico o número do pedido criado.

# 'Invalid request: Encrypted data used outside of valid time period'


#     # Não foi possível simularmos esse cenário. A Adyen nos retorna o erro 500.
# @Cria_com_data_incorreta @cria_transacao_fluxos_alternativos
# Cenário: Criar transação com Data Validade incorreta.
#     Dado que tenho o cartão encriptado:
#         |cardNumber      |month|year| CVV |
#         |2222400010000008| 01  | 28 | 737 |
#     Quando faço um envio de uma transação com data de validade incorreta.
#     # Entao Recebo o status code "406"
#     Entao Recebo o status code "406" e a mensagem "outro"