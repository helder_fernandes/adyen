#language: pt
@Teste_cartoes
Funcionalidade: Fluxo de operações com cartões de crédito
    Validar as operações de cartões de crédito
    Salvar, consultar, excluir e operações diversas

@SEVERITY:critical @salvar_cartao_mastercard @poc_bk @cria_todos_cartoes
Cenario: Salvar cartão MasterCard - Status Code 201
    Dado que eu esteja na tela de encriptar cartões 
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |2222400010000008| 10  | 20 | 737 |
    E coleto os dados encriptados para um MasterCard.
    E envio o cartão
    E recebo a mensagem "The card was successfully saved" e o status code "201"
    E verifico na API que o cartão foi criado com sucesso
    Então verifico na Adyen que o cartão foi salvo.

@SEVERITY:critical @salvar_cartao_amex @poc_bk @cria_todos_cartoes
Cenario: Salvar cartão AMEX - Status Code 201
    Dado que eu esteja na tela de encriptar cartões 
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |370000000000002 | 10  | 20 | 7373|
    E coleto os dados encriptados para um Amex.
    E envio o cartão
    E recebo a mensagem "The card was successfully saved" e o status code "201"
    E verifico na API que o cartão foi criado com sucesso
    Então verifico na Adyen que o cartão foi salvo.

@SEVERITY:critical @salvar_cartao_visa @cria_todos_cartoes
Cenario: Salvar cartão Visa - Status Code 201
    Dado que eu esteja na tela de encriptar cartões 
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |4000020000000000| 10  | 20 | 737 |
    E coleto os dados encriptados para um Visa.
    E envio o cartão
    E recebo a mensagem "The card was successfully saved" e o status code "201"
    E verifico na API que o cartão foi criado com sucesso
    Então verifico na Adyen que o cartão foi salvo.

@SEVERITY:trivial @salvar_cartao_hipercard @cria_todos_cartoes
Cenario: Salvar cartão HiperCard - Status Code 201
    Dado que eu esteja na tela de encriptar cartões 
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |6062828888666688| 10  | 20 | 737 |
    E coleto os dados encriptados para um HiperCard.
    E envio o cartão
    E recebo a mensagem "The card was successfully saved" e o status code "201"
    E verifico na API que o cartão foi criado com sucesso
    Então verifico na Adyen que o cartão foi salvo.



@SEVERITY:trivial @salvar_cartao_elo @cria_todos_cartoes
Cenario: Salvar cartão Elo - Status Code: 201
    Dado que eu esteja na tela de encriptar cartões 
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |5066991111111118| 10  | 20 | 737 |
    E coleto os dados encriptados para um Elo.
    E envio o cartão
    E recebo a mensagem "The card was successfully saved" e o status code "201"
    E verifico na API que o cartão foi criado com sucesso
    Então verifico na Adyen que o cartão foi salvo.


@SEVERITY:minor @Salvar_cartao_param_vazio_ano @cria_cartao_parametro_vazio
Cenário: Salvar cartão com parâmetro year vazio - Status Code: 400
    Dado que eu esteja na tela de encriptar cartões 
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |2222400010000008| 10  | 20 | 737 |
    E coleto os dados encriptados.
    E envio o cartão com o parametro year vazio
    Então receberei a mensagem "Required field" com o status code "400"

@Salvar_cartao_param_vazio_numero @cria_cartao_parametro_vazio
Cenário: Salvar cartão com parâmetro number vazio - Status Code: 400
    Dado que eu esteja na tela de encriptar cartões 
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |2222400010000008| 10  | 20 | 737 |
    E coleto os dados encriptados.
    E envio o cartão com o parametro numero vazio
    Então receberei a mensagem "Required field" com o status code "400"

@Salvar_cartao_param_vazio_mes @cria_cartao_parametro_vazio
Cenário: Salvar cartão com parâmetro month vazio - Status Code: 400
    Dado que eu esteja na tela de encriptar cartões 
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |2222400010000008| 10  | 20 | 737 |
    E coleto os dados encriptados.
    E envio o cartão com o parametro Month vazio
    Então receberei a mensagem "Required field" com o status code "400"

@Salvar_cartao_param_vazio_cvv @cria_cartao_parametro_vazio
Cenário: Salvar cartão com parâmetro cvv vazio - Status Code: 400
    Dado que eu esteja na tela de encriptar cartões 
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |2222400010000008| 10  | 20 | 737 |
    E coleto os dados encriptados.
    E envio o cartão com o parametro cvv vazio
    Então receberei a mensagem "Required field" com o status code "400"

@Salvar_cartao_hmac_invalido
Cenário: Salvar cartão com HMAC inválido - Status Code: 401
    Dado que eu esteja na tela de encriptar cartões 
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |2222400010000008| 10  | 20 | 737 |
    E coleto os dados encriptados.
    E envio HMAC incorreto
    Quando realizar a consulta terei como resposta status code "401" e a mensagem "Access denied due to invalid hmac"

@Salvar_cartao_numero_invalido
Cenário: Salvar cartão com Número inválido - Status Code: 406
    Dado que eu esteja na tela de encriptar cartões 
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |2222400010000008| 10  | 20 | 737 |
    E coleto os dados encriptados mas com número incorreto.
    E envio o cartão
    Quando realizar a consulta terei como resposta status code "406" e a mensagem "Unable to determine variant"

@salvar_sem_customerId @cria_todos_cartoes
Cenario: Salvar cartão removendo parâmetro customerId - Status Code: 400
    Dado que eu esteja na tela de encriptar cartões 
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |2222400010000008| 10  | 20 | 737 |
    E coleto os dados encriptados para um cartão sem customerID.
    E envio o cartão   
    Então receberei a mensagem "Required field" com o status code "400" 

@consultar_cartao_sucesso @consulta_cartoes
Cenário: Consultar cartões com sucesso - Status Code: 200
    Dado o endereço da API a ser consultada
    E eu tenha o id e Email
    Quando realizar a consulta terei como resposta status code "200" e o JSON body do cartão cadastrado.

@consultar_cartao_sem_email @consulta_cartoes
Cenário: Consultar cartão sem informar email - Status Code: 400
    Dado o endereço da API a ser consultada
    E eu informe apenas o customer ID
    Quando realizar a consulta terei como resposta status code "400" e a mensagem "Required field"

@consultar_cartao_sem_customer_id @consulta_cartoes
Cenário: Consultar cartão sem informar customer id - Status Code: 400
    Dado o endereço da API a ser consultada
    E eu informe apenas o email
    Quando realizar a consulta terei como resposta status code "400" e a mensagem "Required field"

@consultar_cartao_hmac_incorreto @consulta_cartoes
Cenário: Consultar cartão com HMAC incorreto - Status Code: 401
    Dado o endereço da API a ser consultada
    E eu informe um valor HMAC inválido
    Quando realizar a consulta terei como resposta status code "401" e a mensagem "Access denied due to invalid hmac"

@deletar_cartao_sucesso @deletar_cartao
Cenario: Deletar cartão com sucesso - Status Code: 204
    Dado o endereço da API para realizar a exclusão do cartão
    E consulte os cartoes cadastrados e colete o saveId de um deles
    Quando informar esses dados e consumir a API para exclusão do cartão
    Então receberei o status code 204 confirmando que o card foi deletado com sucesso.

@SEVERITY:minor @deletar_cartao_id_inexistente @deletar_cartao
Cenário: Deletar cartão de ID inexistente - Status Code: 404
    Dado o endereço da API para realizar a exclusão do cartão
    E um ID de cartão que não exista na base
    Quando executar o método com cartão inexistente verei a mensagem "not found" e o status code "404"

@SEVERITY:minor @deletar_card_hmac_incorreto @deletar_cartao
Cenário: Deletar cartão com HMAC incorreto - Status Code: 401
    Dado o endereço da API para realizar a exclusão do cartão
    E consulte os cartoes cadastrados e colete o saveId de um deles
    E informe os dados para exclusão mas com o HMAC incorreto
    Quando executar o método verei a mensagem "Access denied due to invalid hmac" e o status code "401"

@fluxo_cartao_completo_MasterCard @fluxo_cartao_completo
Cenário: Fluxo salvar, recuperar e excluir cartão - MasterCard.
    Dado que eu esteja na tela de encriptar cartões
    E Verifico que o cliente não possui nenhum cartão salvo
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |2222400010000008| 10  | 20 | 737 |
    E coleto os dados encriptados.
    E envio o cartão, recebo o texto "The card was successfully saved" e o status code "201" 
    Quando realizar a consulta na API verei que o cartão foi criado e o status code "200"
    Então verifico na Adyen que o cartão foi salvo.
    Quando informar esses dados e consumir a API para exclusão do cartão
    Então receberei o status code "204" confirmando que o card foi deletado com sucesso.
    Quando realizar a consulta na API verei que o cartão foi deletado e não foi retornado
    Então eu tento deletar o mesmo cartão novamente e receberei o status code "404" e a mensagem "not found"

@fluxo_cartao_completo_amex @fluxo_cartao_completo
Cenário: Fluxo salvar, recuperar e excluir cartão - AMEX.
    Dado que eu esteja na tela de encriptar cartões 
    E Verifico que o cliente não possui nenhum cartão salvo
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |370000000000002 | 10  | 20 | 7373|
    E coleto os dados encriptados.
    E envio o cartão, recebo o texto "The card was successfully saved" e o status code "201" 
    Quando realizar a consulta na API verei que o cartão foi criado e o status code "200"
    Então verifico na Adyen que o cartão foi salvo.
    Quando informar esses dados e consumir a API para exclusão do cartão
    Então receberei o status code "204" confirmando que o card foi deletado com sucesso.
    Quando realizar a consulta na API verei que o cartão foi deletado e não foi retornado
    Então eu tento deletar o mesmo cartão novamente e receberei o status code "404" e a mensagem "not found"

@fluxo_cartao_completo_visa @fluxo_cartao_completo
Cenário: Fluxo salvar, recuperar e excluir cartão - Visa.
    Dado que eu esteja na tela de encriptar cartões 
    E Verifico que o cliente não possui nenhum cartão salvo
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |4000020000000000| 10  | 20 | 737 |
    E coleto os dados encriptados.
    E envio o cartão, recebo o texto "The card was successfully saved" e o status code "201" 
    Quando realizar a consulta na API verei que o cartão foi criado e o status code "200"
    Então verifico na Adyen que o cartão foi salvo.
    Quando informar esses dados e consumir a API para exclusão do cartão
    Então receberei o status code "204" confirmando que o card foi deletado com sucesso.
    Quando realizar a consulta na API verei que o cartão foi deletado e não foi retornado
    Então eu tento deletar o mesmo cartão novamente e receberei o status code "404" e a mensagem "not found"

@fluxo_cartao_completo_hipercard @fluxo_cartao_completo
Cenário: Fluxo salvar, recuperar e excluir cartão - Hipercard.
    Dado que eu esteja na tela de encriptar cartões 
    E Verifico que o cliente não possui nenhum cartão salvo
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |6062828888666688| 10  | 20 | 737 |
    E coleto os dados encriptados.
    E envio o cartão, recebo o texto "The card was successfully saved" e o status code "201" 
    Quando realizar a consulta na API verei que o cartão foi criado e o status code "200"
    Então verifico na Adyen que o cartão foi salvo.
    Quando informar esses dados e consumir a API para exclusão do cartão
    Então receberei o status code "204" confirmando que o card foi deletado com sucesso.
    Quando realizar a consulta na API verei que o cartão foi deletado e não foi retornado
    Então eu tento deletar o mesmo cartão novamente e receberei o status code "404" e a mensagem "not found"

@fluxo_cartao_completo_elo @fluxo_cartao_completo
Cenário: Fluxo salvar, recuperar e excluir cartão - Elo.
    Dado que eu esteja na tela de encriptar cartões 
    E Verifico que o cliente não possui nenhum cartão salvo
    E informe os dados do cartão
    |cardNumber      |month|year| CVV |
    |5066991111111118| 10  | 20 | 737 |
    E coleto os dados encriptados.
    E envio o cartão, recebo o texto "The card was successfully saved" e o status code "201" 
    Quando realizar a consulta na API verei que o cartão foi criado e o status code "200"
    Então verifico na Adyen que o cartão foi salvo.
    Quando informar esses dados e consumir a API para exclusão do cartão
    Então receberei o status code "204" confirmando que o card foi deletado com sucesso.
    Quando realizar a consulta na API verei que o cartão foi deletado e não foi retornado
    Então eu tento deletar o mesmo cartão novamente e receberei o status code "404" e a mensagem "not found"



# @salvar_cartao_hiper @cria_todos_cartoes
# Cenario: Salvar cartão Hiper.
#     Dado que eu esteja na tela de encriptar cartões 
#     E informe os dados do cartão
#     |cardNumber      |month|year| CVV |
#     |6370950000000005| 10  | 20 | 737 |
#     E coleto os dados encriptados para um Hiper.
#     E envio o cartão
#     E recebo a mensagem "The card was successfully saved" e o status code "201"
#     E verifico na API que o cartão foi criado com sucesso
#     Então verifico na Adyen que o cartão foi salvo.