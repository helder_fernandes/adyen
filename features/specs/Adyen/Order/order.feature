#language: pt

Funcionalidade: Criando transações através do Order do app.
-Neste caso, a API a ser testada é a API do ORDER.
-Sendo assim, as transações serão criadas através dela e verificaremos o comportamento esperado.

# @cria_transacao_order
# Cenário: Criar transação pelo Order com sucesso.
#     Dado que tenho o cartão encriptado:
#         |cardNumber      |month|year| CVV |
#         |2222400010000008| 10  | 20 | 737 |
#     Dado que eu tenha o Body para criação do Pedido
#     E envio o mesmo realizando um put 
#     E recebo o status "PAYMENTCONFIRM" e orderId do pedido criado.
#     E acesso a plataforma da Adyen consultando o OrderID criado.
#     Então verifico no banco se o pedido foi criado com sucesso.
#     E confirmo uma das transações e recebo o status code "201" como resposta
#     Então acesso a Adyen e o pedido deve apresentar o status SettledInInstallments
#     E no banco deve ser apresentado o status CAPTURED

# @criar_transacao_com_fraud
# Cenário: Criar transação para validar Fraude.
#     Dado que tenho o cartão encriptado:
#         |cardNumber      |month|year| CVV |
#         |2222400010000008| 10  | 20 | 737 |
#     Dado que eu tenha o Body para criação do Pedido com fraude
#     E faço o put alterado para simular FRAUD
#     E acesso a Adyen e verifico que o pedido foi cancelado
#     Então no banco verei o status Denied.