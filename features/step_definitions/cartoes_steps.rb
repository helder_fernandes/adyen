require_relative '../support/configs.rb'

Dado("que eu esteja na tela de encriptar cartões") do
    visit $pagina_encriptacao
    puts "Página de Encriptação: #{$pagina_encriptacao}"
end

Dado("informe os dados do cartão") do |table|
# Delete para limpar os dados:
    deleta_cards()
    dados = table.hashes[0]
   
    @encriptacao = @encrypter.preencher_cartao(dados)
    # puts dados
    # puts dados["code"]
    tirar_foto(@scenario_name)
end

Dado("coleto os dados encriptados.") do
    code = "mc"
    @body = cria_cartao(@encriptacao,code)
    puts "Body de Envio:"
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@body))
    puts "--------------------"
end

Dado("coleto os dados encriptados mas com número incorreto.") do
    code = "mc"
    @body = cria_cartao_numero_errado(@encriptacao,code)
    puts "Body de Envio:"
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@body))
    puts "--------------------"
end

Dado("coleto os dados encriptados para um MasterCard.") do
    code = "mc"
    @body = cria_cartao(@encriptacao,code)
    puts "Body de Envio:"
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@body))
    puts "--------------------"
end

Dado("coleto os dados encriptados para um Amex.") do
    code = "amex"
    @body = cria_cartao(@encriptacao,code)
    puts "Body de Envio:"
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@body))
    puts "--------------------"
end

Dado("coleto os dados encriptados para um Visa.") do
    code = "visa"
    @body = cria_cartao(@encriptacao,code)
    puts "Body de Envio:"
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@body))
    puts "--------------------"
end

Dado("coleto os dados encriptados para um HiperCard.") do
    code = "hipercard"
    @body = cria_cartao(@encriptacao,code)
    puts "Body de Envio:"
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@body))
    puts "--------------------"
end

Dado("coleto os dados encriptados para um Hiper.") do
    code = "hiper"
    @body = cria_cartao(@encriptacao,code)
    puts "Body de Envio:"
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@body))
    puts "--------------------"
end

Dado("coleto os dados encriptados para um Elo.") do
    code = "elo"
    @body = cria_cartao(@encriptacao,code)
    puts "Body de Envio:"
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@body))
    puts "--------------------"
end

Dado("envio o cartão") do
    @response = @cartao.putCard()
    url = @response.request.last_uri.to_s
    puts "URL: #{url}"
end

Dado("recebo a mensagem {string} e o status code {string}") do |mensagem, statuscode|
    expect(@response.body.to_s).to have_content(mensagem) 
    expect(@response.code.to_s).to eql statuscode
    puts "Status Code: #{@response.code}"
    puts "Mensagem de Retorno: #{@response}" 
end

Então("verifico na Adyen que o cartão foi salvo.") do
    
    @adyen.login($adyen_login)

#     puts "--------------------"
#     # teste = find(:xpath, '/html/body/main/div/div/div/div/div[2]/div[1]/table/tbody/tr[1]/td[7]').text
#     # within("tbody#paymentTable"){ puts find(:xpath, "//td[contains(@title, \"elo\")]").text }
#     puts find(:css, "img[title='elo']").text
#     puts "--------------------"

#    puts page.all(:css, "img[title='elo']")
#     # puts teste
#     puts "--------------------"
hmac, @response_getCard = busca_cartao_api()
obj = JSON.parse(@response_getCard.body)
pretty_str = JSON.pretty_unparse(obj)

# Pegar o ID do cartão criado
obj.each do |item|  
#   puts JSON.pretty_generate(item)
  @saveIdCard = item["savedId"]
end
    sleep (20)
    @adyen.openPayments()
    tirar_foto(@scenario_name)
    codigo = @adyen.busca_cartao()
    puts "Código do cartão na Adyen : #{@saveIdCard}"
    tirar_foto(@scenario_name)
    # tirar_foto(@scenario_name)

end

Então("verifico na API que o cartão foi criado com sucesso") do
    hmac, busca_resposta = busca_cartao_api()
    
    puts "HMAC : #{hmac}"
    # hmac = hmac_consulta("customerId=#{$customerId}&email=#{$email}")
    # response = @busca_cartao.getCard("&email=#{$email}")
    obj = JSON.parse(busca_resposta.body)
    pretty_str = JSON.pretty_unparse(obj)
    puts "Status Code  : #{busca_resposta.code} #{busca_resposta.message}"
    puts "Body Resposta: "
    puts pretty_str
end

Dado("envio o cartão com o parametro year vazio") do                         
    @body = cria_cartao_sem_ano(@encriptacao)
    puts JSON.pretty_generate(JSON.parse(@body))
    @response = @cartao.putCard()
end                                                                          
                                                                            
Dado("envio o cartão com o parametro numero vazio") do                       
    @body = cria_cartao_sem_numero(@encriptacao)
    puts JSON.pretty_generate(JSON.parse(@body))
    @response = @cartao.putCard()
end                                                                          
                                                                            
Dado("envio o cartão com o parametro cvv vazio") do                          
    @body = cria_cartao_sem_cvv(@encriptacao)
    puts JSON.pretty_generate(JSON.parse(@body))
    @response = @cartao.putCard()
end                                                                          

Dado("envio o cartão com o parametro Month vazio") do
    @body = cria_cartao_sem_mes(@encriptacao)
    puts JSON.pretty_generate(JSON.parse(@body))
    @response = @cartao.putCard()
end

Então("receberei a mensagem {string} com o status code {string}") do |mensagem, statuscode|
  
    puts "Status Code: #{@response.code} - #{@response.message}"
    obj = JSON.parse(@response.body)
    pretty_str = JSON.pretty_unparse(obj)
    
    puts "body de retorno da API:"
    puts "--------------------"
    puts pretty_str
    puts "--------------------"
    
    expect(@response.code.to_s).to eql statuscode
    expect(@response.body.to_s).to have_content(mensagem)    
end

Dado("envio HMAC incorreto") do
    @body = cria_cartao_hmac_incorreto(@encriptacao)
    puts JSON.pretty_generate(JSON.parse(@body))
    @response = @cartao.putCard()
end

Quando("realizar o put terei como resposta status code {string} e a mensagem {string} informando que o HMAC é invalido.") do |statuscode, mensagem|
    puts "Status Code: #{@response.code} - #{@response.message}"
    obj = JSON.parse(@response.body)
    pretty_str = JSON.pretty_unparse(obj)
    
    puts "body de retorno da API:"
    puts "--------------------"
    puts pretty_str
    puts "--------------------"
    
    expect(@response.code.to_s).to eql statuscode
    expect(@response.body.to_s).to have_content(mensagem)    
end

Dado("o endereço da API a ser consultada") do
    puts "Endereço de consulta: #{$api_consulta_cards}"
end

Dado("eu tenha o id e Email") do
    
# método para criação de um cartão para validar a consulta
    cria_cartao_completo()

    puts "customerId: #{$customerId}"
    puts "email     : #{$email}"
    hmac = hmac_consulta("customerId=#{$customerId}&email=#{$email}")
    puts "HMAC      : #{hmac}"
end

Quando("realizar a consulta terei como resposta status code {string} e o JSON body do cartão cadastrado.") do |code|
    @response = @cartao.getCard($customerId,$email)
    obj = JSON.parse(@response.body)
    pretty_str = JSON.pretty_unparse(obj)
    puts "URL da consulta : #{@response.request.last_uri.to_s}"
    puts "Status Code  : #{@response.code} #{@response.message}"
    puts "Body Resposta: "
    puts "--------------------"
    puts pretty_str
    puts "--------------------"
    expect(@response.code.to_s).to eql code
end

Dado("eu informe apenas o customer ID") do
    cria_cartao_completo()

    puts "customerId: #{$customerId}"
    puts "email     : #{$email}"
    hmac = hmac_consulta("customerId=#{$customerId}&email=")
    puts "HMAC      : #{hmac}"
    @response = @cartao.getCard($customerId,'')
end

Dado("eu informe apenas o email") do
    cria_cartao_completo()

    puts "customerId: #{$customerId}"
    puts "email     : #{$email}"
    hmac = hmac_consulta("customerId=&email=#{$email}")
    puts "HMAC      : #{hmac}"
    @response = @cartao.getCard('',$email)

end

Dado("eu informe um valor HMAC inválido") do
    cria_cartao_completo()
    
    hmac = hmac_consulta("customerId=#{$customerId}&#{$email}0")
    puts "HMAC gerado: #{hmac}"
    @response = @cartao.getCard($customerId,$email)
end

Quando("realizar a consulta terei como resposta status code {string} e a mensagem {string}") do |code, mensagem|
    puts "URL da consulta: #{@response.request.last_uri.to_s}"
    obj = JSON.parse(@response.body)
    pretty_str = JSON.pretty_unparse(obj)
    puts "Status Code: #{@response.code} #{@response.message}"
    
    puts "Body Resposta: "
    puts pretty_str
    expect(@response.code.to_s).to eql code
end

Dado("o endereço da API para realizar a exclusão do cartão") do
    puts "Endereço para Exclusão de Cartões: #{$api_exclusao_cards}"
end

Dado("consulte os cartoes cadastrados e colete o saveId de um deles") do
    # limpar os cartões do cliente:
    deleta_cards()

    # depois cria um cartão para realizar o teste:
    resposta_cria = cria_cartao_completo()

    puts resposta_cria

    hmac, @response = busca_cartao_api()

    puts "CustomerId     : #{$customerId}"
    puts "Email          : #{$email}"
    puts "HMAC           : #{hmac}"
    
    expect(@response.code.to_s).to eql "200"

    @saveID = JSON.parse(@response.body)
    pretty_str = JSON.pretty_unparse(@savedId)
    @saveIdCard = ''
    puts "Cartões do usuário:"
    puts "--------------------"
    @saveID.each do |item|  
      puts JSON.pretty_generate(item)
      @saveIdCard = item["savedId"]
    end
    puts "--------------------"
    
    puts "Id Salvo:"
    if @saveIdCard != ""
      puts "Save ID   : #{@saveIdCard}"
    else
      puts "Save ID   : Usuário não possui cartão"
    end
end

Quando("informar esses dados e consumir a API para exclusão do cartão") do
    # dados = "#{$customerId}&#{@saveIdCard}"
    # puts "Dados: #{dados}"
    # hmac = hmac_delete(dados)
    # puts "HMAC : #{hmac}"
    @delete_response = deleta_cartao_com_id(@saveIdCard)
    puts "URL Completa da exclusão: #{@delete_response.request.last_uri.to_s}"
end

Então("receberei o status code {int} confirmando que o card foi deletado com sucesso.") do |code|
    expect(@delete_response.code).to eql code
    puts "Retorno da API:"
    puts "Status Code : #{@delete_response.code} #{@delete_response.message}"
end

Dado("um ID de cartão que não exista na base") do

    @saveIdCard = "0000000000000"
    hmac = hmac_delete("#{$customerId}&#{@saveIdCard}")
    puts "CustomerId     : #{$customerId}"
    puts "ID para busca  : #{@saveIdCard}"
    puts "HMAC           : #{hmac}"

end

Quando("executar o método com cartão inexistente verei a mensagem {string} e o status code {string}") do |mensagem, code|
    
    @response = @deleta_cartao.delete_Card("#{@saveIdCard}")
    puts "URL utilizada para exclusão do cartão:"
    puts "#{@response.request.last_uri.to_s}"
    puts "Status Code : #{@response.code}"
    puts "Mensagem API: #{@response}"

    expect(@response.code.to_s).to eql code
    expect(@response.to_s).to have_content(mensagem)
end

Dado("informe os dados para exclusão mas com o HMAC incorreto") do
    puts "HMAC incorreto : #{@hmac}"   
    @test = hmac_delete("#{$customerId}&#{@saveIdCard}/invalido")
end

Quando("executar o método verei a mensagem {string} e o status code {string}") do |mensagem, code|
    # hmac_delete("teste_adyen_aldemir2019&#{@saveIdCard}1234")
    hmac_delete("#{$customerId}&#{@saveIdCard}1234")
    @response = @deleta_cartao.delete_Card("#{@saveIdCard}")
    url = @response.request.last_uri.to_s
    puts url
    obj = JSON.parse(@response.body)
    pretty_str = JSON.pretty_unparse(obj)
    puts "Status Code: #{code} #{@response.message}"
    puts "Body Resposta: "
    puts "--------------------"
    puts pretty_str
    puts "--------------------"

    expect(@response.code.to_s).to eql code
    expect(@response.to_s).to have_content(mensagem)
end

Dado("Verifico que o cliente não possui nenhum cartão salvo") do
    # Delete para limpar os dados:
    deleta_cards()
    hmac, @response = busca_cartao_api()

    puts "CustomerId     : #{$customerId}"
    puts "Email          : #{$email}"
    puts "HMAC           : #{hmac}"
    puts "\n"
    puts "Retorno Consulta API:"
    puts "Status: #{@response.code} #{@response.message}"
    puts "Body: "
    puts "--------------------"
    puts @response.body
    puts "--------------------"

end

Dado("envio o cartão, recebo o texto {string} e o status code {string}") do |mensagem, statuscode|
    @response = @cartao.putCard()
    url = @response.request.last_uri.to_s
    expect(@response.body.to_s).to have_content(mensagem) 
    expect(@response.code.to_s).to eql statuscode
    puts "Retorno da API:"
    puts "Status Code: #{@response.code}"
    puts "Mensagem de Retorno: #{@response}"
end

Quando("realizar a consulta na API verei que o cartão foi criado e o status code {string}") do |code|
    hmac, @response_getCard = busca_cartao_api()
    obj = JSON.parse(@response_getCard.body)
    pretty_str = JSON.pretty_unparse(obj)

    # Pegar o ID do cartão criado
    obj.each do |item|  
    #   puts JSON.pretty_generate(item)
      @saveIdCard = item["savedId"]
    end
    
    puts "URL da consulta : #{@response_getCard.request.last_uri.to_s}"
    puts "HMAC consulta : #{hmac}"
    puts "Status Code  : #{@response_getCard.code} #{@response_getCard.message}"
    puts "Body Resposta: "
    puts "--------------------"
    puts pretty_str
    puts "--------------------"
    expect(@response_getCard.code.to_s).to eql code
end


Então("receberei o status code {string} confirmando que o card foi deletado com sucesso.") do |code|
    # delete_response = @deleta_cartao.delete_Card(@saveIdCard)
    # puts delete_response
    # puts delete_response.class
    puts "URL Completa: #{@delete_response.request.last_uri.to_s}"
    puts "Resposta da API:"
    puts "Status Code : #{@delete_response.code} #{@delete_response.message}"
    puts "Body : #{@delete_response.body}"
    expect(@delete_response.code.to_s).to eql code
end

Quando("realizar a consulta na API verei que o cartão foi deletado e não foi retornado") do
    hmac, @response = busca_cartao_api()

    puts "CustomerId     : #{$customerId}"
    puts "Email          : #{$email}"
    puts "HMAC           : #{hmac}"
    puts "\n"
    puts "Retorno Consulta API:"
    puts "Status: #{@response.code} #{@response.message}"
    puts "Body: "
    puts "--------------------"
    puts @response.body
    puts "--------------------"
end

Então("eu tento deletar o mesmo cartão novamente e receberei o status code {string} e a mensagem {string}") do |code, mensagem|
    @delete_response = deleta_cartao_com_id(@saveIdCard)
    puts "URL Completa: #{@delete_response.request.last_uri.to_s}"
    puts "Resposta da API:"
    puts "Status Code : #{@delete_response.code}"
    puts "Mensagem    : #{@delete_response.body}"
    expect(@delete_response.code.to_s).to eql code
    expect(@delete_response.body.to_s).to have_content(mensagem)
end


Dado("coleto os dados encriptados para um cartão sem customerID.") do
    code = "mc"
    @body = cria_cartao_sem_customerId(@encriptacao,code)
    puts "Body de Envio:"
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@body))
    puts "--------------------"
end