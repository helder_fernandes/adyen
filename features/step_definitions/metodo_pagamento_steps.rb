Dado("que faço a consulta na API") do
    @response = @metodo_pagamento.get_metodo_pagamento()
    url = @response.request.last_uri.to_s
    puts "URL de Consulta: #{url}"
    
end

Então("Recebo o retorno com status code {string} e todos os métodos de pagamento disponíveis") do |code|

    responseJson = JSON.parse(@response.body)
    pretty_str = JSON.pretty_unparse(responseJson)
    puts "Status Code: #{@response.code} #{@response.message}"
    puts "Body Resposta - Métodos de Pagamento disponíveis:"
    puts "--------------------"
    puts pretty_str
    puts "--------------------"
    expect(@response.code.to_s).to eql code
    # copia_arquivos($data_execucao)

    
end