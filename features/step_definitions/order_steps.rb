Dado("que eu tenha o Body para criação do Pedido") do
    code = 'mc'
    @body = cria_transacao_order(@encriptacao, code)
    puts JSON.pretty_generate(JSON.parse(@body))
  end
  
  Dado("envio o mesmo realizando um put") do
    @retorno = @putOrder.put_order_transaction()
    url = @retorno.request.last_uri.to_s
    puts url
  end
  
  Dado("recebo o status {string} e orderId do pedido criado.") do |string|
    @orderid = ""
    @data = ""
    obj = JSON.parse(@retorno.body)
    pretty_str = JSON.pretty_unparse(obj)
    obj.each_pair {|k,v| 
        @data = v if k == "data"
    }
    @data.each_pair {|k,v| 
        @orderid = v if k == "orderId"
    }
    puts pretty_str
    puts "Order criado: #{@orderid}"
    
  end
  
  Dado("acesso a plataforma da Adyen consultando o OrderID criado.") do
    @adyen.login($adyen_login)
    # visit 'https://ca-test.adyen.com/ca/ca/login.shtml'
    @adyen.openPayments()
    puts "O Order id criado foi       : #{@orderid}"
    @orderAdyen = @adyen.consultOrderPayment(@orderid)
    # @orderAdyen = find(:xpath, "//span[@title='#{@orderid}']")
    puts "Order id na página da Adyen : #{@orderAdyen.text}"
    expect(@orderAdyen.text).eql? @orderid
    # page.save_screenshot('results/screenshots/image_name.png')
    tirar_foto(@scenario_name)

    @adyen.openOrder()
    # page.save_screenshot('results/screenshots/image_name2.png')
    tirar_foto(@scenario_name)
  end
  
  Então("verifico no banco se o pedido foi criado com sucesso.") do
    result, @transactionId = consultaBanco_status(@orderid)
  end

  Quando("confirmo uma das transações e recebo o status code {string} como resposta") do |code|
    @statuscode = @order.confirma_Transacao(@transactionId)
    url = @statuscode.request.last_uri.to_s
    puts "URL de Confirmação: #{url}"
    puts "Retorno API: #{@statuscode.code} #{@statuscode.message}"
    puts @statuscode
    expect(@statuscode.code.to_s).to eql code
end
  
  
  Então("acesso a Adyen e o pedido deve apresentar o status SettledInInstallments") do
    # @adyen.login($adyen_login)
    @adyen.openPayments()

    @adyen.consulta_ordens_SettledInInstallments(@orderid)
    tirar_foto(@scenario_name)
  end
  
  Então("no banco deve ser apresentado o status CAPTURED") do
    @dados_arr = consultaBancoStatusCaptured(@transactionId,@status)
  end
  
  Dado("que eu tenha o Body para criação do Pedido com fraude") do
    code = 'mc'
    @body = cria_transacao_com_fraude(@encriptacao, code)
    puts JSON.pretty_generate(JSON.parse(@body))
  end

  Dado("faço o put alterado para simular FRAUD") do
    @retorno = @putOrder.put_order_transaction()
    url = @retorno.request.last_uri.to_s
    puts url
    puts @retorno
  end
  
  Então("no banco verei o status Denied.") do
    pending # Write code here that turns the phrase above into concrete actions
  end