Dado("que tenho o cartão encriptado:") do |table|
    # Delete para limpar os dados:
    deleta_cards()
    visit $pagina_encriptacao
    dados = table.hashes[0]
   
    @encriptacao = @encrypter.preencher_cartao(dados)
    # puts dados["code"]
    tirar_foto(@scenario_name)
end

Quando("faço um envio de uma transação com Mastercard") do
    @id = ""
    @status = ""
    code = "mc"
    @envio, @orderid = criaTransacao(@encriptacao,code)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL: #{url}"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "--------------------"

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 

end

Quando("faço um envio de uma transação com Visa") do
    @id = ""
    @status = ""
    code = "visa"
    @envio, @orderid = criaTransacao(@encriptacao,code)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL: #{url}"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "--------------------"

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end

Quando("faço um envio de uma transação com Amex") do
    @id = ""
    @status = ""
    code = "amex"
    @envio, @orderid = criaTransacao(@encriptacao,code)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL: #{url}"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "--------------------"

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end

Quando("faço um envio de uma transação com Hipercard") do
    @id = ""
    @status = ""
    code = "hipercard"
    @envio, @orderid = criaTransacao(@encriptacao,code)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL: #{url}"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "--------------------"

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end

Quando("faço um envio de uma transação com Elo") do
    @id = ""
    @status = ""
    code = "elo"
    @envio, @orderid = criaTransacao(@encriptacao,code)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL: #{url}"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "--------------------"

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end

Quando("faço um envio de uma transação com Hiper") do
    @id = ""
    @status = ""
    code = "hiper"
    @envio, @orderid = criaTransacao(@encriptacao,code)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL: #{url}"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "--------------------"

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end

Entao("Recebo o código de sucesso {string} e o código da transação") do |string|
    puts "Retorno da API"
    obj = JSON.parse(@retorno.body)
    pretty_str = JSON.pretty_unparse(obj)
    puts "Status: #{@status}" 
    puts "Código da Transação: #{@id}"
    puts "JSON Retorno: "
    puts "--------------------"
    puts pretty_str
    puts "--------------------"
    expect(@status).to eql "Authorised"
    
    # puts @status
end

Entao("Recebo o código de sucesso {string} os dados da transação e a mensagem de captura") do |code|
    puts "Retorno da API"
    obj = JSON.parse(@retorno.body)

    obj.each_pair {|k,v| 
        @pspReference = v if k == "pspReference"        
    } 
    

    pretty_str = JSON.pretty_unparse(obj)
    puts "Status         : #{@retorno.code} #{@retorno.message}" 
    puts "pspReference   : #{@pspReference}"
    puts "JSON Retorno: "
    puts "--------------------"
    puts pretty_str
    puts "--------------------"
    expect(@retorno.code.to_s).to eql code
    expect(pretty_str).to have_content("capture-received")
    
    # puts @status
end

Entao("verifico no banco os dados dessa transação") do
    @dados_arr = consultaBanco(@id,@status)
end

Entao("acesso a plataforma da Adyen e verifico o número do pedido criado.") do
    @adyen.login($adyen_login)
    # visit 'https://ca-test.adyen.com/ca/ca/login.shtml'
    @adyen.openPayments()
    puts "O Order id criado foi       : #{@orderid}"
    @orderAdyen = @adyen.consultOrderPayment(@orderid)
    # @orderAdyen = find(:xpath, "//span[@title='#{@orderid}']")
    puts "Order id na página da Adyen : #{@orderAdyen.text}"
    expect(@orderAdyen.text).eql? @orderid
    # page.save_screenshot('results/screenshots/image_name.png')
    tirar_foto(@scenario_name)

    @adyen.openOrder()
    # page.save_screenshot('results/screenshots/image_name2.png')
    tirar_foto(@scenario_name)
end

Quando("faço um envio de uma transação sem o parametro storeCard") do
    @id = ""
    @status = ""
    @envio, @orderid = criaTransacao_Sem_StoreCard(@encriptacao)
    @retorno = @transaction.postTransaction()
    puts "URL Completa: #{@retorno.request.last_uri.to_s}" 
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "----------------------"
end

Entao("Recebo o código {string} e a mensagem {string}") do |code, mensagem|
    puts "Status Code: #{@retorno.code}"
    puts "#{@retorno.body}"
    expect(@retorno.code.to_s).to eql code
    expect(@retorno.body.to_s).to have_content(mensagem)
end

Quando("faço um envio de uma transação com um CVC incorreto.") do
    @id = ""
    @status = ""
    code = "mc"
    @envio, @orderid = criaTransacao(@encriptacao,code)
    @retorno = @transaction.postTransaction()
    puts "URL Completa: #{@retorno.request.last_uri.to_s}" 
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts "----------------------"
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "----------------------"
end

Entao("Recebo o status code {string} e a mensagem {string}") do |code, mensagem|
    
    # puts @retorno
    
    obj = JSON.parse(@retorno.body)
    pretty_str = JSON.pretty_unparse(obj)
    puts "Status Code: #{@retorno.code} #{@retorno.message}"
    puts "Body reposta API:" 
    puts "----------------------"
    puts pretty_str
    puts "----------------------"

    expect(@retorno.code.to_s).to eql code
    expect(@retorno.body.to_s).to have_content(mensagem)
end

Então("consulto na base de dados se a Adyen nos retornou o status {string} {int} vezes") do |status, qtde|
    result, transactionId = consultaBanco_status(@orderid)

    result.each do |res|
      expect(res).to eql status
    end
    expect(result.size).to eql qtde
end

Quando("faço um envio de uma transação com dados expirados.") do
    @id = ""
    @status = ""
    @envio, @orderid = criaTransacao_numero_errado()
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts "----------------------"
    
    if (@retorno.to_s).include?("Encrypted data used outside of valid time period")
        puts "ATENÇÃO! Número encriptado fora da validade, utilize um novo para o teste"
        @expirado = true
        puts "----------------------"
    else
        puts JSON.pretty_generate(JSON.parse(@envio))
        @response = JSON.parse(@retorno.body)
        @response.each_pair {|k,v| 
            @id = v if k == "id"
            @status = v if k == "status"
        } 
        @expirado = false
        puts "----------------------"
    end
end

Entao("Recebo o status code {string} e a mensagem informando que o número esta incorreto") do |code|
    # puts @retorno
    
    if @expirado
        puts "A valiação não pode ser efetuada devido à encriptação Expirada"
        obj = JSON.parse(@retorno.body)
        pretty_str = JSON.pretty_unparse(obj)
        puts "Status Code: #{@retorno.code} #{@retorno.message}"
        puts "Body reposta API:" 
        puts "----------------------"
        puts pretty_str
        puts "----------------------"
    else
        obj = JSON.parse(@retorno.body)
        pretty_str = JSON.pretty_unparse(obj)
        puts "Status Code: #{@retorno.code} #{@retorno.message}"
        puts "Body reposta API:" 
        puts "----------------------"
        puts pretty_str
        puts "----------------------"

        expect(@retorno.code.to_s).to eql code
        # expect(@retorno.body.to_s).to have_content(mensagem)
    end

end

Quando("faço um envio de uma transação com data de validade expirada.") do
    @id = ""
    @status = ""
    code = "mc"
    @envio, @orderid = criaTransacao(@encriptacao,code)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "----------------------"
  
    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end

Quando("faço um envio de uma transação nova") do
    # @id = ""
    # @status = ""

    @order_id_manual = Time.now.strftime("%Y%m%d%H%M%S")

    @body_envio, @orderid = criaTransacao_Orderid_Manual(@encriptacao, @order_id_manual)
    @retorno = @transaction.postTransaction()
    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
        @orderid = v if k == "orderId"
    } 
    puts "id        : #{@id}"
    puts "Status    : #{@status}"
    puts "Orderid   : #{@orderid}"
    puts "Body Envio: "
    puts "----------------------"
    puts JSON.pretty_generate(JSON.parse(@body_envio))
    puts "----------------------"
end

Quando("crie uma nova transação com mesmo orderID") do
    @body_envio1, @orderid = criaTransacao_Orderid_Manual(@encriptacao, @orderid)
    @retorno = @transaction.postTransaction()
    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id1 = v if k == "id"
        @status1 = v if k == "status"
        @orderid = v if k == "orderId"
    } 
    puts "id         : #{@id1}"
    puts "Status     : #{@status1}"
    puts "Orderid    : #{@orderid}"
    puts "Body Envio : "
    puts "----------------------"
    puts JSON.pretty_generate(JSON.parse(@body_envio1))
    puts "----------------------"
end

Quando("eu vou na adyen e confirmo que ambas as transações serão apresentadas.") do
    @adyen.login($adyen_login)
    @adyen.openPayments()
    puts "O Order id criado foi: #{@orderid}"
    puts "id da Transação: #{@id}"
    puts "Status: #{@status}"

    @adyen.consulta_ordens_duplicadas(@orderid)
    foto = tirar_foto(@scenario_name)
    # foto_arr.push(pic1.to_s)
    @adyen.logout()    

end

Quando("confirmo uma das transações e recebo o status code {string}") do |code|
    @statuscode = @transaction.confirma_Transacao(@id)
    expect(@statuscode.code.to_s).to eql code
    puts "Retorno API: #{@statuscode.code} #{@statuscode.message}"
end

Quando("consulto no banco ambas todas as transações com o OrderID criado anteriormente.") do
    @order_arr = consultaBancoExternalId(@order_id_manual, @status)
end

Então("consulta na Adyen novamente e uma das transação apresentará o status SettledInInstallments.") do
    @adyen.login($adyen_login)
    @adyen.openPayments()
    puts "O Order id criado foi: #{@orderid}"
    puts "id do Retorno do pedido: #{@id}"
    puts "Status: #{@status}"

    @adyen.consulta_ordens_SettledInInstallments(@orderid)
    tirar_foto(@scenario_name)
end

Dado("que tenho uma transação criada com o cartão:") do |table|
    @id, @status, @body, @pretty_str1 = envia_transacao_full(table)
    expect(@status).to eql "Authorised"
    # puts "Body Envio:"
    puts @pretty_str1
end

Quando("confirmo a transação com token invalido") do
    @retorno = @transaction.confirma_Transacao_invalid_token(@id)
    url = @retorno.request.last_uri.to_s
    puts "URL da Transação: #{url}"
    puts "----------------------"
    puts "x-api-token: 5E05B564-3CF3-4ADB-9A19-8138419D6CB3"
    puts "----------------------"
    @response = JSON.parse(@retorno.body)
    @pretty_str = JSON.pretty_unparse(@response)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    }
    # @url = @urlConfirm.request.last_uri.to_s
    # puts "URL de Confirmação : #{@url}"
end

Entao("Recebo o status code {string} e a mensagem {string} como retorno.") do |code, mensagem|
    expect(@retorno.code.to_s).to eql code
    expect(@response).to have_content(mensagem)
    puts "Status Code : #{@retorno.code}"
    puts "Mensagem    : #{@retorno.message}"

    puts "Body da resposta:" 
    puts "----------------------"
    puts @pretty_str
    puts "----------------------"
  end

Quando("confirmo a transação") do
    # sleep 2
    @urlConfirm = @transaction.confirma_Transacao(@id)
    @url = @urlConfirm.request.last_uri.to_s
    puts "URL de Confirmação : #{@url}"
    # url = @response.request_last.uri.to_s
    # puts "URL Completa: #{url}"
end

Entao("recebo o código de sucesso {string} informando que a transação foi criada.") do |code|
    puts "Resposta API: #{@urlConfirm.code} #{@urlConfirm.message}"
    expect(@urlConfirm.code.to_s).to eql code
end

Entao("verifico no banco os dados dessa transação esta com status CAPTURED") do
    @dados_arr = consultaBancoStatusCaptured(@id,@status)
end

Quando("faço um envio de uma transação inexistente") do
    code = "mc"
    @envio, @orderid = criaTransacao(@encriptacao,code)
    @id = "8b3ba062-cac6-11e9-0000-2e0020a46423"
    puts "Transação Enviada: #{@id}"
    @retorno = @transaction.confirma_Transacao(@id)
end

Entao("Recebo o código {string} e o retorno de que a transação não foi encontrada") do |code|
    puts "Status Code  : #{@retorno.code}"
    puts "Body Retorno API: #{@retorno}"
    # puts JSON.pretty_generate(JSON.parse(@retorno.body))
    expect(@retorno.code.to_s).to eql code
end

Quando("deleto a transação") do
    @deletaUrl = @transaction.deleta_Transacao(@id) 
    @url = @deletaUrl.request.last_uri.to_s
    puts "URL de Exclusão : #{@url}"
    puts "Retorno API     : #{@deletaUrl.code}"
end

Entao("Recebo o código de sucesso {string}") do |code|
    expect(@deletaUrl.code.to_s).to eql code
    puts "Retorno API : #{@deletaUrl.code}"

end

Quando("deleto a transação inexistente") do
    @idIncorreto = "8b3ba062-cac6-11e9-0000-2e0020a00000"
    puts "Transação a ser deletada: #{@idIncorreto}"
    @response = @transaction.deleta_Transacao(@idIncorreto)
end

Entao("Recebo o código de sucesso {string} e a mensagem informando que a Transação não foi encontrada.") do |code|
    expect(@response.code.to_s).to eql code
    puts "Status Code  : #{@response.code}"
    puts "Resposta API : #{@response}"
end

Dado("que faço o charge back de uma transação para validar a API") do
    make_chargeback()
    @body = @chargeback.chargeBack_Transacao()

    @body.each_pair {|k,v| 
        puts k, v
        @retorno_api = v
        
        # @id = v if k == "id"
        # @status = v if k == "status"
    } 
end


Quando("faço um envio de uma transação sem o parametro zipcode") do
    @envio, @orderid = criaTransacao_sem_Zipcode(@encriptacao)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end

Quando("faço um envio de uma transação com zipcode nulo") do
    @envio, @orderid = criaTransacao_informando_Zipcode(@encriptacao, nil)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end

Quando("faço um envio de uma transação com zipcode vazio") do
    @envio, @orderid = criaTransacao_informando_Zipcode(@encriptacao, "")
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end

Quando("faço um envio de uma transação com zipcode inválido") do
    @envio, @orderid = criaTransacao_informando_Zipcode(@encriptacao, "abcde-000")
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end

Quando("faço um envio de uma transação com storeCard true") do
    @envio, @orderid = criaTransacaoStoreCard(@encriptacao, true)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end

Entao("verifico no banco os dados dessa transação card_saved_id está com o saveId do cartão") do
    @dados_arr = consultaBanco(@id,@status)
end

Quando("faço um envio de uma transação com storeCard false") do
    @envio, @orderid = criaTransacaoStoreCard(@encriptacao, false)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end

Entao("verifico no banco que a coluna card_saved_id está vazia") do
    @dados_arr = consultaBanco(@id,@status)
end

Quando("faço um envio de uma transação com processingType {string}") do |type|
    @id = ""
    @status = ""
    @envio, @orderid = criaTransacaoProcessingType(@encriptacao, type)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "----------------------"

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    }
  end

Entao("verifico no banco os dados dessa transação com status CAPTURING E CAPTURED") do
    @order_arr = consultaBancoExternalId(@orderid,@status)
    # sleep(60)
end

Quando("tento realizar a confirmação da transação deletada e retorna o status code {string} informando que a transação não existe.") do |code|
 # necessário realizar o delete duas vezes para funcionar (validando com Jedi)
    # @transaction.deleta_Transacao(@id) 

    @response = @transaction.confirma_Transacao(@id)
    puts "Status Code : #{@response.code}"
    puts "Resposta API: #{@response.body}"
    expect(@response.code.to_s).to eql code
end

Entao("verifico no banco os dados dessa transação com status DENIED") do
    @order_arr = consultaBancoExternalId(@orderid,@status)
end

Dado("uma transação confirmada") do
    @urlConfirma = @transaction.confirma_Transacao(@id)

    
    @url = @urlConfirma.request.last_uri.to_s
    puts "URL de Confirmação  : #{@url}"

    @response = JSON.parse(@body.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
    puts "Status              : #{@status}" 
    puts "Código da Transação : #{@id}"

end

Entao("Recebo o código de sucesso {string} informando que a transação foi confirmada.") do |code|
    puts "URL de Confirmação : #{@url}"
    puts "Status Code        : #{@urlConfirma.code}"
    puts "Resposta API       : #{@urlConfirma.message}"
    expect(@urlConfirma.code.to_s).to eql code
end

Entao("verifico no banco os dados dessa transação com status REFUNDING e {string}") do |status|
    @dados_arr, @statusRefunded, @erro_timeout = consultaBanco_status_refunded(@id,@status)
    
    if @erro_timeout
        puts "Não foi possível validar o status '#{status}' devido ao tempo limite de espera..."
        expect(@statusRefunded).to eql status
    else
        expect(@statusRefunded).to eql status
    end
    
end

Quando("faço um envio de uma transação com um valor negativo") do
    @body_envio, @id = criaTransacaoComValor(@encriptacao, "-10")
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts "Body de Envio:" 
    puts "----------------------"
    puts JSON.pretty_generate(JSON.parse(@body_envio))
    puts "----------------------"
end

Então("recebo o status code {string} e a mensagem {string}") do |code, mensagem|
    puts "Status Code: #{@retorno.code}"
    puts "Retorno API: #{@retorno.body}"
    
    expect(@retorno.code.to_s).to eql code
    expect(@retorno.to_s).to have_content(mensagem)
end

Quando("faço um envio de uma transação com um valor zerado") do
    @body_envio, @body = criaTransacaoComValor(@encriptacao, "0.00")
    @retorno = @transaction.postTransaction()
    # @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts JSON.pretty_generate(JSON.parse(@body_envio))
end

Então("recebo o status code {string} e a mensagem {string} informando que o parametro amount está zerado.") do |code, mensagem|
    expect(@retorno.to_s).to have_content(mensagem)
    expect(@retorno.code.to_s).to eql code

    # obj = JSON.parse(@retorno.body)
    # pretty_str = JSON.pretty_unparse(obj)
    puts "Retorno API :"
    puts "Status Code : #{@retorno.code}"
    puts "Mensagem    : #{@retorno.body}"
    # puts pretty_str
end

Quando("faço um envio de uma transação com um valor alfanumérico") do
    @body_envio, @orderid = criaTransacaoComValor(@encriptacao, "123dsdafa2")
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts JSON.pretty_generate(JSON.parse(@body_envio))
end

Quando("faço um envio de uma transação com um valor muito alto") do                               
    @body, @orderid = criaTransacaoComValor(@encriptacao, "40000000000000000")
    @body_envio1 = @transaction.postTransaction()
    @urlpost = @body_envio1.request.last_uri.to_s
    puts "URL Completa: #{@urlpost}"
    puts JSON.pretty_generate(JSON.parse(@body))
end                                                                                               
                                                                                                
Quando("recebo o status code {string} e a mensagem {string} como resposta.") do |code, mensagem| 
    expect(@body_envio1.code.to_s).to eql code
    expect(@body_envio1.body.to_s).to have_content(mensagem)
    
    puts "Status Code: #{@body_envio1.code}"
    puts "Mensagem: #{@body_envio1}"
end                                                                                               

Quando("faço um envio de uma transação com um valor definido") do            
    @body_envio1, @orderid = criaTransacaoComValor(@encriptacao, "29.90")
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    @id = ""
    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    }
    puts "Id da Transação: #{@id}"
    puts JSON.pretty_generate(JSON.parse(@body_envio1))
end                                                                          
                                                                            
Então("altero o valor no banco para um valor diferente") do     
    @novoValor = updateBanco(@id, 5990)
    expect(@novoValor.to_i).to eql 5990
    puts "Novo Valor: #{@novoValor}"

    puts "aqui"
end

E("confirmo a transação com o valor inicial") do
    puts "depois aqui"
    @transaction.confirma_Transacao(@id)
    @banco = consultaBanco(@id, @status) 

end

Quando("faço um envio de uma transação") do                                          
    @id = ""
    @status = ""
    code = "mc"
    @envio, @orderid = criaTransacao(@encriptacao,code)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Completa: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "----------------------"
    
    obj = JSON.parse(@retorno.body)
    pretty_str = JSON.pretty_unparse(obj)
    puts "Resposta API: #{pretty_str}"
    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
        puts @status
    } 
end                                                                                  
                                                                                    
Entao("acesso a plataforma da Adyen e realizo o chargeback.") do                     

    data = Time.now.strftime("%Y-%m-%d %H:%M:%S")
    @adyen.openChageBackPage($adyen_login)

    charge_back_dados = OpenStruct.new

    charge_back_dados.orderId = @orderid
    charge_back_dados.pspReference = "123456789"
    charge_back_dados.originalReference = "987654321"
    charge_back_dados.eventDate = data
    charge_back_dados.amount = "10"
    charge_back_dados.paymentMethod = "mc"
    charge_back_dados.reason = "TestingChargeBack"
        
    @adyen.doingChargeBack(charge_back_dados)
    page.save_screenshot('results/screenshots/image_name.png')
    tirar_foto(@scenario_name)
    puts "ChargeBack Realizado com sucesso."
end

Então("retorno ao banco onde deverá ser apresentado o status {string}") do |statusBanco|  
    @dados_arr, @statusChargeback = consultaBancoStatusChargeback(@id,@status)
    expect(@statusChargeback).to eql statusBanco
end


Dado("faço o envio de uma nova transação") do
    
    @id = ""
    @status = ""
    code = "mc"
    @envio, @orderid = criaTransacao(@encriptacao, code)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Transação: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "----------------------"

    @response = JSON.parse(@retorno.body)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    }
    puts "Retorno da transação"
    puts "----------------------"
    puts "Status              : #{@status}" 
    puts "Código da Transação : #{@id}"
    puts "----------------------"

end

Dado("deleto cartão que foi utilizado na transação.") do
    deleta_cards()
    @response = @cartao.getCard($customerId,$email)
    obj = JSON.parse(@response.body)
    pretty_str = JSON.pretty_unparse(obj)
    puts "URL da consulta : #{@response.request.last_uri.to_s}"
    puts "Status Code  : #{@response.code} #{@response.message}"
    puts "Body Resposta: "
    puts "--------------------"
    puts pretty_str
    puts "--------------------"
    puts "Cartões deletados com sucesso"
    # puts "URL utilizada para exclusão do cartão:"
    # puts "#{@response.request.last_uri.to_s}"
    # puts "Status Code : #{@retorno.code}"
    # puts "Mensagem API: #{@retorno}"
end

Dado("confirmo a transação criada") do
    @urlConfirm = @transaction.confirma_Transacao(@id)
    @url = @urlConfirm.request.last_uri.to_s
    puts "URL de Confirmação: #{@url}"
    puts "Retorno API "
    puts "Status Code : #{@urlConfirm.code}"
    puts "Mensagem    : #{@urlConfirm.message}"
end

Entao("recebo o código status code {string} informando que a transação foi criada.") do |code|
    expect(@retorno.code.to_s).to eql code
    puts "Status Code: #{@retorno.code} #{@retorno.message}"
end

Entao("verifico no banco os dados dessa transação ela com status CAPTURED") do
    @dados_arr = consultaBancoStatusCaptured(@id,@status)
end

Quando("faço um envio de uma transação com ApiToken inválido") do                  
    @id = ""
    @status = ""
    @envio, @orderid = criaTransacao_invalid_token(@encriptacao)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL da Transação: #{url}"
    puts "----------------------"
    puts "x-api-token: 5E05B564-3CF3-4ADB-9A19-8138419D6CB3"
    puts "Body de Envio:" 
    puts "----------------------"
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "----------------------"
    @response = JSON.parse(@retorno.body)
    @pretty_str = JSON.pretty_unparse(@response)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    } 
end                                                                                
                                                                                    
Entao("Recebo o status code {string} e a mensagem {string}.") do |code, mensagem| 
    expect(@retorno.code.to_s).to eql code
    expect(@response).to have_content(mensagem)
    puts "Status Code : #{@retorno.code}"
    puts "Mensagem    : #{@retorno.message}"

    puts "Body da resposta:" 
    puts "----------------------"
    puts @pretty_str
    puts "----------------------"
end                                                                                

Entao("confirmo a transação que já foi deletada.") do
    @retornoConfirm = @transaction.confirma_Transacao(@id)
    url = @retornoConfirm.request.last_uri.to_s
    puts "URL: #{url}"
    puts "Status Code: #{@retornoConfirm.code}"
    puts "Resposta API: #{@retornoConfirm.body}"
end

Então("Acesso o banco e vejo que o status {string} será apresentado.") do |statusBanco|
    @dados_arr, @statusDenied, @erro_timeout = consultaBancoStatusDenied(@id,@status)
    
    if @erro_timeout
        puts "Não foi possível validar o status '#{statusBanco}' devido ao tempo limite de espera..."
        expect(@statusDenied).to eql statusBanco
    else
        expect(@statusDenied).to eql statusBanco
    end
end

Quando("faço um envio de uma transação com numero incorreto") do
    @id = ""
    @status = ""
    code = "mc"
    @envio, @orderid = criaTransacao(@encriptacao, code)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Transação: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "----------------------"

    @response = JSON.parse(@retorno.body)
    @pretty_str = JSON.pretty_unparse(@response)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @status = v if k == "status"
    }
    puts "Retorno da transação"
    puts "----------------------"
    puts "Status Code         : #{@retorno.code} #{@retorno.message}" 
    puts "Código da Transação : #{@id}"
    puts "----------------------"
end
  
Então("recebo o status code {string} e mensagem informando que o número é inválido.") do |code|
    expect(@retorno.code.to_s).to eql code
    puts @pretty_str
end

Quando("faço um envio de uma transação com o additionalData Address removido do Body.") do
    @id = ""
    @status = ""
    code = "mc"
    @envio, @orderid = criaTransacao_sem_address(@encriptacao, code)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL Transação: #{url}"
    puts "----------------------"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "----------------------"

    # @response = JSON.parse(@retorno.body)
    # @pretty_str = JSON.pretty_unparse(@response)
    # @response.each_pair {|k,v| 
    #     @id = v if k == "id"
    #     @status = v if k == "status"
    # }
end

Quando("deleto a transação com um token invalido") do
    @retorno = @transaction.deleta_Transacao_invalid_token(@id)
    @url = @retorno.request.last_uri.to_s
    puts "URL de Exclusão : #{@url}"
    puts "Retorno API     : #{@retorno.code}"
    puts "----------------------"
    puts "x-api-token: 5E05B564-3CF3-4ADB-9A19-8138419D6CB3"
    puts "----------------------"
    @response = JSON.parse(@retorno.body)
    @pretty_str = JSON.pretty_unparse(@response)
end

Então("recebo o status code {string} e mensagem informando que não foi possível encontrar o endereço.") do |code|
    expect(@retorno.code.to_s).to eql code
    puts "Retorno da transação"
    puts "----------------------"
    puts "Status Code         : #{@retorno.code}"
    puts "Mensagem de Retorno : #{@retorno.body}"
end

Entao("Recebo o status code {string} e a mensagem {string} informando que o token é invalido.") do |code, mensagem|
    expect(@retorno.code.to_s).to eql code
    expect(@response).to have_content(mensagem)
    puts "Status Code : #{@retorno.code}"
    puts "Mensagem    : #{@retorno.message}"

    puts "Body da resposta:" 
    puts "----------------------"
    puts @pretty_str
    puts "----------------------"
end

Quando("faço um envio de uma transação com o cardHolderName = TEST_EDP_ADYEN_RESPONSE:{int}") do |int|
    @id = ""
    @message = ""
    code = "mc"
    @envio, @orderid = criaTransacao_fraude(@encriptacao,code)
    @retorno = @transaction.postTransaction()
    url = @retorno.request.last_uri.to_s
    puts "URL: #{url}"
    puts "x-api-token: #{$api_token}"
    puts "Body de Envio:" 
    puts "--------------------"
    puts JSON.pretty_generate(JSON.parse(@envio))
    puts "--------------------"

    @response = JSON.parse(@retorno.body)
    @pretty_str = JSON.pretty_unparse(@response)
    @response.each_pair {|k,v| 
        @id = v if k == "id"
        @message = v if k == "message"
    }
end
  
Entao("Recebo o status {string} e a mensagem {string}") do |code, mensagem|
    expect(@retorno.code.to_s).to eql code
    expect(@retorno.body.to_s).to have_content(mensagem)
    puts "Status Code: #{@retorno.code} #{@retorno.message}"
    puts @pretty_str
end
  
Entao("acesso a plataforma da Adyen e verifico que o pedido será apresentado com status Refused.") do
    @adyen.login($adyen_login)
    # visit 'https://ca-test.adyen.com/ca/ca/login.shtml'
    @adyen.openPayments()
    puts "O Order id criado foi       : #{@orderid}"
    @orderAdyen = @adyen.consultOrderPayment(@orderid)
    # @orderAdyen = find(:xpath, "//span[@title='#{@orderid}']")
    puts "Order id na página da Adyen : #{@orderAdyen.text}"
    expect(@orderAdyen.text).eql? @orderid
    # page.save_screenshot('results/screenshots/image_name.png')
    tirar_foto(@scenario_name)

    @adyen.openOrder()
    # page.save_screenshot('results/screenshots/image_name2.png')
    tirar_foto(@scenario_name)
end